<?php
include('conexion.php');
include('usuario.php');
include("inicio.php");
include('manual.php');
$texto = $manualIndex;
$passBlanqueada = 0;

$checkusuario = "SELECT * FROM usuario left join sucursal on sucursal_usuario = id_sucursal WHERE nombre_usuario = '$nombre_usuario_log' and estado_usuario = '1' ";
$ejecutarA = mysqli_query($con, $checkusuario);		

while($fila = mysqli_fetch_array($ejecutarA)){
    $pass = $fila['password_usuario'];
}

if (password_verify($nombre_usuario_log, $pass)) {	
  $passBlanqueada = 1;		
}

$query = "SELECT * FROM usuario WHERE id_usuario = $id_usuario_log";
$ejecutar = mysqli_query($con, $query);
while ($row = mysqli_fetch_array($ejecutar)) {
  $descripcion_usuario_log = $row['descripcion_usuario'];
}

$tex = rand(0, 4);

switch ($tex) {
  case 0:
      $mensajeBienvenida =  "<h4> Hola <b> $descripcion_usuario_log</b> ¿Como estás? </h4>";
      break;
  case 1:
    $mensajeBienvenida =  "<h4> Hola de nuevo <b> $descripcion_usuario_log</b>. </h4>";
    break;
  case 2:
    $mensajeBienvenida =  "<h4>Hola, ¿Que tal estás <b> $descripcion_usuario_log</b>?  </h4>";
    break;
  case 3:
    $mensajeBienvenida =  "<h4>Hey! Es bueno volver a verte <b> $descripcion_usuario_log</b></h4>";
    break;
  case 4:
    $mensajeBienvenida =  "<h4> Hola <b> $descripcion_usuario_log</b> ¿Como estás? </h4>";
    break;
}

?>

<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Sistema de Gestión de Ferreteria</h3>
      </div>
    <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
      <i class="fa fa-question-circle fa-2x"></i> 
    </button>
    </div>
    <div class="clearfix"></div>

    <div class="row">
      <div class="col-md-12 col-sm-12  ">
        <div class="x_panel">
          <div class="x_title">
            <h4> Bienvenido <?php echo $descripcion_usuario_log ?></h4>

            <div class="clearfix"></div>
          </div>
          <div class="x_content">


            </div>

            <div class="col-md-6 col-sm-6" >

           <?php echo $mensajeBienvenida ?>
<p>Recordá que podés presionar el simbolo <i class='fa fa-question-circle'></i> para visualizar la ayuda en todas las pantallas.</p>



</div>

<div class="col-md-6 col-sm-6" >
              <div style="float:right">
                <div id="TT_FWTAbhYBt88aWFhUKfSzjDzzD6lUTYclbY1YEZCIKED1k1k1E" ></div>
                <script type="text/javascript" src="https://www.tutiempo.net/s-widget/l_FWTAbhYBt88aWFhUKfSzjDzzD6lUTYclbY1YEZCIKED1k1k1E"></script>
              </div>

          </div>
          <div class="col-md-6 col-sm-6">
              <p><b>Sucursal:</b> <?php echo $nombre_sucursal_log ?></p>
              <p><b>Perfil: </b> <?php echo $perfil_usuario_log ?></p>
              <?php if( $passBlanqueada == 1) { ?>
                <p style="color:red"><b>Atención: </b> Parece que aún tenés la contraseña por defecto. Hacé click <a href="cambiarPass.php">ACA</a> para cambiarla</p>
            <?php } ?>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Inicio");
</script>