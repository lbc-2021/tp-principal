<?php //query para traer el contenido de la tabla Sector para ser usando en el ComboBox
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualCrearUsuario;

if ($perfil_usuario_log != 'Administrador') {
    $message = "No posee permisos para realizar la acción";
    $class = "alert alert-danger";
    header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
}

include('inicio.php');



//query para traer las sucursales para llenar el combobox
$querySucursales = "SELECT * from sucursal where estado_sucursal = '1'";
$ejecutarSucursales = mysqli_query($con, $querySucursales);

//query para traer los perfiles para llenar el combobox
$queryPerfiles = "SELECT * from perfil where estado_perfil = '1'";
$ejecutarPerfiles = mysqli_query($con, $queryPerfiles);

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Crear Usuario</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form method="post" action="funciones/crearusuario_funcion.php" method="POST" novalidate>
                            <span class="section">Completar Datos</span>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Nombre y Apellido<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" data-validate-length-range="3" data-validate-words="2" name="nombre" id="nombre" required="required">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Usuario<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="usuario" data-validate-length-range="4" required="required">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="sucursal" class="form-control" required="required">
                                        <option value="">Seleccione una Sucursal</option>
                                        <?php while ($row = mysqli_fetch_array($ejecutarSucursales)) { ?>
                                            <option value="<?php echo $row['id_sucursal']; ?>">
                                                <?php echo $row['nombre_sucursal']  ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Perfil<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select onchange="switchRequiredEmail()" name="perfil" id="perfil" class="form-control" required="required">
                                        <option value="">Seleccione un Perfil</option>
                                        <option value="Cajero">Cajero</option>
                                        <option value="Vendedor">Vendedor</option>
                                        <option value="Supervisor">Supervisor</option>
                                        <option value="Gerente">Gerente</option>
                                        <option value="Administrador">Administrador</option>
                                    </select>
                                </div>
                            </div>

                            <div class="field item form-group" id="mailDiv">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Mail</label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control email optional" type="email" name="mail" id="mail">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Cargar</button>
                                    <button type='reset' class="btn btn-success">Limpiar Campos</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Nuevo usuario");
</script>