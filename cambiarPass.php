<?php
include('conexion.php');
include('usuario.php');
include("inicio.php"); 
include('manual.php');
$texto = $manualCambiarPass;?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cambiar Contraseña</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form class="" action="funciones/cambiarContraseña_funcion.php" method="post" novalidate>

                            <span class="section">Completar Datos</span>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Contraseña Actual<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="password" id="passwordv" name="passwordv" title="Contraseña Actual" required />
                                    <span style="position: absolute;right:15px;top:7px;" onclick="hideshow2()">
                                        <i id="slashv" style="display:none" class="fa fa-eye-slash"></i>
                                        <i id="eyev" class="fa fa-eye"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Nueva Contraseña<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="password" id="password1" name="password" placeholder="Minimo 8 caracteres, una mayuscula, una minuscula y un número " pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Minimo 8 caracteres, una mayuscula, una minuscula y un número" required />
                                    <span style="position: absolute;right:15px;top:7px;" onclick="hideshow()">
                                        <i id="slash" class="fa fa-eye-slash" style="display:none"></i>
                                        <i id="eye" class="fa fa-eye"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Repetir Nueva Contraseña<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" type="password" name="password2" data-validate-linked='password' required='required' />
                                </div>
                            </div>

                            <div class="ln_solid">
                                <div class="form-group">
                                    <div class="col-md-6 offset-md-3">
                                        <button type='submit' class="btn btn-primary">Cambiar Contraseña</button>
                                        <button type='reset' class="btn btn-success">Limpiar Campos</button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Cambiar contraseña");
</script>