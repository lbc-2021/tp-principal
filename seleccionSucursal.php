<?php

include('conexion.php');
include('usuario.php');
include('inicio.php');
include('manual.php');
$texto = $manualSeleccionSucursal;

$querySucursales = "SELECT * from sucursal where estado_sucursal = '1'";
$ejecutarSucursales = mysqli_query($con, $querySucursales);
$disabledBtn = "disabled";
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Reporte de caja diaria</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <form method="post" action="verSucursal.php?parametro=reporte" method="POST" novalidate>
                        <span class="section">Selección de sucursal y de rango de fechas</span>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <select name="sucursal" class="form-control" required="required">
                                    <option value="">Seleccione una Sucursal</option>
                                    <?php while ($row = mysqli_fetch_array($ejecutarSucursales)) { ?>
                                        <option value="<?php echo $row['id_sucursal']; ?>">
                                            <?php echo $row['nombre_sucursal']  ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha de inicio<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="fecha_inicio" id="fecha_inicio" onchange="validarFechas()" required="required" type="date" />
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha de fin<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="fecha_fin" id="fecha_fin" onchange="validarFechas()" required="required" type="date" />
                            </div>
                        </div>
                        <div class="field item form-group">
                            <div class="col-md-4 col-sm-4 label-align">
                                <button type='submit' class="btn btn-primary"  disabled id="myBtn">Generar reporte</button>
                            </div>
                        </div>
                        <div id = "mjeError" style="position: auto; left: 20%;" class="col-md-12 col-sm-12"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<script>
    function validarFechas(){
        console.log('Entrando en validar fechas');
        var fechainicial = document.getElementById("fecha_inicio").value;
        var fechafinal = document.getElementById("fecha_fin").value;
        if(fechafinal != '' && fechafinal != ''){
            if(Date.parse(fechafinal) >= Date.parse(fechainicial)) {
            document.getElementById("myBtn").disabled = false;
            document.getElementById("mjeError").innerHTML = "";
            }else {
                document.getElementById("myBtn").disabled = true;
                document.getElementById("mjeError").innerHTML = 
                    "<div class='alert alert-danger alert-dismissable col-md-6 col-sm-6'>"+
                        "<button type='button' class='close' data-dismiss='alert'>&times;</button>"+
                        "<strong>Error: La Fecha de inicio debe ser anterior o igual a la Fecha de fin.</strong>"+
                    "</div>";
            }
        }
       
}
</script>
<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Gestión de cuenta corriente");
</script>