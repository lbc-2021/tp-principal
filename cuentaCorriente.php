<?php

$id = $_GET['id'];
$parametro = $_GET['parametro'];
include('conexion.php');
include('usuario.php');
include('manual.php');

$querySelect = "SELECT * from $parametro where id_$parametro = $id";
$resultadoSelect = mysqli_query($con, $querySelect);

while ($fila = mysqli_fetch_array($resultadoSelect)) {
    $nombrev = $fila['nombre_' . $parametro];
    $deuda = $fila['deuda_' . $parametro];
    $limite = $fila['limite_' . $parametro];
    $estado = $fila['estado_' . $parametro];

    if ($estado == '1') {
        $estado = "Activo";
    } 
    if ($estado == '2') {
        $estado = "Moroso";
    }
    if ($estado == '3') {
        $estado = "Incobrable";
    }
    if ($estado == '0') {
        $estado = "Inactivo";
    }


}

if ($parametro == 'cliente') {
    $queryAuditoria = "SELECT * from auditoria_cc_clientes where cliente_accc = $id ORDER BY fecha_accc DESC";
    $aux = "accc";
    $texto = $manualCuentaCorrienteCliente;
} else {
    $queryAuditoria = "SELECT * from auditoria_cc_proveedores where proveedor_accp = $id ORDER BY fecha_accp DESC";
    $aux = "accp";
    $texto = $manualCuentaCorrienteProveedor;
}
$resultadoSelectAuditoria = mysqli_query($con, $queryAuditoria);

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cuenta corriente de <?php echo $nombrev ?> (<?php echo $parametro ?>)</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>   
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <?php if ($parametro == "cliente") { ?>
                        <span class="section">Cliente Nº <?php echo $id ?></span>
                    <?php } else { ?>
                        <span class="section">Proveedor Nº <?php echo $id ?></span>
                    <?php } ?>

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Deuda actual</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" name="deuda" id="deuda" readonly value="<?php if ($deuda >= 0) { ?>$<?php echo $deuda ?> <?php } else { ?> $(<?php echo $deuda ?>) <?php } ?>">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Límite de la deuda</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" name="limite" id="limite" readonly value="$<?php echo $limite ?>" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Estado del <?php echo $parametro ?></label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" name="estado" id="estado" readonly value="<?php echo $estado ?>" />
                        </div>
                    </div>
                    <div class="field item form-group">
                        <div class="col-md-5 col-sm-5 label-align">
                            <a href="cuentaCorrienteOperaciones.php?id=<?php echo $id; ?>&parametro=<?php echo $parametro; ?>" class="btn btn-success">Realizar operaciones</a>
                        </div>
                    </div>
                    <div class="table-title">
                        <h2>Historial de movimientos</h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class='text-center'>ID de la operación</th>
                                    <th class='text-center'>Fecha</th>
                                    <th class='text-center'>Descripción</th>
                                    <th class='text-center'>Monto anterior a la operación</th>
                                    <th class='text-center'>Monto</th>
                                    <th class='text-center'>Monto posterior a la operación</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php while ($row = mysqli_fetch_array($resultadoSelectAuditoria)) {
                                    $idAuditoria = $row['id_' . $aux];
                                    $fechaAuditoria = $row['fecha_' . $aux];
                                    $descripcionAuditoria = $row['descripcion_' . $aux];
                                    $montoPrevioAuditoria = $row['montoPrevio_' . $aux];
                                    $montoAuditoria = $row['monto_' . $aux];
                                    $montoActualAuditoria = $row['montoActual_' . $aux]; ?>
                                    <?php 
                                        $colorTd = "";
                                        if ($montoPrevioAuditoria > $montoActualAuditoria){
                                            $colorTd = 'ForestGreen';
                                        }else{
                                            $colorTd = 'red';
                                    } ?>
                                    <tr class="<?php echo $text_class; ?>">
                                        <td class='text-center'><?php echo $idAuditoria ?></td>
                                        <td class='text-center'><?php echo $fechaAuditoria ?></td>
                                        <td class='text-center'><?php echo $descripcionAuditoria ?></td>
                                        <td class='text-center'>$<?php echo $montoPrevioAuditoria ?></td>
                                        <?php if ($montoAuditoria != "0") { ?>
                                            <td class='text-center' style='color:<?php echo $colorTd?>'>$<?php echo $montoAuditoria ?></td>
                                        <?php } else { ?>
                                            <td class='text-center'>N/C</td>
                                        <?php } ?>
                                        <td class='text-center'>$<?php echo $montoActualAuditoria ?></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Gestión de cuenta corriente");
</script>