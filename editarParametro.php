<?php

$id = $_GET['id'];
$parametro = $_GET['parametro'];
include('conexion.php');
include('usuario.php');
include('manual.php');
$read = "";
if($perfil_usuario_log == "Vendedor")
{
    $read = "disabled";
}

$querySelect = "SELECT * from $parametro where id_$parametro = $id";

if ($parametro == 'producto') {
    $querySelect = "SELECT * from $parametro left join categoria on categoria_producto = id_categoria left join proveedor on proveedorPredeterminado_producto = id_proveedor left join subcategoria on subcategoria_producto = id_subcategoria  where id_$parametro = $id";
}
if ($parametro == 'proveedor' || $parametro == 'sucursal' || $parametro == 'cliente') {
    $querySelect = "SELECT * from $parametro left join domicilio on domicilio_$parametro = id_domicilio where id_$parametro = $id";
}
if ($parametro == 'subcategoria') {
    $querySelect = "SELECT * from $parametro left join categoria on categoria_subcategoria = id_categoria where id_$parametro = $id";
}

if($parametro == 'cliente'){
    $texto = $manualEditarParametroCliente;
}
if($parametro == 'producto'){
    $texto = $manualEditarParametroProducto;
}
if($parametro == 'sucursal'){
    $texto = $manualEditarParametroSucursal;
}
if($parametro == 'proveedor'){
    $texto = $manualEditarParametroProveedor;
}
if($parametro == 'categoria'){
    $texto = $manualEditarParametroCategoria;
}
if($parametro == 'subcategoria'){
    $texto = $manualEditarParametroSubCategoria;
}

$resultadoSelect = mysqli_query($con, $querySelect);

while ($fila = mysqli_fetch_array($resultadoSelect)) {
    $nombrev = $fila['nombre_' . $parametro];
    $estadov = $fila['estado_' . $parametro];
    $apoderado = $fila['apoderado_' . $parametro];
    $dni = $fila['dni_' . $parametro];
    $calle_domicilio = $fila['calle_domicilio'];
    $altura_domicilio = $fila['altura_domicilio'];
    $piso_domicilio = $fila['piso_domicilio'];
    $depto_domicilio = $fila['departamento_domicilio'];
    $cp_domicilio = $fila['cp_domicilio'];
    $localidad_domicilio = $fila['localidad_domicilio'];
    $provincia_domicilio = $fila['provincia_domicilio'];
    $cuil = $fila['cuil_' . $parametro];
    $mail = $fila['mail_' . $parametro];
    $telefono = $fila['telefono_' . $parametro];
    $precio = $fila['precio_' . $parametro];
    $stockMinimo = $fila['stockMinimo_' . $parametro];
    $proveedor = $fila['proveedorPredeterminado_' . $parametro];
    $nombre_proveedor = $fila['nombre_proveedor'];
    $nombre_categoria = $fila['nombre_categoria'];
    $categoria_producto = $fila['categoria_' . $parametro];
    $cantidadLote = $fila['cantidadLote_' . $parametro];
    $cantidadasolicitar = $fila['cantidadASolicitar_' . $parametro];
    $stockseguridad = $fila['stockSeguridad_' . $parametro];
    $condicionFiscal_cliente = $fila['condicionFiscal_cliente'];
    $tipo_cliente = $fila['tipo_cliente'];
    $subcategoria_producto = $fila['subcategoria_producto'];
    $nombre_subcategoria = $fila['nombre_subcategoria'];
    $iva = $fila['responsableIva_cliente'];


}

//traigo el contenido de la tabla Proveedor para mostrarlo en el combobox
$query = "SELECT * FROM proveedor where estado_proveedor= 1 and id_proveedor <> '$proveedor' ORDER BY nombre_proveedor ASC";
$resultado = mysqli_query($con, $query);

//traigo el contenido de la tabla Categoria para mostrarlo en el combobox
$queryCat = "SELECT * FROM categoria where estado_categoria= 1 and id_categoria <> '$categoria_producto' ORDER BY nombre_categoria ASC";
$resultadoCat = mysqli_query($con, $queryCat);

//traigo el contenido de la tabla Subategoria para mostrarlo en el combobox
$querySub = "SELECT * FROM subcategoria where estado_subcategoria= 1 and id_subcategoria <> '$subcategoria_producto' AND categoria_subcategoria = '$categoria_producto' ORDER BY nombre_subcategoria ASC";
$resultadoSub = mysqli_query($con, $querySub);

if ($estadov == '1') {
    $estado_usuarioDesc = "Activo";
    $aux = 0;
    $auxDesc = "Inactivo";
} else {
    $estado_usuarioDesc = "Inactivo";
    $aux = 1;
    $auxDesc = "Activo";
}

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Editar Parametro (<?php echo $parametro ?>)</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form method="post" action="funciones/editarParametro_funcion.php" novalidate>
                            <span class="section">Completar Datos</span>

                            <input type="hidden" <?php echo $read; ?> name="id" value="<?php echo $id ?>">
                            <input type="hidden" <?php echo $read; ?> name="parametro" value="<?php echo $parametro ?>">

                            <div class="field item form-group">

                                <?php if ($parametro != 'proveedor') { ?>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Nombre<span class="required">*</span></label>
                                <?php } else { ?>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Razón Social<span class="required">*</span></label>
                                <?php } ?>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" <?php echo $read; ?> data-validate-length-range="3" data-validate-words="1" name="nombre" id="nombre" required="required" value="<?php echo $nombrev ?>" />
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align"> Estado<span class="required">*</span> </label>
                                <div class="col-md-6 col-sm-6">
                                    <select <?php echo $read; ?> name="estado" class="form-control" required="required">
                                        <option value="<?php echo $estadov ?>"><?php echo $estado_usuarioDesc ?></option>
                                        <option value="<?php echo $aux ?>"><?php echo $auxDesc ?></option>
                                    </select>
                                </div>
                            </div>


                            <!-- Si es una subcategoria-->
                            <?php if ($parametro == 'subcategoria') { ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Categoria a la que pertenece<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select <?php echo $read; ?> class="form-control" name="categoria2" id="categoria2" required="required">
                                            <option value="<?php echo $categoria_producto ?>"><?php echo $nombre_categoria ?> </option>
                                            <?php while ($row = mysqli_fetch_array($resultadoCat)) { ?>
                                                <option value="<?php echo $row['id_categoria']; ?>">
                                                    <?php echo $row['nombre_categoria']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            <?php  } ?>
                            <!-- Si es una subcategoria-->

                            <!-- Si es un proveedor o sucursal o cliente-->
                            <?php if ($parametro == 'proveedor' || $parametro == 'sucursal' || $parametro == 'cliente') {
                                if ($parametro == 'proveedor') { ?>
                                    <div class=" field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Apoderado<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input <?php echo $read; ?> class="form-control" data-validate-length-range="6" data-validate-words="1" name="apoderado" id="apoderado" required="required" value="<?php echo $apoderado ?>" />
                                        </div>
                                    </div>
                                <?php }
                                if ($parametro == 'proveedor' || $parametro == 'cliente') { ?>
                                    <div class=" field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">DNI<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input <?php echo $read; ?> class="form-control" type="number" class='number' name="dni" id="dni" data-validate-length-range="8,8" required="required" value="<?php echo $dni ?>">
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">CUIL/CUIT<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input <?php echo $read; ?> class="form-control" name="cuil" id="cuil" required="required" value="<?php echo $cuil ?>" data-validate-length-range="11,11" type="number" />
                                        </div>
                                    </div>
                                    <?php } ?>

                                <?php if ($parametro == 'cliente') { ?>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Responsable Iva<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select <?php echo $read; ?> name="iva" class="form-control" required="required">
                                                <option value="<?php echo $iva ?>"><?php echo $iva ?></option>
                                                <?php if ($iva == 'No') echo "<option value='Si'>Si</option>";
                                                 if ($iva == 'Si') echo "<option value='No'>No</option>";  ?>                                                
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Domicilio:</span></label>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Calle<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="calle" id="calle" required='required' type="text" value="<?php echo $calle_domicilio ?>" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Altura<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="altura" id="altura" required='required' type="number" min="1" data-validate-minmax="1,999999999" value="<?php echo $altura_domicilio ?>" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Piso</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="piso" id="piso" type="number" min="0" <?php if ($piso_domicilio != null) { ?> value="<?php echo $piso_domicilio ?>" <?php } ?> />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Departamento</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="depto" id="depto" type="text" <?php if ($depto_domicilio != null) { ?> value="<?php echo $depto_domicilio ?>" <?php } ?> />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Código postal<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="cp" id="cp" required='required' type="text" pattern="^[A-Za-z0-9_-]*$" value="<?php echo $cp_domicilio ?>" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Localidad<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="localidad" id="localidad" required='required' type="text" value="<?php echo $localidad_domicilio ?>" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Provincia<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="provincia" id="provincia" required='required' type="text" value="<?php echo $provincia_domicilio ?>" />
                                    </div>
                                </div>

                                <?php if ($parametro == 'proveedor' || $parametro == 'cliente') { ?>
                                    <div class="field item form-group">
                                        <br>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Telefono<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input <?php echo $read; ?> class="form-control" type="tel" class='tel' n name="telefono" id="telefono" required="required" value="<?php echo $telefono ?>" data-validate-length-range="8,20" />
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Mail<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input <?php echo $read; ?> class="form-control" name="mail" id="mail" class='email' required="required" value="<?php echo $mail ?>" type="email" />
                                        </div>
                                    </div>
                                <?php }
                                if ($parametro == 'cliente') { ?>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"> Condición fiscal<span class="required">*</span> </label>
                                        <div class="col-md-6 col-sm-6">
                                            <select <?php echo $read; ?> name="condicionFiscal" class="form-control" required="required">
                                                <option value="Consumidor Final">Consumidor Final</option>
                                                <option value="IVA Responsable Inscripto">IVA Responsable Inscripto</option>
                                                <option value="IVA Responsable no Inscripto">IVA Responsable no Inscripto</option>
                                                <option value="IVA no Responsable">IVA no Responsable</option>
                                                <option value="IVA Sujeto Exento">IVA Sujeto Exento</option>
                                                <option value="Consumidor Final">Consumidor Final</option>
                                                <option value="Responsable Monotributo">Responsable Monotributo</option>
                                                <option value="Sujeto no Categorizado">Sujeto no Categorizado</option>
                                                <option value="Proveedor del Exterior">Proveedor del Exterior</option>
                                                <option value="Cliente del Exterior">Cliente del Exterior</option>
                                                <option value="IVA Liberado – Ley Nº 19.640">IVA Liberado – Ley Nº 19.640</option>
                                                <option value="IVA Responsable Inscripto – Agente de Percepción">IVA Responsable Inscripto – Agente de Percepción</option>
                                                <option value="Pequeño Contribuyente Eventual">Pequeño Contribuyente Eventual</option>
                                                <option value="Monotributista Social">Monotributista Social</option>
                                                <option value="Pequeño Contribuyente Eventual Social">Pequeño Contribuyente Eventual Social</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"> Tipo<span class="required">*</span> </label>
                                        <div class="col-md-6 col-sm-6">
                                            <select <?php echo $read; ?> name="tipo" class="form-control" required="required">
                                                <option value="fisico">Físico</option>
                                                <option value="juridico">Jurídico</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <!-- Fin proveedor o sucursal o cliente-->

                            <!-- Si es un producto -->
                            <?php if ($parametro == 'producto') { ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Precio<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="precio" id="precio" required="required" data-validate-minmax="0,999999999" value="<?php echo $precio ?>"  type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Categoria del
                                        Producto<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select <?php echo $read; ?> class="form-control" name="categoria" id="categoria" required="required" onchange="versubcategoria();">
                                            <option value="<?php echo $categoria_producto ?>">
                                                <?php echo $nombre_categoria ?></option>
                                            <?php while ($row = mysqli_fetch_array($resultadoCat)) { ?>
                                                <option value="<?php echo $row['id_categoria']; ?>">
                                                    <?php echo $row['nombre_categoria']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <input <?php echo $read; ?> type="hidden" id="subcataux" value="<?php echo $subcategoria_producto ?>">
                                <input <?php echo $read; ?> type="hidden" id="subcataux2" value="<?php echo $nombre_subcategoria ?>">


                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Subcategoria del Producto<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <div id="myDiv">
                                            <select <?php echo $read; ?> class="form-control" name="subcategoria" id="subcategoria" required="required">
                                                <option value="<?php echo $subcategoria_producto ?>"><?php echo $nombre_subcategoria ?></option>
                                                <?php while ($row = mysqli_fetch_array($resultadoSub)) { ?>
                                                    <option value="<?php echo $row['id_subcategoria']; ?>">
                                                        <?php echo $row['nombre_subcategoria']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>


                                <div class="field item form-group" id="lotediv" <?php if($categoria_producto == 2) echo  "hidden" ?>>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Cantidad por
                                        lote<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="cantidadLote" id="cantidadLote" <?php if($categoria_producto != 2) echo   'required="required"' ?> value="<?php echo $cantidadLote ?>" type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group" id="unidaddiv" <?php if($categoria_producto != 2) echo  "hidden" ?>>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Unidad de Compra<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" name="unidaddecompra" id="unidaddecompra" <?php if($categoria_producto == 2) echo   'required="required"' ?>>
                                            <?php  if($cantidadLote == 'Galon'){ echo "<option value='Galon'>Galon</option><option value='Litro'>Litro</option>"; }
                                              else if($cantidadLote == 'Litro') { echo "<option value='Litro'>Litro</option><option value='Galon'>Galon</option>"; }
                                              else {echo "<option value='Litro'>Litro</option><option value='Galon'>Galon</option>"; }?>
                                        </select>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align"><?php echo "¿Este producto tiene validación de Stock Minimo? <br><small> (Solicitud automatica por mail al Proveedor/Gerente) </small>" ?>
                                    </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="js-switch" type="checkbox" name="checkstock" id="checkstock" onchange="mostrardivs()" <?php if ($stockMinimo != 0)  echo "checked"; ?>>
                                    </div>
                                </div>


                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Proveedor
                                        Predeterminado<span class="required">*</span></label>
                                    <?php if($read != 'disabled'){?>
                                    <div class="col-md-6 col-sm-6">
                                        <select disabled class="form-control" required="required" name="proveedor" id="proveedor" >
                                            <option value="<?php echo $proveedor ?>"><?php echo $nombre_proveedor ?>
                                            </option>
                                            <?php while ($row = mysqli_fetch_array($resultado)) { ?>
                                                <option value="<?php echo $row['id_proveedor']; ?>">
                                                    <?php echo $row['nombre_proveedor']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php }else{?>
                                        <div class="col-md-6 col-sm-6">
                                    <input <?php echo $read; ?> class="form-control" name="cantidadLote" id="cantidadLote" required="required" value="<?php echo $nombre_proveedor?>" type="text" />
                                    </div>
                                    <?php } ?>
                                </div>



                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Stock
                                        Minimo</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="stockminimo" id="stockminimo" type="number" value="<?php echo $stockMinimo ?>" />
                                    </div>
                                </div>

                                <div class=" field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Cantidad a
                                        Solicitar</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="cantidadasolicitar" id="cantidadasolicitar" type="number" value="<?php echo $cantidadasolicitar ?>" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Stock de
                                        Seguridad</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input <?php echo $read; ?> class="form-control" name="stockseguridad" id="stockseguridad" type="number" value="<?php echo $stockseguridad ?>" />
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- Fin producto -->
                    </div>
                    <?php if($perfil_usuario_log != "Vendedor"){ ?>
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <button type='submit' class="btn btn-primary">Cargar</button>
                            <button type='reset' class="btn btn-success">Limpiar Campos</button>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /page content -->


<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Editar <?php echo $parametro ?>");
</script>