<?php
$id = $_GET['id'];
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualEditarUsuario;

if ($perfil_usuario_log != 'Administrador') {
    $message = "No posee permisos para realizar la acción";
    $class = "alert alert-danger";
    header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
}


//traigo el contenido del valor a editar consultando por ID y lo guardo en variables
$query = "SELECT * FROM usuario left join sucursal on sucursal_usuario = id_sucursal WHERE id_usuario = $id";
$resultado = mysqli_query($con, $query);

while ($fila = mysqli_fetch_array($resultado)) {
    $nombre_usuario = $fila['nombre_usuario'];
    $descripcion_usuario = $fila['descripcion_usuario'];
    $id_sucursal = $fila['sucursal_usuario'];
    $perfil = $fila['perfil_usuario'];
    $nombre_sucursal = $fila['nombre_sucursal'];
    $estado_usuario = $fila['estado_usuario'];
    $mail = $fila['mail_usuario'];
}

if ($estado_usuario == '1') {
    $estado_usuarioDesc = "Activo";
    $aux = 0;
    $auxDesc = "Inactivo";
} else {
    $estado_usuarioDesc = "Inactivo";
    $aux = 1;
    $auxDesc = "Activo";
}

//query para traer las sucursales para llenar el combobox
$querySucursales = "SELECT * from sucursal where estado_sucursal = '1' and id_sucursal <> $id_sucursal";
$ejecutarSucursales = mysqli_query($con, $querySucursales);


include('inicio.php');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Editar Usuario</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form method="post" action="funciones/usuarioEditar_funcion.php" method="POST" novalidate>
                            <span class="section">Completar Datos</span>

                            <input type="hidden" name="id" id="id" value="<?php echo $id; ?>">

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Nombre<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" data-validate-length-range="3" data-validate-words="2" name="descripcion" id="descripcion" required="required" value="<?php echo $descripcion_usuario; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Usuario<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="usuario" data-validate-length-range="4" required="required" value="<?php echo $nombre_usuario; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="sucursal" class="form-control" required="required">
                                        <option value="<?php echo $id_sucursal ?>"><?php echo $nombre_sucursal ?></option>
                                        <?php while ($row = mysqli_fetch_array($ejecutarSucursales)) { ?>
                                            <option value="<?php echo $row['id_sucursal']; ?>">
                                                <?php echo $row['nombre_sucursal']  ?></option>
                                        <?php } ?>
                                        <input type="hidden" name="nombre_sucursal" id="nombre_sucursal" value="<?php echo $nombre_sucursal; ?>">
                                    </select>
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Perfil<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select onchange="switchRequiredEmail()" name="perfil" id="perfil" class="form-control" required="required">
                                        <?php if ($perfil == 'Cajero') { ?>
                                            <option value="Cajero" selected>Cajero</option>
                                            <option value="Vendedor">Vendedor</option>
                                            <option value="Supervisor">Supervisor</option>
                                            <option value="Gerente">Gerente</option>
                                            <option value="Administrador">Administrador</option>
                                        <?php } else if ($perfil == 'Vendedor') { ?>
                                            <option value="Cajero">Cajero</option>
                                            <option value="Vendedor" selected>Vendedor</option>
                                            <option value="Supervisor">Supervisor</option>
                                            <option value="Gerente">Gerente</option>
                                            <option value="Administrador">Administrador</option>
                                        <?php } else if ($perfil == 'Supervisor') { ?>
                                            <option value="Cajero">Cajero</option>
                                            <option value="Vendedor">Vendedor</option>
                                            <option value="Supervisor" selected>Supervisor</option>
                                            <option value="Gerente">Gerente</option>
                                            <option value="Administrador">Administrador</option>
                                        <?php } else if ($perfil == 'Gerente') { ?>
                                            <option value="Cajero">Cajero</option>
                                            <option value="Vendedor">Vendedor</option>
                                            <option value="Supervisor">Supervisor</option>
                                            <option value="Gerente" selected>Gerente</option>
                                            <option value="Administrador">Administrador</option>
                                        <?php } else { ?>
                                            <option value="Cajero">Cajero</option>
                                            <option value="Vendedor">Vendedor</option>
                                            <option value="Supervisor">Supervisor</option>
                                            <option value="Gerente">Gerente</option>
                                            <option value="Administrador" selected>Administrador</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="field item form-group" id="mailDiv">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Mail<?php if ($perfil == 'Gerente') { ?><span class="required">*</span><?php } ?></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control email" type="email" <?php if ($perfil == 'Gerente') { ?> required="required" <?php } ?> name="mail" id="mail" value="<?php echo $mail; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align"> Estado <span class="required">*</span> </label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="estado" class="form-control" required="required">
                                        <option value="<?php echo $estado_usuario ?>"><?php echo $estado_usuarioDesc ?></option>
                                        <option value="<?php echo $aux ?>"><?php echo $auxDesc ?></option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Cargar</button>
                                    <button type='reset' class="btn btn-success">Reestablecer Campos</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Editar usuario");
</script>