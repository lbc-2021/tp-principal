<?php
require_once("../conexion.php");

$action = (isset($_REQUEST['action']) && $_REQUEST['action'] != NULL) ? $_REQUEST['action'] : '';
if ($action == 'ajax') {
	$query = $_REQUEST['query'];
	$p = $_REQUEST['p'];

	$tables = "venta left join sucursal on id_sucursal = sucursal_venta left join cliente on cliente_venta = id_cliente";
	$campos = "*";
	$sWhere = "( nombre_cliente is null or nombre_cliente like '%" . $query . "%') and ( sucursal_venta like '%" . $p . "%') and estado_venta <> 'Pendiente'";
	//$aidi= "$id";
	//$sWhere.=" order by nombre_internado DESC OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";

	include 'paginationventasr.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
	$per_page = $_REQUEST['per_page']; //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	$count_query   =  mysqli_query($con, "SELECT count(*) AS numrows FROM $tables where $sWhere");
	if ($row = mysqli_fetch_array($count_query)) {
		$numrows = $row['numrows'];
	} else {
		echo mysqli_errors($con);
	}
	$total_pages = ceil($numrows / $per_page);
	//main query to fetch the data
	$query = mysqli_query($con, "SELECT $campos FROM  $tables where $sWhere order by id_venta desc LIMIT $offset,$per_page");
	//loop through fetched data

	if ($numrows > 0) {
?>
		<div class="table-responsive ">
			<table class="table table-hover">
				<thead>
					<tr>
						<th class='text-center'>Nro Venta </th>
						<th class='text-center'>Fecha y Hora </th>
						<th class='text-center'>Tipo </th>
						<th class='text-center'>Cliente </th>
						<th class='text-center'>Sucursal </th>
						<th class='text-center'>Monto </th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($query)) {
						$id_venta = $row['id_venta'];
						$fecha_venta = $row['fechahora_venta'];
						$nombre_sucursal = $row['nombre_sucursal'];
                        $fecha_venta = date("d/m/Y - H:i", strtotime($fecha_venta));
						$cliente_venta = $row['nombre_cliente'];
                        if($cliente_venta == '') $cliente_venta = "Cliente no registrado";
						$monto_venta = $row['monto_venta'];
                        $estado_venta = $row['estado_venta'];
                        $tipo = 'Local';
                        if ($estado_venta == 'Abonada - Virtual') $tipo = 'Virtual';

					?>
						<tr class="<?php echo $text_class; ?>">
							
							<td style="cursor:pointer" onclick="location.href='verVentaRealizada.php?id=<?php echo $id_venta; ?>'" class='text-center' title="Ver Venta"><?php echo $id_venta; ?></td>
							<td style="cursor:pointer" onclick="location.href='verVentaRealizada.php?id=<?php echo $id_venta; ?>'" class='text-center' title="Ver Venta"><?php echo $fecha_venta; ?></td>
							<td style="cursor:pointer" onclick="location.href='verVentaRealizada.php?id=<?php echo $id_venta; ?>'" class='text-center' title="Ver Venta"><?php echo $tipo; ?></td>
							<td style="cursor:pointer" onclick="location.href='verVentaRealizada.php?id=<?php echo $id_venta; ?>'" class='text-center' title="Ver Venta"><?php echo $cliente_venta; ?></td>
							<td style="cursor:pointer" onclick="location.href='verVentaRealizada.php?id=<?php echo $id_venta; ?>'" class='text-center' title="Ver Venta"><?php echo $nombre_sucursal; ?></td>
							<td style="cursor:pointer" onclick="location.href='verVentaRealizada.php?id=<?php echo $id_venta; ?>'" class='text-center' title="Ver Venta"><?php echo "$".$monto_venta; ?></td>							
							<td><a href="verVentaRealizada.php?id=<?php echo $id_venta; ?>"> <i class="fa fa-eye fa-2x" title="Ver Venta"></i> </a></td>
						</tr>
					<?php } ?>
					</tbody>
				</table>
		<div class="table-responsive ">
			<table class="table">
				<tr>
					<td colspan='11' style="z-index: 0;">
						<?php
							$inicios = $offset + 1;
							$finales =  $inicios + $per_page;
							if ($finales > $numrows) $finales = $numrows;
							echo "<br>";
							echo "<br>";
							echo "Mostrando $inicios al $finales de $numrows resultados";
							echo paginate($page, $total_pages, $adjacents);
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>



<?php
	}
}
?>