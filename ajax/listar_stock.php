
<?php
	
	include ("../conexion.php");
	include ("../usuario.php");
	
	
$action = (isset($_REQUEST['action'])&& $_REQUEST['action'] !=NULL)?$_REQUEST['action']:'';
if($action == 'ajax'){ 
	$query = $_REQUEST['query'];
	$p = $_REQUEST['p'];

	
	$tables="stock_sucursal left join producto on stock_sucursal.id_producto = producto.id_producto";
	$campos="*";	
	$sWhere=" producto.nombre_producto LIKE '%".$query."%' and producto.estado_producto = 1 and id_sucursal = $p";
	include 'pagination.php'; 
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page']))?$_REQUEST['page']:1;
	$per_page = $_REQUEST['per_page']; 
	$adjacents  = 4; 
	$offset = ($page - 1) * $per_page;
	$count_query   =  mysqli_query($con,"SELECT count(*) AS numrows FROM $tables  where $sWhere ");
	if ($row= mysqli_fetch_array($count_query)){$numrows = $row['numrows'];}
	else {echo mysqli_error($con);}
	$total_pages = ceil($numrows/$per_page);
	$query = mysqli_query($con,"SELECT $campos FROM  $tables   where $sWhere order by  producto.nombre_producto asc LIMIT $offset,$per_page");


	if ($numrows>0){
	?>
		<div class="table-responsive small">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class='text-center'>ID </th>
						<th class='text-center'>Producto </th>
						<th class='text-center'>Stock </th>
						<th></th>
					</tr>
				</thead>
				<tbody>	
					<?php 
						while($row = mysqli_fetch_array($query)){	
							$id = $row['id'];
							$id_producto=$row['id_producto'];
                            $stock_producto=$row['stock_sucursal'];
                            $nombre_producto=$row['nombre_producto'];
                    ?>
                        <tr>
							<td class='text-center'><?php echo $id_producto;?></td>				
                            <td class='text-center'><?php echo $nombre_producto;?></td>				
                            <td class='text-center'><?php echo $stock_producto;?></td>
							<td>
							<?php if ($perfil_usuario_log != "Vendedor") { ?>
								<div class="dropdown">
									<button class="btn btn-danger dropdown-toggle" data-toggle="dropdown"
                    					aria-haspopup="true" aria-expanded="false">
										<i class="fa fa-check"></i>
									</button>
									
									<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
									
										<li><a class="dropdown-item" href="stockEditar.php?id=<?php echo $id ?>&accion=ingresar"> <i class="fa fa-arrow-right"></i> Ingresar Stock</a></li>
										<?php if($stock_producto >0){ ?>
										<li><a class="dropdown-item" href="stockEditar.php?id=<?php echo $id ?>&accion=retirar"><i class="fa fa-arrow-left"></i> Retirar Stock</a></li>
										<li><a class="dropdown-item" href="stockEditar.php?id=<?php echo $id ?>&accion=cambiosucursal"><i class="fa fa-arrow-circle-up"></i> Enviar Stock a otra Sucursal</a></li>
										<?php } ?>
									</ul>
								</div>
								<?php }?>
							</td>
					<!--	<td><a href="stockEditar.php?id=<?php echo $id;?>"> <i class="fa fa-pencil fa-2x"  title="Editar" ></i> </a></td> -->
                        </tr>
						<?php }?>
						<td colspan='11' style="z-index: 0;">
							<?php
							$inicios = $offset + 1;
							$finales =  $inicios + $per_page ;
							if($finales > $numrows) $finales = $numrows;
							echo "<br>";
							echo "<br>";
							echo "Mostrando $inicios al $finales de $numrows resultados";
							echo paginate($page, $total_pages, $adjacents);
							?>
						</td>
						</tr>
				</tbody>			
			</table>
		</div>	
	<?php	
	}	
}
?>          
		  
