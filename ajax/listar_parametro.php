<?php

include("../conexion.php");
include("../usuario.php");



$action = (isset($_REQUEST['action']) && $_REQUEST['action'] != NULL) ? $_REQUEST['action'] : '';
if ($action == 'ajax') {
	$query = $_REQUEST['query'];
	$v = $_REQUEST['v'];
	$tables = $v;
	if ($v == 'sucursalCaja') {
		$tables = 'sucursal';
		$v2 = 'sucursalCaja';
		$v = 'sucursal';
	} else if ($v == 'clientes') {
		$tables = 'cliente';
		$v2 = 'clientes';
		$v = 'cliente';
	} else if ($v == 'proveedores') {
		$tables = 'proveedor';
		$v2 = 'proveedores';
		$v = 'proveedor';
	}

	$campos = "*";
	$sWhere = " nombre_$v LIKE '%" . $query . "%'";
	include 'paginationParametro.php';
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
	$per_page = $_REQUEST['per_page'];
	$adjacents  = 4;
	$offset = ($page - 1) * $per_page;
	$count_query   =  mysqli_query($con, "SELECT count(*) AS numrows FROM $tables  where $sWhere ");
	if ($row = mysqli_fetch_array($count_query)) {
		$numrows = $row['numrows'];
	} else {
		echo mysqli_error($con);
	}
	$total_pages = ceil($numrows / $per_page);
	$query = mysqli_query($con, "SELECT $campos FROM $tables where $sWhere order by id_$v asc LIMIT $offset,$per_page");

	//me fijo si es un producto para hacer el join a la tabla categoria para traer el nombre de la categoria
	if ($v == 'producto') $query = mysqli_query($con, "SELECT $campos FROM $tables left join categoria on categoria_producto = id_categoria left join subcategoria on subcategoria_producto = id_subcategoria where $sWhere order by id_$v asc LIMIT $offset,$per_page");
	//me fijo si es una sucursal o un proveedor o un cliente para hacer el join a la tabla domicilio para traer el domicilio
	if ($v == 'proveedor' || $v == 'sucursal' || $v2 == 'sucursalCaja' || $v == 'cliente') $query = mysqli_query($con, "SELECT $campos FROM $tables left join domicilio on domicilio_$v = id_domicilio where $sWhere order by id_$v asc LIMIT $offset, $per_page");


	if ($numrows > 0) {
?>
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th class='text-center'>ID </th>
						<?php if ($v == 'producto') { ?>
							<th class='text-center'>Descripción </th>
							<th class='text-center'>Categoria</th>
							<th class='text-center'>Precio</th>
						<?php } else if ($v == 'sucursal' && $v2 != 'sucursalCaja') { ?>
							<th class='text-center'>Descripción </th>
							<th class='text-center'>Domicilio</th>
						<?php } else if ($v2 == 'sucursalCaja') { ?>
							<th class='text-center'>Nombre </th>
							<th class='text-center'>Monto en efectivo</th>
							<th class='text-center'>Monto en cheques</th>
							<th class='text-center'>Monto en crédito</th>
						<?php } else if ($v == 'proveedor' && $v2 != 'proveedores') { ?>
							<th class='text-center'>Razón social</th>
							<th class='text-center'>Apoderado</th>
							<th class='text-center'>DNI del apoderado</th>
							<th class='text-center'>CUIL/CUIT</th>
							<th class='text-center'>Domicilio</th>
							<th class='text-center'>Mail</th>
							<th class='text-center'>Teléfono</th>
						<?php } else if ($v == 'cliente' && $v2 != 'clientes') { ?>
							<th class='text-center'>Nombre/Razón social</th>
							<th class='text-center'>DNI</th>
							<th class='text-center'>CUIL/CUIT</th>
							<th class='text-center'>Telefono</th>
							<th class='text-center'>Mail</th>
							<th class='text-center'>Domicilio</th>
							<th class='text-center'>Condición fiscal</th>
							<th class='text-center'>Tipo</th>
						<?php } else if ($v2 == 'clientes' || $v2 == 'proveedores') { ?>
							<th class='text-center'>Nombre/Razón social</th>
							<th class='text-center'>Deuda actual</th>
						<?php } else { ?>
							<th class='text-center'>Descripción</>
							<?php } ?>
							<th class='text-center'>Estado </th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($query)) {
						$id_parametro = $row['id_' . $v];
						$montoEfectivo_parametro = $row['montoEfectivo_' . $v];
						$montoCheques_parametro = $row['montoCheques_' . $v];
						$montoCredito_parametro = $row['montoCredito_' . $v];
						$nombre_parametro = $row['nombre_' . $v];
						$estado_parametro = $row['estado_' . $v];
						$precio_producto = $row['precio_' . $v];
						$nombre_categoria = $row['nombre_categoria'];
						$apoderado_proveedor = $row['apoderado_proveedor'];
						$dni_proveedor = $row['dni_proveedor'];
						$deuda = $row['deuda_' . $v];
						$calle_domicilio = $row['calle_domicilio'];
						$altura_domicilio = $row['altura_domicilio'];
						$piso_domicilio = $row['piso_domicilio'];
						$depto_domicilio = $row['departamento_domicilio'];
						$cp_domicilio = $row['cp_domicilio'];
						$localidad_domicilio = $row['localidad_domicilio'];
						$provincia_domicilio = $row['provincia_domicilio'];

						if ($v2 != 'sucursalCaja' || (($v2 == 'sucursalCaja' && $sucursal_usuario_log == $id_parametro) || $perfil_usuario_log == "Administrador")) {

							if ($piso_domicilio == "" && $depto_domicilio == "") {
								$domicilio = $calle_domicilio . " " . $altura_domicilio . ", " . $localidad_domicilio . " (CP " . $cp_domicilio . "), " . $provincia_domicilio;
							} else if ($piso_domicilio == "") {
								$domicilio = $calle_domicilio . " " . $altura_domicilio . ", depto. " . $depto_domicilio . ", " . $localidad_domicilio . " (CP " . $cp_domicilio . "), " . $provincia_domicilio;
							} else if ($depto_domicilio == "") {
								$domicilio = $calle_domicilio . " " . $altura_domicilio . ", piso " . $piso_domicilio . ", " . $localidad_domicilio . " (CP " . $cp_domicilio . "), " . $provincia_domicilio;
							} else {
								$domicilio = $calle_domicilio . " " . $altura_domicilio . ", piso " . $piso_domicilio . ", depto. " . $depto_domicilio . ", " . $localidad_domicilio . " (CP " . $cp_domicilio . "), " . $provincia_domicilio;
							}
							$cuil_proveedor = $row['cuil_proveedor'];
							$mail_proveedor = $row['mail_proveedor'];
							$telefono_proveedor = $row['telefono_proveedor'];
							$dni_cliente = $row['dni_cliente'];
							$cuil_cliente = $row['cuil_cliente'];
							$telefono_cliente = $row['telefono_cliente'];
							$mail_cliente = $row['mail_cliente'];
							$condicionFiscal_cliente = $row['condicionFiscal_cliente'];
							$tipo_cliente = $row['tipo_cliente'];
							if ($estado_parametro == '1') {
								$estado_parametro = "Activo";
							} 
							if ($estado_parametro == '2') {
								$estado_parametro = "Moroso";
							}
							if ($estado_parametro == '3') {
								$estado_parametro = "Incobrable";
							}
							if ($estado_parametro == '0') {
								$estado_parametro = "Inactivo";
							}
					?>
							<tr>
								<td class='text-center'><?php echo $id_parametro; ?></td>
								<?php if ($v2 != 'sucursalCaja' && $v2 != 'proveedores' && $v2 != 'clientes') { 	?>
									<td class='text-center'><a href="editarParametro.php?id=<?php echo "$id_parametro"; ?>&parametro=<?php echo "$v"; ?>"> <b> <?php echo $nombre_parametro; ?></b></a></td>
								<?php } else if ($v2 == 'proveedores' || $v2 == 'clientes') { ?>
									<td class='text-center'><a href="cuentaCorriente.php?id=<?php echo "$id_parametro"; ?>&parametro=<?php echo "$v"; ?>"> <b> <?php echo $nombre_parametro; ?></b></a></td>
								<?php } else { ?>
									<td class='text-center'><a href="verSucursal.php?id=<?php echo "$id_parametro"; ?>"> <b> <?php echo $nombre_parametro; ?></b></a></td>
								<?php } ?>
								<?php if ($v == 'producto') { ?>
									<td class='text-center'><?php echo $nombre_categoria; ?></td>
									<td class='text-center'><?php echo "$" . $precio_producto; ?></td>
								<?php } else if ($v == 'sucursal' && $v2 != 'sucursalCaja') { ?>
									<td class='text-center'><?php echo $domicilio; ?></td>
								<?php } else if ($v2 == 'sucursalCaja') { ?>
									<td class='text-center'><?php echo '$' . $montoEfectivo_parametro; ?></td>
									<td class='text-center'><?php echo '$' . $montoCheques_parametro; ?></td>
									<td class='text-center'><?php echo '$' . $montoCredito_parametro; ?></td>
								<?php } else if ($v == 'cliente' && $v2 != 'clientes') { ?>
									<td class='text-center'><?php echo $dni_cliente; ?></td>
									<td class='text-center'><?php echo $cuil_cliente; ?></td>
									<td class='text-center'><?php echo $telefono_cliente; ?></td>
									<td class='text-center'><?php echo $mail_cliente; ?></td>
									<td class='text-center'><?php echo $domicilio; ?></td>
									<td class='text-center'><?php echo $condicionFiscal_cliente; ?></td>
									<td class='text-center'><?php echo $tipo_cliente; ?></td>
								<?php } else if ($v == 'proveedor' && $v2 != 'proveedores') { ?>
									<td class='text-center'><?php echo $apoderado_proveedor; ?></td>
									<td class='text-center'><?php echo $dni_proveedor; ?></td>
									<td class='text-center'><?php echo $cuil_proveedor; ?></td>
									<td class='text-center'><?php echo $domicilio; ?></td>
									<td class='text-center'><?php echo $mail_proveedor; ?></td>
									<td class='text-center'><?php echo $telefono_proveedor; ?></td>
								<?php } else if ($v2 == 'clientes' || $v2 == 'proveedores') { ?>
									<td class='text-center'>$<?php echo $deuda; ?></td>
								<?php } ?>
								<td class='text-center'><?php echo $estado_parametro; ?></td>
						<?php }
					} ?>
							</tr>
							<?php  ?>
							<td colspan='11' style="z-index: 0;">
								<?php
								$inicios = $offset + 1;
								$finales =  $inicios + $per_page;
								if ($finales > $numrows) $finales = $numrows;
								echo "<br>";
								echo "<br>";
								echo "Mostrando $inicios al $finales de $numrows resultados";
								echo paginate($page, $total_pages, $adjacents);
								?>
							</td>
				</tbody>
			</table>
		</div>
<?php

	}
}

?>