<?php

require_once("../conexion.php");

$action = (isset($_REQUEST['action']) && $_REQUEST['action'] != NULL) ? $_REQUEST['action'] : '';
if ($action == 'ajax') {
	$query = $_REQUEST['query'];

	$tables = "usuario";
	$campos = "*";
	$sWhere = " descripcion_usuario like '%" . $query . "%' or nombre_usuario LIKE '%" . $query . "%'";
	//$aidi= "$id";
	//$sWhere.=" order by nombre_internado DESC OFFSET 0 ROWS FETCH NEXT 10 ROWS ONLY";

	include 'paginationusu.php'; //include pagination file
	//pagination variables
	$page = (isset($_REQUEST['page']) && !empty($_REQUEST['page'])) ? $_REQUEST['page'] : 1;
	$per_page = $_REQUEST['per_page']; //how much records you want to show
	$adjacents  = 4; //gap between pages after number of adjacents
	$offset = ($page - 1) * $per_page;
	//Count the total number of row in your table*/
	$count_query   =  mysqli_query($con, "SELECT count(*) AS numrows FROM $tables where $sWhere");
	if ($row = mysqli_fetch_array($count_query)) {
		$numrows = $row['numrows'];
	} else {
		echo mysqli_errors($con);
	}
	$total_pages = ceil($numrows / $per_page);
	//main query to fetch the data
	$query = mysqli_query($con, "SELECT $campos FROM  $tables where $sWhere order by id_usuario asc LIMIT $offset,$per_page");
	//loop through fetched data

	if ($numrows > 0) {
?>
		<div class="table-responsive ">
			<table class="table">
				<thead>
					<tr>
						<th class='text-center'>Id </th>
						<th class='text-center'>Usuario </th>
						<th class='text-center'>Nombre </th>
						<th class='text-center'>Perfil </th>
						<th class='text-center'>Estado </th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php
					while ($row = mysqli_fetch_array($query)) {
						$id_usuario = $row['id_usuario'];
						$nombre_usuario = $row['nombre_usuario'];
						$descripcion_usuario = $row['descripcion_usuario'];
						$perfil_usuario = $row['perfil_usuario'];
						$estado_usuario = $row['estado_usuario'];

					?>
						<tr class="<?php echo $text_class; ?>">
							<td class='text-center'><?php echo $id_usuario; ?></td>
							<td class='text-center'><?php echo $nombre_usuario; ?></td>
							<td class='text-center'><?php echo $descripcion_usuario; ?></td>
							<td class='text-center'><?php echo $perfil_usuario; ?></td>
							<td class='text-center'><?php if ($estado_usuario == 1) echo "Activo";
													else echo "Inactivo"; ?></td>
							<td>
								<a href="usuarioEditar.php?id=<?php echo $id_usuario; ?>"> <i class="fa fa-pencil fa-2x" title="Editar"></i> </a>
								<a href="funciones/blanquearContraseña_funcion.php?id=<?php echo $id_usuario; ?>"> <i class="fa fa-check fa-2x" title="Blanquear Contraseña"></i> </a>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<td colspan='11' style="z-index: 0;">
							<?php
							$inicios = $offset + 1;
							$finales =  $inicios + $per_page;
							if ($finales > $numrows) $finales = $numrows;
							echo "<br>";
							echo "<br>";
							echo "Mostrando $inicios al $finales de $numrows resultados";
							echo paginate($page, $total_pages, $adjacents);
							?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>



<?php
	}
}
?>