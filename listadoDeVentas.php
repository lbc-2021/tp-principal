<?php
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualListadoDeVentas;

//traigo el contenido de la tabla Sucursal para mostrarlo en el combobox
$query = "SELECT * FROM sucursal where estado_sucursal= 1 ORDER BY nombre_sucursal ASC";
$resultado = mysqli_query($con, $query);


/*if($perfil_ventaario_log != 'Cajero'){
  $message="No posee permisos para realizar la acción";
  $class="alert alert-danger";
  header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
} */

//llamo a inicio.php para que incluir la cabecera y los recursos que usamos en la pagina.
include("inicio.php");
?>



<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Ventas</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                   <div class="x_title">
                        <h2>Listado de Ventas Realizadas</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h2>Ventas <b>Realizadas</b></h2>
                                </div>
                                <div class='col-sm-2 pull-right'>
                                </div>
                                <div class="col-md-4 pull-right">
                                    <select class="form-control" name="p" id="p" onchange="loadventasr(1);">
                                    <option value=""> Todas las Sucursales </option>
                                        <?php while ($row = mysqli_fetch_array($resultado)) { ?>
                                            <option value="<?php echo $row['id_sucursal']; ?>"> Sucursal:
                                                <?php echo $row['nombre_sucursal']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class='col-sm-4 pull-right'>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Buscar Nombre de Cliente"
                                            id="q" onkeyup="loadventasr(1);" />
                                        <button class="btn btn-buscar" type="button" onclick="loadventasr(1);">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='clearfixventasr'></div>
                        <div id="resultadosventasr"></div><!-- Carga de datos ajax aqui -->
                        <div class='outer_divventasr'></div><!-- Carga de datos ajax aqui -->
                        <div id="loaderventasr"></div><!-- Carga de datos ajax aqui -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /page content -->

<?php
include("fin.php");
?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Listado de ventas a cobrar");
</script>