<?php //query para traer el contenido de la tabla Sector para ser usando en el ComboBox
include('conexion.php');
include('usuario.php');
include('manual.php');

$texto = $manualVenta;

//query para traer las sucursales para llenar el combobox
$queryClientes = "SELECT * from cliente where estado_cliente >= '1'";
$ejecutarClientes = mysqli_query($con, $queryClientes);


//query para traer las sucursales para llenar el combobox
$queryProductos = "SELECT * from producto left join categoria on id_categoria = categoria_producto left join stock_sucursal on stock_sucursal.id_producto = producto.id_producto where estado_producto = '1' and id_sucursal = $sucursal_usuario_log and nombre_categoria <> 'Pintura' order by nombre_producto asc";
$ejecutarProductos = mysqli_query($con, $queryProductos);
$ejecutarProductos2 = mysqli_query($con, $queryProductos);
$ejecutarProductos3 = mysqli_query($con, $queryProductos);
$ejecutarProductos4 = mysqli_query($con, $queryProductos);
$ejecutarProductos5 = mysqli_query($con, $queryProductos);
$ejecutarProductos6 = mysqli_query($con, $queryProductos);
$ejecutarProductos7 = mysqli_query($con, $queryProductos);
$ejecutarProductos8 = mysqli_query($con, $queryProductos);
$ejecutarProductos9 = mysqli_query($con, $queryProductos);
$ejecutarProductos10 = mysqli_query($con, $queryProductos);
$ejecutarProductos11 = mysqli_query($con, $queryProductos);
$ejecutarProductos12 = mysqli_query($con, $queryProductos);
$ejecutarProductos13 = mysqli_query($con, $queryProductos);
$ejecutarProductos14 = mysqli_query($con, $queryProductos);
$ejecutarProductos15 = mysqli_query($con, $queryProductos);
$ejecutarProductos16 = mysqli_query($con, $queryProductos);
$ejecutarProductos17 = mysqli_query($con, $queryProductos);
$ejecutarProductos18 = mysqli_query($con, $queryProductos);
$ejecutarProductos19 = mysqli_query($con, $queryProductos);
$ejecutarProductos20 = mysqli_query($con, $queryProductos);


$cliente = $_GET['cliente'];

if ($cliente != ''){

$querySelect = "SELECT * from cliente where id_cliente = $cliente";
$resultadoSelect = mysqli_query($con, $querySelect);

    while ($fila = mysqli_fetch_array($resultadoSelect)) {
        $nombre_cliente = $fila['nombre_cliente'];
        $dni_cliente = $fila['dni_cliente'];
    }
}

if($cliente == 0){
    $nombre_cliente = 'Cliente no registrado';
    $dni_cliente = 'No Aplica';
}

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Nueva Venta</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <?php if ($cliente == ''){ ?>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form method="get" novalidate>
                            <span class="section">Buscar Cliente </span> 
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Buscar Cliente por Nombre / Dni<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                <select name="cliente" class="select2_single form-control" data-placeholder="Elige un color" required="required">
                                        <option value="0">Venta a Cliente no registrado</option>
                                        <?php while($row = mysqli_fetch_array($ejecutarClientes)){ ?>
                                        <option value="<?php echo $row['id_cliente']; ?>">
                                            <?php echo $row['nombre_cliente']." (Dni: ".$row['dni_cliente'].")"  ?></option> <!-- PARCHECITO DE -1 -->
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>                            

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Siguiente</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($cliente != ''){ 
        $texto = $manualVentaCliente;?>
        


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div id="main" class="x_content">

                        <form method="post" action="crearPedido.php" novalidate>

                        <input type="hidden" name="cliente" id="cliente" value="<?php echo $cliente; ?>">

                            <h3>Cliente</h3> <hr>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Nombre de Cliente<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control"
                                    name="descripcion" id="descripcion" readonly  value="<?php echo $nombre_cliente; ?>" >
                                </div>
                            </div>     
                            
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Dni de Cliente<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" d
                                    name="descripcion" id="descripcion" readonly  value="<?php echo $dni_cliente; ?>" >
                                </div>
                            </div>   

                            <h3>Pedido</h3> <hr>
                            <label> Inidicar los productos que se incluyen en la venta con su respectiva cantidad.</label> <br>
                            <label><small><font style="color:red">Atención:</font> En caso de que se solicite una cantidad mayor al stock disponible en la sucursal, se otorgará la mayor cantidad posible. </small></label>

                            <div class="field item form-group" id="1">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Producto<span class="required">*</span></label>
                                <div class="col-md-4 col-sm-4">
                                    <select id="producto1" name="producto1" class="form-control" required="required" style="width:100%">
                                        <option value="">Seleccione un producto</option>
                                        <?php while($row = mysqli_fetch_array($ejecutarProductos)){
                                            $stk = $row['stock_sucursal'];
                                            $stk2 = $stk." en stock";
                                            if($stk == 0){ ?>
                                        <option style="color: red !important;" value="<?php echo $row['id_producto']; ?>" disabled>
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (Sin Stock)" ?></option> <!-- PARCHECITO DE -1 -->
                                        <?php } else { ?>
                                            <option value="<?php echo $row['id_producto']; ?>">
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (".$stk2.")" ?></option> 
                                            <?php }
                                            }  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <label class="col-form-label col-md-1 col-sm-1  label-align">Cantidad</label>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <input type="number" class="form-control" name="cantidad1" id="cantidad1" required="required" data-validate-minmax="1,999999999">
                                </div>
                            </div> 

                            <a href="#" onclick="agregarProducto(2)" id= "a2">+ Agregar producto</a>


                            <?php for ($i = 2; $i <= 20; $i++ ){ ?>
                            <div class="field item form-group" id="<?php echo $i?>" style="display: none">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Producto<span class="required">*</span></label>
                                <div class="col-md-4 col-sm-4">
                                    <select id="producto<?php echo $i?>" name="producto<?php echo $i?>" class="form-control" style="width:100%">
                                        <option value="">Seleccione un producto</option>
                                        <?php $queryaux= 'ejecutarProductos'.$i; ?>
                                        <?php while($row = mysqli_fetch_array($$queryaux)){
                                            $stk = $row['stock_sucursal'];
                                            $stk2 = $stk." en stock";
                                            if($stk == 0){ ?>
                                                <option style="color: red !important;" value="<?php echo $row['id_producto']; ?>">
                                                <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (Sin Stock)" ?></option> <!-- PARCHECITO DE -1 -->
                                                <?php } else { ?>
                                                <option value="<?php echo $row['id_producto']; ?>">
                                                <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (".$stk2.")" ?></option> 
                                            <?php }
                                            }  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <label class="col-form-label col-md-1 col-sm-1  label-align">Cantidad</label>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <input type="number" class="form-control"  name="cantidad<?php echo $i?>" id="cantidad<?php echo $i?>" >
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <a onclick="eliminarProducto(<?php echo $i?>)" class="btn btn-danger"><i class="fa fa-ban" style="color:white" aria-hidden="true"></i></a>
                                </div>
                            </div> 

                            <a href="#" onclick="agregarProducto(<?php echo $i+1 ?>) " id= "a<?php echo $i+1?>" style="display:none">+ Agregar producto</a>

                          <?php  }?>


                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Procesar Pedido</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

                            
    <?php  } ?>
    </div>

</div>


<?php include("fin.php");?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Cargar venta local");
</script>