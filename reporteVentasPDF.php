<?php

include('conexion.php');
include('usuario.php');


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$texto = $manualReporteVentas;
$aviso = '';

$querySucursales = "SELECT * from sucursal where estado_sucursal = '1'";
$ejecutarSucursales = mysqli_query($con, $querySucursales);

$queryVendedor = "SELECT * from usuario where estado_usuario = '1'";
$ejecutarVendedor = mysqli_query($con, $queryVendedor);


$disabledBtn = "disabled";

$sucursal = $_GET['sucursal'];
$vendedor = $_GET['vendedor'];
$fecha_fine = $_GET['fecha_fin'];
$fecha_inicioe = $_GET['fecha_inicio'];
$fecha_fin = date("d/m/Y", strtotime($fecha_fine));
$fecha_fin2 = date("Y/m/d", strtotime($fecha_fine));
$fecha_inicio = date("d/m/Y", strtotime($fecha_inicioe));
$fecha_inicio2 = date("Y/m/d", strtotime($fecha_inicioe));


if ($sucursal != ''){
    $querySucursales2 = "SELECT * from sucursal where estado_sucursal = '1' and id_sucursal = $sucursal";
    $ejecutarSucursales2 = mysqli_query($con, $querySucursales2);
    while ($row = mysqli_fetch_array($ejecutarSucursales2)) {
        $nombre_sucursal = $row['nombre_sucursal'];
    } 
}

else { $nombre_sucursal = "Todas las Sucursales";}

$laquery = "SELECT *, a1.descripcion_usuario as vende, a2.descripcion_usuario as cobra from venta left join usuario a1 on vendedor_venta = a1.id_usuario left join usuario a2 on vendedor_venta = a2.id_usuario  left join sucursal on sucursal_venta = id_sucursal  where vendedor_venta like '%$vendedor%' and  sucursal_venta like '%$sucursal%' and fechahora_venta between '$fecha_inicio2' and '$fecha_fin2' order by id_venta desc ";
$ejecutar = mysqli_query($con, $laquery);

$row_count = mysqli_num_rows($ejecutar);
if ($row_count == 0) {
    $aviso = "No hay resultados para mostrar, por favor pruebe con otro periodo";
}





        /////////////////////////////////////////////PDF/////////////////////////////////////////////////////



        require_once __DIR__ . '/vendors/vendorDePDF/autoload.php';

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
        
    
        $mpdf->WriteHTML("<link rel='stylesheet' href='css\maps/custom.css'>"); 
        $mpdf->WriteHTML("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>");    
        $mpdf->WriteHTML("<link rel='stylesheet' href='css\maps/csspdf.css'>");    
        $mpdf->Image('css/logotipo.jpeg', 240, 0, 40, 15, 'jpeg', '', true, false);
     
        $mpdf->WriteHTML("<div class='container'>

        <div class='table-wrapper'>
            <div class='table-title'>
                <div class='row'>
                    <div class='col-sm-12'>
                        <h2><b>Reporte de Ventas de $nombre_sucursal (del $fecha_inicio al $fecha_fin)</b></h2>
                    </div>
                </div>
    
    
            </div>
            <div class='row'>
                <form method='post' action='verRegistroBack.php'>
                    
    
                   
                   
                  
                        <table class='table'>
                        <thead>
                        <tr>                                    
                        <th class='text-center'>#</th>
                        <th class='text-center'>Fecha</th>
                        <th class='text-center'>Tipo</th>
                            <th class='text-center'>Productos</th>
                            <th class='text-center'>Precio Total</th>
                            <th class='text-center'>Vendedor</th>
                            <th class='text-center'>Sucursal</th>
                            <th class='text-center'>Cajero</th>
                            <th class='text-center'>Medio de Pago/Moneda</th>
                        </tr>
                    </thead>
                            <tbody>


                            ");





                            ////////////////////////////////////////////////////////////////////

                            while ($row = mysqli_fetch_array($ejecutar)) {
                                $id_venta = $row['id_venta'];
                                $vendedor_venta = $row['vendedor_venta'];
                                $monto_venta = $row['monto_venta'];
                                $fechahora_venta = $row['fechahora_venta'];
                                $fechahora_venta = date("d/m/Y - H:i", strtotime($fechahora_venta));
                                $estado_venta = $row['estado_venta'];
                                $tipo = "Local";
                                if($estado_venta == 'Abonada - Virtual') $tipo = 'Virtual';
                                $sucursal_venta = $row['sucursal_venta'];
                                $cobrador_venta = $row['cobrador_venta'];
                                $nombre_usuario = $row['vende'];
                                $nombre_usuario2 = $row['cobra'];
                                $nombre_sucursal = $row['nombre_sucursal'];  
                                
                                $queryDetalle = "SELECT * from detalle_venta left join producto on id_producto = producto_detalle where venta_detalle = $id_venta";
                                $ejecutarDetalle = mysqli_query($con, $queryDetalle);


                                $forma='';

                                $medio = "SELECT * from pago_cheque where venta_cheque = $id_venta";
                                $eject = mysqli_query($con, $medio);
                                $row_count = mysqli_num_rows($eject);
                                if ($row_count != 0) {
                                    while ($row = mysqli_fetch_array($eject)) { 
                                    $forma = $forma."<li> Cheque (".$row['moneda_cheque'].")</li>";
                                    }
                                }

                                $medio = "SELECT * from pago_credito where venta_credito = $id_venta";
                                $eject = mysqli_query($con, $medio);
                                $row_count = mysqli_num_rows($eject);
                                if ($row_count != 0) {
                                    while ($row = mysqli_fetch_array($eject)) { 
                                        $forma = $forma."<li> Credito</li>";
                                        }

                                }

                                $medio = "SELECT * from pago_efectivo where venta_efectivo = $id_venta";
                                $eject = mysqli_query($con, $medio);
                                $row_count = mysqli_num_rows($eject);
                                if ($row_count != 0) {
                                    while ($row = mysqli_fetch_array($eject)) { 
                                        $forma = $forma."<li> Efectivo (".$row['moneda_efectivo'].")</li>";
                                        }

                                }

                                $medio = "SELECT * from pago_tarjeta where venta_tarjeta = $id_venta";
                                $eject = mysqli_query($con, $medio);
                                $row_count = mysqli_num_rows($eject);
                                if ($row_count != 0) {
                                    while ($row = mysqli_fetch_array($eject)) { 
                                        $forma = $forma."<li> Tarjeta (".$row['moneda_tarjeta'].")</li>";
                                        }

                                }
                            
                                if ($forma == '') $forma = "<li> Cta Corriente </li>";

                            
                                $mpdf->WriteHTML("



                                <tr>
                                    <td class='text-center'> $id_venta </td>
                                    <td class='text-center'> $fechahora_venta </td>
                                    <td class='text-center'> $tipo </td>
                                    <td > ");
                            
                            
                            
                                    while ($row = mysqli_fetch_array($ejecutarDetalle)) { 
                                        $mpdf->WriteHTML(" <li> ");

                                        $stringAux=  $row['nombre_producto']." x".$row['cantidad_detalle'];

                                        $mpdf->WriteHTML(" $stringAux ");

                                        $mpdf->WriteHTML(" </li> ");

                                    } 
                                    
                                    
                                    $mpdf->WriteHTML(" </td> ");


                                    $mpdf->WriteHTML(" 
                                    
                                    <td class='text-center'> $ $monto_venta </td>
                                    <td class='text-center'> $nombre_usuario  </td>
                                    <td class='text-center'> $nombre_sucursal </td>
                                    <td class='text-center'> $nombre_usuario2 </td>
                                    <td> $forma </td>
                                    </tr>
                                    
                                    
                                    
                                    ");
                            
                            
                            }

                            ////////////////////////////////////////////////////////////////////


                        
                               
    
                                $mpdf->WriteHTML("
                              
    
                            </tbody>
                        </table>
                    
    
    
           
        </div>
    </div>");
    
    
    
    
        $mpdf->Output();
      

        ?>
