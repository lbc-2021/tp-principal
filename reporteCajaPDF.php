<?php

include('conexion.php');
include('usuario.php');
$id_suc = $_POST['sucursal'];

$parametro = $_GET['parametro'];
if ($parametro == 'reporte') {
    $id_suc = $_POST['sucursal'];
} else {
    $id_suc = $_GET['id'];
    $texto = $manualVerSucursal;
}
if ($id_suc != 0)
    $querySucursal = mysqli_query($con, "SELECT * FROM sucursal where id_sucursal = $id_suc");
else
    $querySucursal = mysqli_query($con, "SELECT *, sum(montoEfectivo_sucursal) AS total FROM sucursal");

if ($parametro == 'reporte') {
    $fecha_inicio = $_POST['fecha_inicio'];
    $fecha_fin = $_POST['fecha_fin'];
    $queryACaja = mysqli_query($con, "SELECT *, nombre_usuario FROM auditoria_caja left join usuario ON id_usuario = usuario_acaja where sucursal_acaja = $id_suc AND '$fecha_inicio 00:00:00' < fecha_acaja AND '$fecha_fin 23:59:59' > fecha_acaja ORDER BY fecha_acaja DESC");
} else {
    if ($id_suc != 0)
        $queryACaja = mysqli_query($con, "SELECT *, nombre_usuario FROM auditoria_caja left join usuario ON id_usuario = usuario_acaja where sucursal_acaja = $id_suc ORDER BY fecha_acaja DESC");
    else
        $queryACaja = mysqli_query($con, "SELECT *, nombre_usuario, nombre_sucursal FROM auditoria_caja left join usuario ON id_usuario = usuario_acaja LEFT JOIN sucursal ON id_sucursal = sucursal_acaja ORDER BY fecha_acaja DESC");
}
$max_value = 999999;
while ($row = mysqli_fetch_array($querySucursal)) {
    $nombre_sucursal = $row['nombre_sucursal'];
    if ($id_suc != 0)
        $monto_sucursal = $row['montoEfectivo_sucursal'];
    else
        $monto_sucursal = $row['total'];
    $tope_value = $max_value;
}




        /////////////////////////////////////////////PDF/////////////////////////////////////////////////////



        require_once __DIR__ . '/vendors/vendorDePDF/autoload.php';

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
        
    
        $mpdf->WriteHTML("<link rel='stylesheet' href='css\maps/custom.css'>"); 
        $mpdf->WriteHTML("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>");    
        $mpdf->WriteHTML("<link rel='stylesheet' href='css\maps/csspdf.css'>");    
      //  $mpdf->WriteHTML('<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>');
    
      $mpdf->Image('css/logotipo.jpeg', 240, 0, 40, 15, 'jpeg', '', true, false);
      $mpdf->WriteHTML("<div class='container'>
        <div class='table-wrapper'>
            <div class='table-title'>
                <div class='row'>
                    <div class='col-sm-12'>
                        <h2><b>Reporte de caja diaria:  $nombre_sucursal</b></h2>
                    </div>
                </div>
    
    
            </div>
            <div class='row'>
                <form method='post' action='verRegistroBack.php'>
                    
    
                   
                   
                  
                        <table class='table'>
                            <thead>
                                <tr>
                                <th class='text-center'>ID de la operación</th>   
                                <th class='text-center'>Medio de Pago</th>
                                <th class='text-center'>Monto anterior</th>
                                <th class='text-center'>Monto</th>
                                <th class='text-center'>Monto posterior</th>
                                <th class='text-center'>Responsable</th>
                                <th class='text-center'>Descripción</th>
                                <th class='text-center'>Fecha</th>
                                </tr>
                            </thead>
                            <tbody>


                            ");


                            while ($row = mysqli_fetch_array($queryACaja)) {
                                $id_acaja = $row['id_acaja'];
                                if ($id_suc == 0)
                                    $nombre_sucursal = $row['nombre_sucursal'];
                                    $medio_acaja = $row['medio_acaja'];
                                    $moneda_acaja = $row['moneda_acaja'];
                                $monto_acaja = $row['monto_acaja'];
                                $montoAnterior_acaja = $row['montoAnterior_acaja'];
                                $nombre_usuario = $row['nombre_usuario'];
                                $montoPosterior_acaja = $row['montoPosterior_acaja'];
                                $descripcion_acaja = $row['descripcion_acaja'];
                                $fecha_acaja = $row['fecha_acaja'];
                                $fecha_acaja = date("d/m/Y - H:i", strtotime($fecha_acaja));


                                if ($montoAnterior_acaja < $montoPosterior_acaja){
                                    $signo = '';
                                }else{
                                    $signo = '-';
                                }

                                $mpdf->WriteHTML("
                                <tr>                                   
                                <td><label>$id_acaja</label></td>
                                <td class='text-center'>$medio_acaja/$moneda_acaja</td>
                                <td><label>$montoAnterior_acaja</label></td>
                                <td><label>$signo$monto_acaja</label></td>
                                <td><label>$montoPosterior_acaja</label></td>
                                <td><label>$nombre_usuario</label></td>
                                <td><label>$descripcion_acaja</label></td>
                                <td><label>$fecha_acaja</label></td>

                                

                                </tr>

                                ");



                           
                            
                            }







                               
    
                                $mpdf->WriteHTML("
                              
    
                            </tbody>
                        </table>
                    
    
    
           
        </div>
    </div>");
    
    
    
    
        $mpdf->Output();




        /////////////////////////////////////////////PDF/////////////////////////////////////////////////////
































/*


                /////////////////////////////////////////////PDF/////////////////////////////////////////////////////



                require_once __DIR__ . '/vendors/vendorDePDF/autoload.php';

                $mpdf = new \Mpdf\Mpdf();
                
            
                $mpdf->WriteHTML("<link rel='stylesheet' href='css/custom.css'>"); 
                $mpdf->WriteHTML("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>");    
                $mpdf->WriteHTML("<link rel='stylesheet' href='css/csspdf.css'>");    
              //  $mpdf->WriteHTML('<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>');
            
             
                $mpdf->WriteHTML("
                
                
               
                                    <table class='table table-striped table-hover'>
                                        <thead>
                                            <tr>
                                                <th class='text-center'>ID de la operación</th>
                                                'if ($id_suc == 0) { 
                                                <th class='text-center'>Sucursal</th>
                                                '} 
                                                <th class='text-center'>Monto anterior a la operación</th>
                                                <th class='text-center'>Monto</th>
                                                <th class='text-center'>Monto posterior a la operación</th>
                                                <th class='text-center'>Responsable</th>
                                                <th class='text-center'>Descripción</th>
                                                <th class='text-center'>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            ");

                                            while ($row = mysqli_fetch_array($queryACaja)) {
                                                $id_acaja = $row['id_acaja'];
                                                if ($id_suc == 0)
                                                    $nombre_sucursal = $row['nombre_sucursal'];
                                                $monto_acaja = $row['monto_acaja'];
                                                $montoAnterior_acaja = $row['montoAnterior_acaja'];
                                                $nombre_usuario = $row['nombre_usuario'];
                                                $montoPosterior_acaja = $row['montoPosterior_acaja'];
                                                $descripcion_acaja = $row['descripcion_acaja'];
                                                $fecha_acaja = $row['fecha_acaja'];
                                            
                                                $mpdf->WriteHTML("

                                            <tr class=''echo $text_class; '>
                                                <td class='text-center'>'echo $id_acaja; </td>
                                                'if ($id_suc == 0) { 
                                                <td class='text-center'>'echo $nombre_sucursal; </td>
                                                '} 
                                                '
                                                    $colorTd = '';
                                                    if ($montoAnterior_acaja < $montoPosterior_acaja){
                                                        $colorTd = 'ForestGreen';
                                                    }else{
                                                        $colorTd = 'red';
                                                    } 
                                                <td class='text-center'>$'echo $montoAnterior_acaja; </td>
                                                <td class='text-center' style='color:'echo $colorTd'>
                                                    $'echo $monto_acaja; </td>
                                                <td class='text-center'>$'echo $montoPosterior_acaja; </td>
                                                <td class='text-center'>'echo $nombre_usuario; </td>
                                                <td class='text-center'>'echo $descripcion_acaja; </td>
                                                <td class='text-center'>'echo $fecha_acaja; </td>
                                            </tr>
                                            ");
                                            } 

                                            $mpdf->WriteHTML("
                                        </tbody>
                                    </table>
                               
                
                
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>

</body>
                
                
                ");
            
            
            
            
                $mpdf->Output();




                /////////////////////////////////////////////PDF/////////////////////////////////////////////////////

			*/
            
           
		
		

      

        ?>
