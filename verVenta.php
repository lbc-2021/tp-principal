<?php
$id = $_GET['id'];
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualVerVenta;

/*if($perfil_usuario_log != 'Cajero'){
    $message="No posee permisos para realizar la acción";
    $class="alert alert-danger";
    header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
} */



//Consulto el valor de venta del dolar
$url="https://api-dolar-argentina.herokuapp.com/api/dolaroficial";
$json=file_get_contents($url);
$datos=json_decode($json,true);
$valor_dolar=$datos["venta"];
//$valor_dolar= round($valor_dolar, 2);

//$valor_dolar=round($valor_dolar, 2);

if(count($datos) == 0){
    $valor_dolar = 100.89 ;
    
}

//Consulto el valor de venta del euro
$url="https://api-dolar-argentina.herokuapp.com/api/euro/nacion";
$json=file_get_contents($url);
$datos=json_decode($json,true);
$valor_euro=$datos["venta"];
//$valor_euro= round($valor_euro, 2);

//$valor_euro=round($valor_euro, 2);

if(count($datos) == 0){
    $valor_euro = 117.50;   
}





//traigo el contenido del valor a editar consultando por ID y lo guardo en variables
$query = "SELECT * FROM venta  left join cliente on cliente_venta = id_cliente left join sucursal on sucursal_venta = id_sucursal WHERE id_venta = $id";
$resultado = mysqli_query($con, $query);

while ($fila = mysqli_fetch_array($resultado)) {
    $cliente_venta = $fila['cliente_venta'];
    $nombre_cliente = $fila['nombre_cliente'];
    $nombre_sucursal = $fila['nombre_sucursal'];
    $id_venta = $fila['id_venta'];
    $fechahora_venta = $fila['fechahora_venta'];
    $fechahora_venta = date("d/m/Y - H:i", strtotime($fechahora_venta));
    $estado_venta = $fila['estado_venta'];
    $monto_venta = $fila['monto_venta'];
    $estado_cliente = $fila['estado_cliente'];
}

//echo round($monto_venta/$valor_dolar, 2);

$query2 = "SELECT * FROM detalle_venta left join producto on producto_detalle = id_producto WHERE venta_detalle = $id_venta";
$resultado2 = mysqli_query($con, $query2);

if ($cliente_venta == '0') {
    $nombre_cliente = "Cliente sin registrar";
}

include('inicio.php');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Venta</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                   <div class="x_content">
                        <button class="btn btn-danger" style="float:right" onclick="goBack()">Regresar</button> <br>

                        <form method="post" action="funciones/verVenta_funcion.php" method="POST" novalidate>
                            <span class="section">Completar Datos</span>

                            <input type="hidden" name="id" id="id" value="<?php echo $id_venta; ?>">

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Cliente<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" data-validate-length-range="3" data-validate-words="2" name="nombre" id="nombre" required="required" readonly value="<?php echo $nombre_cliente; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Fecha y Hora de Compra<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="fecha" required="required" readonly value="<?php echo $fechahora_venta; ?>">
                                </div>
                            </div>

                            <input type="hidden" name="dolar" id="dolar" required="required" readonly value="<?php echo $valor_dolar; ?>">
                            <input type="hidden" name="euro" id="euro" required="required" readonly value="<?php echo $valor_euro; ?>">


                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Total a Abonar <span class="required">*</span> $</label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="monto"  required="required" readonly value="<?php echo $monto_venta; ?>" id="total">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Detalle<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <textarea class="form-control" readonly cols="30" rows="10">
<?php while ($fila = mysqli_fetch_array($resultado2)) {
    $nombre_producto = $fila['nombre_producto'];
    $cantidad_detalle = $fila['cantidad_detalle'];
    echo "- " . $nombre_producto . " x" . $cantidad_detalle . "\n";
} ?>
                                </textarea>
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Medio de Pago<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="medio_pago" id="medio_pago" class="form-control" required="required" onchange="mostrarDatosMedioDePago('venta')">
                                        <option value="">Seleccionar medio de pago</option>
                                        <option value="efectivo">Efectivo</option>
                                        <option value="credito">Tarjeta de crédito</option>
                                        <option value="cheque">Cheque</option>
                                        <?php if($cliente_venta != 0 && $estado_cliente != 3 ){ ?>
                                        <option value="ctacorriente">Credito/Cuenta Corriente</option>
                                        <?php } ?>                                    
                                    </select>
                                </div>
                            </div>
                            <div class="field item form-group" id="monedadiv">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Moneda<span class="required" >*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="moneda" id="moneda" class="form-control" onchange="verMoneda()" >                                        
                                        <option value="peso">Peso Argentino</option>
                                        <option value="dolar">Dolar (1 u$s = $<?php echo $valor_dolar?>)</option>
                                        <option value="euro">Euro (1 € = $<?php echo $valor_euro?>)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Dinero pagado por el cliente (en la moneda seleccionada):<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="monto_pagado" id="monto_pagado" onkeyup="verMoneda()" required="required" data-validate-minmax="0,<?php echo $monto_venta ?>" type="number" />
                                    <small> (La diferencia entre el dinero pagado y el total a abonar será sumada a la deuda del cliente.) </small>
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="calculadoradiv">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Conversión a peso </label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="calculadora" id="calculadora" readonly  data-validate-minmax="0,<?php echo $monto_venta ?>" type="text" />
                                    
                                </div>
                            </div>

                            <input type="hidden" id="montomoneda" name="montomoneda">

                            <div hidden class="field item form-group" id="banco_div">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" id="banco_label"></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="banco" id="banco" required="required" type="text" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="compania_div">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Compañía al que pertenece la tarjeta<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="compania" id="compania" required="required" type="text" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="numero_div">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" id="numero_label"><span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="numero" id="numero" required="required" data-validate-minmax="11111111,99999999" type="number" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="numero_transaccion_div">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Número de transacción<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="transaccion" id="transaccion" required="required" data-validate-min="1" type="number" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="fecha_div">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha del cheque<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="fecha" id="fecha" required="required" type="date" />
                                </div>
                            </div>

                            <div hidden class="field item form-group" id="vto_div">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Vencimiento de la Tarjeta (MMAA)<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="vto" id="vto" required="required" data-validate-length-range="4" type="text" />
                                </div>
                            </div>

                            <a href="#" onclick="agregarMedio(1) " id= "agregarmedio" >+ Agregar otro medio de pago</a>


<hr>
<div id="nuevomedio" hidden>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Medio de Pago<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="medio_pago2" id="medio_pago2" class="form-control"  onchange="mostrarDatosMedioDePago2('venta2')">
                                        <option value="">Seleccionar medio de pago</option>
                                        <option value="efectivo">Efectivo</option>
                                        <option value="credito">Tarjeta de crédito</option>
                                        <option value="cheque">Cheque</option>
                                        <?php if($cliente_venta != 0 && $estado_cliente != 3 ){ ?>
                                        <option value="ctacorriente">Credito/Cuenta Corriente</option>
                                        <?php } ?>

                                    </select>
                                </div>
                            </div>
                            <div class="field item form-group" id="monedadiv2">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Moneda<span class="required" >*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select name="moneda2" id="moneda2" class="form-control" onchange="verMoneda2()" >                                        
                                        <option value="peso">Peso Argentino</option>
                                        <option value="dolar">Dolar (1 u$s = $<?php echo $valor_dolar?>)</option>
                                        <option value="euro">Euro (1 € = $<?php echo $valor_euro?>)</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Dinero pagado por el cliente (en la moneda seleccionada):<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="monto_pagado2" onkeyup="verMoneda2()" id="monto_pagado2"  data-validate-minmax="0,<?php echo $monto_venta ?>" type="number" />
                                    <small> (La diferencia entre el dinero pagado y el total a abonar será sumada a la deuda del cliente.) </small>
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="calculadoradiv2">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Conversión a peso </label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="calculadora2" id="calculadora2" readonly data-validate-minmax="0,<?php echo $monto_venta ?>" type="text" />
                                    
                                </div>
                            </div>
                            <input type="hidden" id="montomoneda2" name="montomoneda2">
                            <div hidden class="field item form-group" id="banco_div2">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" id="banco_label2"></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="banco2" id="banco2"  type="text" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="compania_div2">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Compañía al que pertenece la tarjeta<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="compania2" id="compania2" type="text" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="numero_div2">
                                <label class="col-form-label col-md-3 col-sm-3 label-align" id="numero_label2"><span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="numero2" id="numero2" data-validate-minmax="11111111,99999999" type="number" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="numero_transaccion_div2">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Número de transacción<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="transaccion2" id="transaccion2"  data-validate-min="1" type="number" />
                                </div>
                            </div>
                            <div hidden class="field item form-group" id="fecha_div2">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha del cheque<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="fecha2" id="fecha2"  type="date" />
                                </div>
                            </div>

                            <div hidden class="field item form-group" id="vto_div2">
                                <label class="col-form-label col-md-3 col-sm-3 label-align">Vencimiento de la Tarjeta (MMAA)<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="vto2" id="vto2" type="number" />
                                </div>
                            </div>

                            <a href="#" onclick="agregarMedio(0) " id= "agregarmedio" ><i class="fa fa-ban"  data-validate-length-range="4" aria-hidden="true"></i> Eliminar medio de pago agregado</a>

<hr>
</div>

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Abonar Venta</button>
                                    <a href="funciones/cancelarVenta_funcion.php?id=<?php echo $id ?>" style="color: white" onClick="return confirm('¿Estas seguro de querer cancelar la venta? (no se puede deshacer)')" class="btn btn-danger">Cancelar Venta</a>
                                </div>
                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Ver venta");
</script>