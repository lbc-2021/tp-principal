<?php

$id = $_GET['id'];
$parametro = $_GET['parametro'];
include('conexion.php');
include('usuario.php');
include('manual.php');
if($parametro == 'cliente')
    $texto = $manualCuentaCorrienteOperacionesCliente;
else
    $texto = $manualCuentaCorrienteOperacionesProveedor;

$querySelect = "SELECT * from $parametro where id_$parametro = $id";
$resultadoSelect = mysqli_query($con, $querySelect);

while ($fila = mysqli_fetch_array($resultadoSelect)) {
    $nombrev = $fila['nombre_' . $parametro];
    $deuda = $fila['deuda_' . $parametro];
    $limite = $fila['limite_' . $parametro];
}

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cuenta corriente de <?php echo $nombrev ?> (<?php echo $parametro ?>)</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <?php if ($parametro == "cliente") { ?>
                        <span class="section">Cliente Nº <?php echo $id ?></span>
                    <?php } else { ?>
                        <span class="section">Proveedor Nº <?php echo $id ?></span>
                    <?php } ?>

                    <input type="hidden" name="id" value="<?php echo $id ?>">
                    <input type="hidden" name="parametro" value="<?php echo $parametro ?>">

                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Deuda actual</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" name="deuda" id="deuda" readonly value="<?php if ($deuda >= 0) { ?>$<?php echo $deuda ?> <?php } else { ?> $(<?php echo $deuda ?>) <?php } ?>">
                        </div>
                    </div>
                    <div class="field item form-group">
                        <label class="col-form-label col-md-3 col-sm-3 label-align">Límite de la deuda</label>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control" name="limite" id="limite" readonly value="$<?php echo $limite ?>" />
                        </div>
                    </div>
                    <br>
                    <span class="section">Registrar un pago</span>
                    <form method="post" action="funciones/cuentaCorrienteOperaciones_funcion.php" novalidate>
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <input type="hidden" name="parametro" value="<?php echo $parametro ?>">
                        <input type="hidden" name="accion" value="pagar">
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Medio de pago<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <select class="form-control" required name="medio_pago" id="medio_pago" onchange="mostrarDatosMedioDePago()">
                                    <option value="">Seleccionar medio de pago</option>
                                    <option value="efectivo">Efectivo</option>
                                    <option value="cheque">Cheque</option>
                                    <option value="credito">Tarjeta de crédito</option>
                                </select>
                            </div>
                        </div>
                        <div hidden class="field item form-group" id="banco_div">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" id="banco_label"></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="banco" id="banco" required="required" type="text" />
                            </div>
                        </div>
                        <div hidden class="field item form-group" id="compania_div">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Compañía al que pertenece la tarjeta<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="compania" id="compania" required="required" type="text" />
                            </div>
                        </div>
                        <div hidden class="field item form-group" id="numero_div">
                            <label class="col-form-label col-md-3 col-sm-3 label-align" id="numero_label"><span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="numero" id="numero" required="required" data-validate-minmax="11111111,99999999" type="number" />
                            </div>
                        </div>
                        <div hidden class="field item form-group" id="numero_transaccion_div">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Número de transacción<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="transaccion" id="transaccion" required="required" data-validate-min="1" type="number" />
                            </div>
                        </div>
                        <div hidden class="field item form-group" id="fecha_div">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha del cheque<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="fecha" id="fecha" required="required" type="date" />
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Monto<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="monto" id="monto" required="required" data-validate-minmax="0,999999999" type="number" />
                            </div>
                            <button type='submit' class="btn btn-success">Pagar</button>
                        </div>
                    </form>
                    <br>
                    <span class="section">Cambiar límite de cuenta corriente</span>
                    <form method="post" action="funciones/cuentaCorrienteOperaciones_funcion.php">
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <input type="hidden" name="parametro" value="<?php echo $parametro ?>">
                        <input type="hidden" name="accion" value="cambiarLimite">
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Nuevo límite<span class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="nuevoLimite" id="nuevoLimite" required="required" min="0" type="number" />
                            </div>
                            <button type='submit' class="btn btn-success">Cambiar</button>
                        </div>
                    </form>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Operaciones de cuenta corriente");
</script>