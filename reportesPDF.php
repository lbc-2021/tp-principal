<?php

include('conexion.php');
include('usuario.php');


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$parametro = $_GET['parametro'];
$array = [];

if ($parametro == 'sucursal') {
    $query = "SELECT SUM(monto_venta) AS total, nombre_sucursal FROM venta LEFT JOIN sucursal ON sucursal_venta = id_sucursal WHERE estado_venta <> 'Cancelada' AND estado_venta <> 'Pendiente' GROUP BY nombre_sucursal ORDER BY SUM(monto_venta) DESC";
    $aux = 'nombre_sucursal';
    $texto = $manualReporteSucursal;
} else if ($parametro == 'vendedor') {
    $query = "SELECT SUM(monto_venta) AS total, descripcion_usuario FROM venta LEFT JOIN usuario ON vendedor_venta = id_usuario WHERE estado_venta <> 'Cancelada' AND estado_venta <> 'Pendiente' GROUP BY descripcion_usuario ORDER BY SUM(monto_venta) DESC";
    $aux = 'descripcion_usuario';
    $texto = $manualReporteVendedor;
} else if ($parametro == 'stockFaltante'){ 
    $query = "SELECT nombre_producto, nombre_sucursal FROM stock_sucursal LEFT JOIN producto ON stock_sucursal.id_producto = producto.id_producto LEFT JOIN sucursal ON stock_sucursal.id_sucursal = sucursal.id_sucursal WHERE stock_sucursal = 0 AND producto.estado_producto = 1 ORDER BY stock_sucursal.id_sucursal ASC";
    $texto = $manualReporteStockFaltante;
}else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') {
    $texto = $manualReporteStockSeguridad;
    if($parametro == 'stockMinimo' )
        $texto = $manualReporteStockMinimo;
    $aux = $parametro . '_producto';
    $order = 'producto.' . $aux . ' / stock_sucursal';
    $query = "SELECT * FROM stock_sucursal LEFT JOIN producto ON stock_sucursal.id_producto = producto.id_producto LEFT JOIN sucursal ON stock_sucursal.id_sucursal = sucursal.id_sucursal WHERE stock_sucursal <= producto.$aux AND stock_sucursal > 0 AND producto.estado_producto = 1 order by (.$order) desc";
} else if ($parametro == 'cliente'){
    $query = "SELECT * FROM auditoria_cliente LEFT JOIN cliente ON cliente_ac = id_cliente LEFT JOIN usuario ON responsable_ac = id_usuario ORDER BY fecha_ac desc";
    $texto = $manualReporteCliente;
}else if ($parametro == 'compras'){
    $query = "SELECT * FROM auditoria_stock LEFT JOIN sucursal ON sucursal_as = id_sucursal LEFT JOIN usuario ON usuario_as = id_usuario LEFT JOIN producto ON producto_as = id_producto WHERE accion_as = 'Agregar Stock' ORDER BY fecha_as desc";
    $texto = $manualReporteCompras;
}else if ($parametro == 'traspasos'){
    $query = "SELECT * FROM auditoria_stock LEFT JOIN sucursal ON sucursal_as = id_sucursal LEFT JOIN usuario ON usuario_as = id_usuario LEFT JOIN producto ON producto_as = id_producto WHERE accion_as = 'Envio a otra Sucursal' ORDER BY fecha_as desc";
    $texto = $manualReporteTraspasos;
}
if ($parametro == 'sucursal' || $parametro == 'vendedor') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $nombre = $fila[$aux];
        $monto = $fila['total'];
        $datos = [$nombre, $monto];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'stockFaltante') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $nombreProducto = $fila['nombre_producto'];
        $nombreSucursal = $fila['nombre_sucursal'];
        $datos = [$nombreProducto, $nombreSucursal];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $nombreProducto = $fila['nombre_producto'];
        $nombreSucursal = $fila['nombre_sucursal'];
        $stockTotal = $fila[$aux];
        $stockActual = $fila['stock_sucursal'];
        $porcentaje = 100 - number_format(($stockActual / $stockTotal) * 100, 2);
        $datos = [$nombreProducto, $nombreSucursal, $stockTotal, $stockActual, $porcentaje];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'cliente') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $fecha = $fila['fecha_ac'];
        $fecha = date("d/m/Y - H:i", strtotime($fecha));

        $cliente = $fila['nombre_cliente'];
        $accion = $fila['comentario_ac'];
        $responsable = $fila['descripcion_usuario'];
        $datos = [$fecha, $cliente, $accion, $responsable];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'compras') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $fecha = $fila['fecha_as'];
        $fecha = date("d/m/Y - H:i", strtotime($fecha));

        $producto = $fila['nombre_producto'];
        $stockPrevio = $fila['stockPrevio_as'];
        $stockFinal = $fila['stockFinal_as'];
        $sucursal = $fila['nombre_sucursal'];
        $comentario = $fila['comentario_as'];
        $responsable = $fila['descripcion_usuario'];
        $datos = [$fecha, $producto, $stockPrevio, $stockFinal, $sucursal, $comentario, $responsable];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'traspasos') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $fecha = $fila['fecha_as'];
        $fecha = date("d/m/Y - H:i", strtotime($fecha));

        $producto = $fila['nombre_producto'];
        $stockPrevio = $fila['stockPrevio_as'];
        $stockFinal = $fila['stockFinal_as'];
        $sucursal = $fila['nombre_sucursal'];
        $id_sucursal = $fila['sucursalDestino_as'];
        $resultado = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $id_sucursal");
        while ($row = mysqli_fetch_array($resultado))
            $sucursalDestino = $row['nombre_sucursal'];
        $comentario = $fila['comentario_as'];
        $responsable = $fila['descripcion_usuario'];
        $datos = [$fecha, $producto, $stockPrevio, $stockFinal, $sucursal, $sucursalDestino, $comentario, $responsable];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
}





        /////////////////////////////////////////////PDF/////////////////////////////////////////////////////



        require_once __DIR__ . '/vendors/vendorDePDF/autoload.php';

        $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
        
    
        $mpdf->WriteHTML("<link rel='stylesheet' href='css\maps/custom.css'>"); 
        $mpdf->WriteHTML("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'>");    
        $mpdf->WriteHTML("<link rel='stylesheet' href='css\maps/csspdf.css'>");    
    
     
       





                            /////////////////////////////////BODY///////////////////////////////////

                            $mpdf->Image('css/logotipo.jpeg', 240, 0, 40, 15, 'jpeg', '', true, false);


                            $mpdf->WriteHTML(" <br>

                            <div class='right_col' role='main'>
                            <div class=''>
                                <div class='page-title'>
                                    <div class='title_left'> 
                                    
                                    <div class='table-title'>
                                    
                                    
                                    ");
                                    

                                         if ($parametro == 'sucursal' || $parametro == 'vendedor') { 
                                            $mpdf->WriteHTML(" <div class='table-title'><h2><b>Reporte de ventas</b></h2>");
                                         } else if ($parametro == 'stockFaltante') { 
                                            $mpdf->WriteHTML(" <div class='table-title'><h2><b>Reporte de stock</b></h2>");
                                         } else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') { 
                                            $mpdf->WriteHTML(" <div class='table-title'><h2><b>Reporte de ítems con riesgo de stock</b></h2>");
                                         } else if ($parametro == 'cliente') { 
                                            $mpdf->WriteHTML(" <div class='table-title'><h2><b>Reporte de clientes</b></h2>");
                                         } else if ($parametro == 'compras') { 
                                            $mpdf->WriteHTML(" <div class='table-title'><h2><b>Reporte de compras</b></h2>");
                                         } else if ($parametro == 'traspasos') { 
                                            $mpdf->WriteHTML(" <div class='table-title'><h2><b>Reporte de traspasos de stock</b></h2>");
                                         } 
                                         $mpdf->WriteHTML(" </div>
                                    <button type='button' class='btn btn-link' style='float:right' data-toggle='modal' data-target='#exampleModal' title='Ayuda'>
                                        <i class='fa fa-question-circle fa-2x'></i>
                                    </button>
                                </div>
                                <div class='clearfix'></div>
                                <div class='row'>
                                    <div class='col-md-12 col-sm-12'>
                                        <div class='x_panel'>
                                            <div class='table-title'> ");
                                                 if ($parametro == 'sucursal' || $parametro == 'vendedor') { 
                                                    $mpdf->WriteHTML("<h2>Ranking de ventas por $parametro </h2>");
                                                 } else if ($parametro == 'stockFaltante') { 
                                                    $mpdf->WriteHTML("<h2>Listado de stock faltante (stock 0) para ítems activos</h2>");
                                                 } else if ($parametro == 'stockMinimo') { 
                                                    $mpdf->WriteHTML("<h2>Listado de stock en riesgo (según stock mínimo)</h2>");
                                                 } else if ($parametro == 'stockSeguridad') { 
                                                    $mpdf->WriteHTML("<h2>Listado de stock en riesgo (según stock de seguridad)</h2>");
                                                 } else if ($parametro == 'cliente') { 
                                                    $mpdf->WriteHTML("<h2>Listado de operaciones con clientes</h2>");
                                                 } else if ($parametro == 'compras') { 
                                                    $mpdf->WriteHTML("<h2>Listado de compras realizadas</h2>");
                                                 } else if ($parametro == 'traspasos') { 
                                                    $mpdf->WriteHTML("<h2>Listado de traspasos de stock realizados entre sucursales</h2>");
                                                 } 
                                                 $mpdf->WriteHTML("</div>");
                                             if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') { // || $parametro == 'sucursal'
                                            
                                                $mpdf->WriteHTML(" <div style='margin:0 auto ; height:40vh; width:60vw;'>
                                                    <canvas id='myChart' style='width:80%; height:80%'></canvas>
                                                </div> ");
                                             } 
                                             $mpdf->WriteHTML(" <div class='table-responsive'>
                                                <table class='table table-striped table-hover'>
                                                    <thead>
                                                        <tr> ");
                                                             if ($parametro == 'sucursal' || $parametro == 'vendedor') { 
                                                                $mpdf->WriteHTML("<th class='text-center'>Puesto</th>");
                                                                 if ($parametro == 'sucursal') { 
                                                                    $mpdf->WriteHTML("<th class='text-center'>Sucursal</th>");
                                                                 } else if ($parametro == 'vendedor') { 
                                                                    $mpdf->WriteHTML("<th class='text-center'>Vendedor</th>");
                                                                 } 
                                                                 $mpdf->WriteHTML(" <th class='text-center'>Monto</th>");
                                                             } else if ($parametro == 'stockFaltante') { 
                                                                $mpdf->WriteHTML("<th class='text-center'>Producto</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Sucursal</th>");
                                                             } else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') { 
                                                                $mpdf->WriteHTML(" <th class='text-center'>Producto</th>");
                                                                $mpdf->WriteHTML(" <th class='text-center'>Sucursal</th>");
                                                                 if ($parametro == 'stockMinimo') {
                                                                    $headerLabel = 'Stock mínimo'; 
                                                                    $mpdf->WriteHTML(" <th class='text-center'>Stock mínimo</th>");
                                                                 } else {
                                                                    $headerLabel = 'Stock de seguridad'; 
                                                                    $mpdf->WriteHTML(" <th class='text-center'>Stock de seguridad</th>");
                                                                 } 
                                                                 $mpdf->WriteHTML("<th class='text-center'>Stock actual</th> ");
                                                                 $mpdf->WriteHTML("<th class='text-center'>Porcentaje (por debajo)</th>");
                                                             } else if ($parametro == 'cliente') { 
                                                                $mpdf->WriteHTML("<th class='text-center'>Fecha</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Cliente</th>");
                                                                $mpdf->WriteHTML(" <th class='text-center'>Operación</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Responsable</th>");
                                                             } else if ($parametro == 'compras' || $parametro == 'traspasos') { 
                                                                $mpdf->WriteHTML(" <th class='text-center'>Fecha</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Producto</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Stock previo</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Stock posterior</th>");
                                                                $mpdf->WriteHTML("<th class='text-center'>Sucursal</th>");
                                                                 if ($parametro == 'traspasos') { 
                                                                    $mpdf->WriteHTML(" <th class='text-center'>Sucursal de destino</th>");
                                                                 } 
                                                                 $mpdf->WriteHTML("<th class='text-center'>Comentario</th>");
                                                                 $mpdf->WriteHTML("<th class='text-center'>Responsable</th>");
                                                             } 
                                                             $mpdf->WriteHTML(" </tr>
                                                    </thead>
                                                    <tbody> ");
                                                         if ($parametro == 'sucursal' || $parametro == 'vendedor') { 
                                                             $i = 1;
                                                            $datosChart = array();
                                                            $datosLabel = array();
                                                            foreach ($array as $e) { 
                                                                $mpdf->WriteHTML("<tr class='  $text_class '>
                                                                    <td class='text-center'>  $i </td>
                                                                    <td class='text-center'>  $e[0] </td>
                                                                    <td class='text-center'>$  $e[1] </td>
                                                                </tr>");
                                                             $i++;
                                                                array_push($datosLabel, $e[0]);
                                                                array_push($datosChart, intval($e[1]));
                                                            }
                                                        } else if ($parametro == 'stockFaltante' || $parametro == 'cliente' || $parametro == 'compras' || $parametro == 'traspasos') {
                                                            foreach ($array as $e) { 
                                                                $mpdf->WriteHTML(" <tr class=' $text_class '> ");
                                                                     for ($i = 0; $i < count($e); $i++) { 
                                                                        $mpdf->WriteHTML("<td class='text-center'> $e[$i] </td> ");
                                                                     } 
                                                                     $mpdf->WriteHTML("</tr> ");
                                                             }
                                                        } else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') {
                                                            $datosChart = array();
                                                            $datosLabel = array();
                                                            foreach ($array as $e) { 
                                                                $mpdf->WriteHTML("<tr class=' $text_class '>
                                                                    <td class='text-center'>  $e[0] </td>
                                                                    <td class='text-center'>  $e[1] </td>
                                                                    <td class='text-center'>  $e[2] </td>
                                                                    <td class='text-center'>  $e[3] </td>
                                                                    <td class='text-center'>  $e[4] %</td>
                                                                </tr> ");
                                                         array_push($datosLabel, $e[0]);
                                                                array_push($datosChart, intval($e[4]));
                                                            }
                                                        } 
                                                        $mpdf->WriteHTML("   </tbody>
                                                </table>
                                            </div>
                                        </div>
                        
                                     
                                    </div>
                        
                        
                        
                        
                                    </form>
                                </div>
                            </div>
                        </div>


");



                            
                            

                            ////////////////////////////////////////////////////////////////////


                        
                               
    
                                $mpdf->WriteHTML("
                              
    
                            </tbody>
                        </table>
                    
    
    
           
        </div>
    </div>");
    
    
    
    
        $mpdf->Output();
      

        ?>
