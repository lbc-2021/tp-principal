<?php
$id = $_GET['id'];
$accion = $_GET['accion'];
include('conexion.php');
include('usuario.php');
include('manual.php');

$texto = $manualIngresarStock;

if ($accion == 'ingresar') {
    $aux = 'Cantidad a Ingresar';
    $titulo = '<b>Ingresar</b> Stock';
    $texto = $manualIngresarStock;
}
if ($accion == 'retirar') {
    $aux = 'Cantidad a Retirar';
    $titulo = '<b>Retirar</b> Stock';
    $texto = $manualRetirarStock;
}
if ($accion == 'cambiosucursal') {
    $aux = 'Cantidad a Mover';
    $titulo = '<b>Mover Stock</b> de Sucursal';
    $texto = $manualCambioSucursalStock;
}

//traigo el contenido del valor a editar consultando por ID y lo guardo en variables
$query = "SELECT * FROM stock_sucursal 
left join producto on stock_sucursal.id_producto = producto.id_producto 
left join sucursal on stock_sucursal.id_sucursal = sucursal.id_sucursal 
left join proveedor on producto.proveedorPredeterminado_producto = proveedor.id_proveedor
where  id = $id ORDER BY nombre_producto ASC";
$resultado = mysqli_query($con, $query);

while ($fila = mysqli_fetch_array($resultado)) {
    $nombre_producto = $fila['nombre_producto'];
    $nombre_sucursal = $fila['nombre_sucursal'];
    $id_sucursal = $fila['id_sucursal'];
    $stock = $fila['stock_sucursal'];
    $lote = $fila['cantidadLote_producto'];
    $max = $stock;
    if ($accion == 'ingresar') $max = 9999;
    if ($accion == 'ingresar' && $lote != '0') $aux = "Cantidad de Lotes x $lote unidades";
    if ($accion == 'ingresar' && $lote == 'Galon') $aux = "Cantidad de Galones a ingresar";
    if ($accion == 'ingresar' && $lote == 'Litro') $aux = "Cantidad de Litros a ingresar";

    //variables para el envio de mail
    $cantidadASolicitar_producto = $fila['cantidadASolicitar_producto'];
    $nombre_proveedor = $fila['nombre_proveedor'];
    $mail_proveedor = $fila['mail_proveedor'];
    $stockMinimo_producto = $fila['stockMinimo_producto'];
}

//traigo el contenido de la tabla Sucursal para mostrarlo en el combobox
$querySuc = "SELECT * FROM sucursal where estado_sucursal= 1 and id_sucursal <> $id_sucursal ORDER BY nombre_sucursal ASC";
$resultadoSuc = mysqli_query($con, $querySuc);

//traigo el contenido de la tabla Proveedor para mostrarlo en el combobox
$queryProv = "SELECT * FROM proveedor where estado_proveedor= 1 ORDER BY nombre_proveedor ASC";
$resultadoProv = mysqli_query($con, $queryProv);

include('inicio.php');

?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Editar Stock</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                    <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form method="post" action="funciones/stockEditar_funcion.php" novalidate>

                            <input type="hidden" name="accion" value="<?php echo $accion; ?>">
                            <input type="hidden" name="id" value="<?php echo $id; ?>">
                            <input type="hidden" name="lote" value="<?php echo $lote; ?>">
                            <input type="hidden" name="id_sucursal" value="<?php echo $id_sucursal; ?>">
                            <input type="hidden" name="nombre_sucursal" value="<?php echo $nombre_sucursal; ?>">
                            <input type="hidden" name="nombre_producto" value="<?php echo $nombre_producto; ?>">
                            <input type="hidden" name="cantidadASolicitar_producto" value="<?php echo $cantidadASolicitar_producto; ?>">
                            <input type="hidden" name="nombre_proveedor" value="<?php echo $nombre_proveedor; ?>">
                            <input type="hidden" name="mail_proveedor" value="<?php echo $mail_proveedor; ?>">
                            <input type="hidden" name="stockMinimo_producto" value="<?php echo $stockMinimo_producto; ?>">

                            <span class="section"><?php echo $titulo ?></span>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Producto<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" data-validate-length-range="3" data-validate-words="1" name="producto" id="producto" readonly required="required" value="<?php echo $nombre_producto ?>" />
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="sucursal" id="sucursal" readonly required='required' type="text" value="<?php echo $nombre_sucursal ?>" />
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Stock Actual<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="stock" id="stock" readonly required='required' type="number" value="<?php echo $stock ?>" />
                                </div>
                            </div>

                            <?php if ($accion == 'cambiosucursal') { ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal de Destino:<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" name="sucursaldestino" id="sucursaldestino">
                                            <?php while ($row = mysqli_fetch_array($resultadoSuc)) { ?>
                                                <option value="<?php echo $row['id_sucursal']; ?>">
                                                    <?php echo $row['nombre_sucursal']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            <?php } ?>

                            <?php if ($accion == 'ingresar') { ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Proveedor:<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" name="id_proveedor" id="id_proveedor">
                                            <?php while ($row = mysqli_fetch_array($resultadoProv)) { ?>
                                                <option value="<?php echo $row['id_proveedor']; ?>">
                                                    <?php echo $row['nombre_proveedor']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Valor del pedido:<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input onchange="actualizarMax()" class="form-control" name="monto_pedido" id="monto_pedido" required="required" data-validate-minmax="0,999999999" type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Dinero pagado al proveedor:<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="monto_pagado" id="monto_pagado" required="required" data-validate-minmax="0,999999999" type="number" />
                                        <small> (La diferencia entre el dinero pagado al proveedor y el valor del pedido será sumada a la deuda con el proveedor.) </small>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Medio de pago<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" required name="medio_pago" id="medio_pago" onchange="mostrarDatosMedioDePago()">
                                            <option value="">Seleccionar medio de pago</option>
                                            <option value="efectivo">Efectivo</option>
                                            <option value="cheque">Cheque</option>
                                            <option value="credito">Tarjeta de crédito</option>
                                        </select>
                                    </div>
                                </div>
                                <div hidden class="field item form-group" id="banco_div">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" id="banco_label"></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="banco" id="banco" required="required" type="text" />
                                    </div>
                                </div>
                                <div hidden class="field item form-group" id="compania_div">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Compañía al que pertenece la tarjeta<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="compania" id="compania" required="required" type="text" />
                                    </div>
                                </div>
                                <div hidden class="field item form-group" id="numero_div">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align" id="numero_label"><span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="numero" id="numero" required="required" data-validate-minmax="11111111,99999999" type="number" />
                                    </div>
                                </div>
                                <div hidden class="field item form-group" id="numero_transaccion_div">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Número de transacción<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="transaccion" id="transaccion" required="required" data-validate-min="1" type="number" />
                                    </div>
                                </div>
                                <div hidden class="field item form-group" id="fecha_div">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha del cheque<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="fecha" id="fecha" required="required" type="date" />
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align"><?php echo $aux ?><span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="cantidad" id="cantidad" required='required' type="number" data-validate-minmax="1,<?php echo $max; ?>" />
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Observación</label>
                                <div class="col-md-6 col-sm-6">
                                    <textarea class="form-control" name="observacion" cols="30" rows="5" <?php if ($accion == 'cambiosucursal' || $accion == 'retirar') echo "required"; ?>></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Cargar</button>
                                    <button type='reset' class="btn btn-success">Limpiar Campos</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Editar stock");
</script>