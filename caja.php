<?php
$parametro = 'sucursalCaja';
include('conexion.php');
include('usuario.php');
include('manual.php');

$texto = $manualCaja;

// if ($perfil_usuario_log != 'Administrador') {
//     $message = "No posee permisos para realizar la acción";
//     $class = "alert alert-danger";
//     header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
// }

$query = mysqli_query($con, "SELECT * FROM sucursal");

//llamo a inicio.php para que incluir la cabecera y los recursos que usamos en la pagina.
include("inicio.php");
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Gestión de caja</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-7">
                                    <?php if ($perfil_usuario_log == 'Administrador' || $perfil_usuario_log == 'Gerente') { ?>
                                        <h2>Elija una sucursal (<a href="verSucursal.php?id=0" style="color:blue">O CLICK AQUI PARA VER TODAS</a>)</h2>
                                    <?php } else { ?>
                                        <h2>Elija una sucursal</h2>
                                    <?php } ?>
                                </div>
                                <div class='col-sm-5 pull-left'>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Buscar Nombre" id="q" onkeyup="loadparametro(1);" />
                                        <input type="hidden" id="v" value="<?php echo $parametro; ?>" />
                                        <button class="btn btn-buscar" type="button" onclick="loadparametro(1);">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class='clearfixparam'></div>
                        <div class='outer_divparam'></div><!-- Carga de datos ajax aqui -->
                        <div id="loaderparam"></div><!-- Carga de datos ajax aqui -->


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /page content -->

<?php
include("fin.php");
?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Listado de cajas");
</script>