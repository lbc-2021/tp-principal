<?php

include('conexion.php');
include('usuario.php');
include('manual.php');

$parametro = $_GET['parametro'];
$array = [];

if ($parametro == 'sucursal') {
    $query = "SELECT SUM(monto_venta) AS total, nombre_sucursal FROM venta LEFT JOIN sucursal ON sucursal_venta = id_sucursal WHERE estado_venta <> 'Cancelada' AND estado_venta <> 'Pendiente' GROUP BY nombre_sucursal ORDER BY SUM(monto_venta) DESC";
    $aux = 'nombre_sucursal';
    $texto = $manualReporteSucursal;
} else if ($parametro == 'vendedor') {
    $query = "SELECT SUM(monto_venta) AS total, descripcion_usuario FROM venta LEFT JOIN usuario ON vendedor_venta = id_usuario WHERE estado_venta <> 'Cancelada' AND estado_venta <> 'Pendiente' GROUP BY descripcion_usuario ORDER BY SUM(monto_venta) DESC";
    $aux = 'descripcion_usuario';
    $texto = $manualReporteVendedor;
} else if ($parametro == 'stockFaltante'){ 
    $query = "SELECT nombre_producto, nombre_sucursal FROM stock_sucursal LEFT JOIN producto ON stock_sucursal.id_producto = producto.id_producto LEFT JOIN sucursal ON stock_sucursal.id_sucursal = sucursal.id_sucursal WHERE stock_sucursal = 0 AND producto.estado_producto = 1 ORDER BY stock_sucursal.id_sucursal ASC";
    $texto = $manualReporteStockFaltante;
}else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') {
    $texto = $manualReporteStockSeguridad;
    if($parametro == 'stockMinimo' )
        $texto = $manualReporteStockMinimo;
    $aux = $parametro . '_producto';
    $order = 'producto.' . $aux . ' / stock_sucursal';
    $query = "SELECT * FROM stock_sucursal LEFT JOIN producto ON stock_sucursal.id_producto = producto.id_producto LEFT JOIN sucursal ON stock_sucursal.id_sucursal = sucursal.id_sucursal WHERE stock_sucursal <= producto.$aux AND stock_sucursal > 0 AND producto.estado_producto = 1 ";
} else if ($parametro == 'cliente'){
    $query = "SELECT * FROM auditoria_cliente LEFT JOIN cliente ON cliente_ac = id_cliente LEFT JOIN usuario ON responsable_ac = id_usuario ORDER BY fecha_ac desc";
    $texto = $manualReporteCliente;
}else if ($parametro == 'compras'){
    $query = "SELECT * FROM auditoria_stock LEFT JOIN sucursal ON sucursal_as = id_sucursal LEFT JOIN usuario ON usuario_as = id_usuario LEFT JOIN producto ON producto_as = id_producto WHERE accion_as = 'Agregar Stock' ORDER BY fecha_as desc";
    $texto = $manualReporteCompras;
}else if ($parametro == 'traspasos'){
    $query = "SELECT * FROM auditoria_stock LEFT JOIN sucursal ON sucursal_as = id_sucursal LEFT JOIN usuario ON usuario_as = id_usuario LEFT JOIN producto ON producto_as = id_producto WHERE accion_as = 'Envio a otra Sucursal' ORDER BY fecha_as desc";
    $texto = $manualReporteTraspasos;
}

if ($parametro == 'sucursal' || $parametro == 'vendedor') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $nombre = $fila[$aux];
        $monto = $fila['total'];
        $datos = [$nombre, $monto];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'stockFaltante') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $nombreProducto = $fila['nombre_producto'];
        $nombreSucursal = $fila['nombre_sucursal'];
        $datos = [$nombreProducto, $nombreSucursal];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $nombreProducto = $fila['nombre_producto'];
        $nombreSucursal = $fila['nombre_sucursal'];
        $stockTotal = $fila[$aux];
        $stockActual = $fila['stock_sucursal'];
        $porcentaje = 100 - number_format(($stockActual / $stockTotal) * 100, 2);
        $datos = [$nombreProducto, $nombreSucursal, $stockTotal, $stockActual, $porcentaje];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'cliente') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $fecha = $fila['fecha_ac'];
        $fecha = date("d/m/Y - H:i", strtotime($fecha));

        $cliente = $fila['nombre_cliente'];
        $accion = $fila['comentario_ac'];
        $responsable = $fila['descripcion_usuario'];
        $datos = [$fecha, $cliente, $accion, $responsable];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'compras') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $fecha = $fila['fecha_as'];
        $fecha = date("d/m/Y - H:i", strtotime($fecha));

        $producto = $fila['nombre_producto'];
        $stockPrevio = $fila['stockPrevio_as'];
        $stockFinal = $fila['stockFinal_as'];
        $sucursal = $fila['nombre_sucursal'];
        $comentario = $fila['comentario_as'];
        $responsable = $fila['descripcion_usuario'];
        $datos = [$fecha, $producto, $stockPrevio, $stockFinal, $sucursal, $comentario, $responsable];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
} else if ($parametro == 'traspasos') {
    $resultQuery = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultQuery)) {
        $fecha = $fila['fecha_as'];
        $fecha = date("d/m/Y - H:i", strtotime($fecha));

        $producto = $fila['nombre_producto'];
        $stockPrevio = $fila['stockPrevio_as'];
        $stockFinal = $fila['stockFinal_as'];
        $sucursal = $fila['nombre_sucursal'];
        $id_sucursal = $fila['sucursalDestino_as'];
        $resultado = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $id_sucursal");
        while ($row = mysqli_fetch_array($resultado))
            $sucursalDestino = $row['nombre_sucursal'];
        $comentario = $fila['comentario_as'];
        $responsable = $fila['descripcion_usuario'];
        $datos = [$fecha, $producto, $stockPrevio, $stockFinal, $sucursal, $sucursalDestino, $comentario, $responsable];
        if (!in_array($datos, $array)) {
            array_push($array, $datos);
        }
    }
}

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <?php if ($parametro == 'sucursal' || $parametro == 'vendedor') { ?>
                    <h3>Reporte de ventas</h3>
                <?php } else if ($parametro == 'stockFaltante') { ?>
                    <h3>Reporte de stock</h3>
                <?php } else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') { ?>
                    <h3>Reporte de ítems con riesgo de stock</h3>
                <?php } else if ($parametro == 'cliente') { ?>
                    <h3>Reporte de clientes</h3>
                <?php } else if ($parametro == 'compras') { ?>
                    <h3>Reporte de compras</h3>
                <?php } else if ($parametro == 'traspasos') { ?>
                    <h3>Reporte de traspasos de stock</h3>
                <?php } ?>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="table-title">
                        <?php if ($parametro == 'sucursal' || $parametro == 'vendedor') { ?>
                            <h2>Ranking de ventas por <?php echo $parametro ?> (Por monto total de las ventas)</h2>
                        <?php } else if ($parametro == 'stockFaltante') { ?>
                            <h2>Listado de stock faltante (stock 0) para ítems activos</h2>
                        <?php } else if ($parametro == 'stockMinimo') { ?>
                            <h2>Listado de stock en riesgo (según stock mínimo)</h2>
                        <?php } else if ($parametro == 'stockSeguridad') { ?>
                            <h2>Listado de stock en riesgo (según stock de seguridad)</h2>
                        <?php } else if ($parametro == 'cliente') { ?>
                            <h2>Listado de operaciones con clientes</h2>
                        <?php } else if ($parametro == 'compras') { ?>
                            <h2>Listado de compras realizadas</h2>
                        <?php } else if ($parametro == 'traspasos') { ?>
                            <h2>Listado de traspasos de stock realizados entre sucursales</h2>
                        <?php } ?>
                    </div>
                    <?php if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad'  ){
                    ?>
                        <div style="margin:0 auto ; height:40vh; width:60vw;">
                            <canvas id="myChart" style="width:80%; height:80%"></canvas>
                        </div>
                        
                    <?php } ?>

                    <?php if ( $parametro == 'sucursal' || $parametro == 'vendedor'  ){
                    ?>
                        <div style="margin:0 auto ; height:40vh; width:60vw;">
                            <canvas id="myChart" style="width:80%; height:80%"></canvas>
                        </div>
                        
                    <?php } ?>


                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <?php if ($parametro == 'sucursal' || $parametro == 'vendedor') { ?>
                                        <th class='text-center'>Puesto</th>
                                        <?php if ($parametro == 'sucursal') { ?>
                                            <th class='text-center'>Sucursal</th>
                                        <?php } else if ($parametro == 'vendedor') { ?>
                                            <th class='text-center'>Vendedor</th>
                                        <?php } ?>
                                        <th class='text-center'>Monto</th>
                                    <?php } else if ($parametro == 'stockFaltante') { ?>
                                        <th class='text-center'>Producto</th>
                                        <th class='text-center'>Sucursal</th>
                                    <?php } else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') { ?>
                                        <th class='text-center'>Producto</th>
                                        <th class='text-center'>Sucursal</th>
                                        <?php if ($parametro == 'stockMinimo') {
                                            $headerLabel = 'Stock mínimo'; ?>
                                            <th class='text-center'>Stock mínimo</th>
                                        <?php } else {
                                            $headerLabel = 'Stock de seguridad'; ?>
                                            <th class='text-center'>Stock de seguridad</th>
                                        <?php } ?>
                                        <th class='text-center'>Stock actual</th>
                                        <th class='text-center'>Porcentaje (por debajo)</th>
                                    <?php } else if ($parametro == 'cliente') { ?>
                                        <th class='text-center'>Fecha</th>
                                        <th class='text-center'>Cliente</th>
                                        <th class='text-center'>Operación</th>
                                        <th class='text-center'>Responsable</th>
                                    <?php } else if ($parametro == 'compras' || $parametro == 'traspasos') { ?>
                                        <th class='text-center'>Fecha</th>
                                        <th class='text-center'>Producto</th>
                                        <th class='text-center'>Stock previo</th>
                                        <th class='text-center'>Stock posterior</th>
                                        <th class='text-center'>Sucursal</th>
                                        <?php if ($parametro == 'traspasos') { ?>
                                            <th class='text-center'>Sucursal de destino</th>
                                        <?php } ?>
                                        <th class='text-center'>Comentario</th>
                                        <th class='text-center'>Responsable</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($parametro == 'sucursal' || $parametro == 'vendedor') { ?>
                                    <?php $i = 1;
                                    $datosChart = array();
                                    $datosLabel = array();
                                    foreach ($array as $e) { ?>
                                        <tr class="<?php echo $text_class; ?>">
                                            <td class='text-center'><?php echo $i ?></td>
                                            <td class='text-center'><?php echo $e[0] ?></td>
                                            <td class='text-center'>$<?php echo $e[1] ?></td>
                                        </tr>
                                    <?php $i++;
                                        array_push($datosLabel, $e[0]);
                                        array_push($datosChart, intval($e[1]));
                                    }
                                } else if ($parametro == 'stockFaltante' || $parametro == 'cliente' || $parametro == 'compras' || $parametro == 'traspasos') {
                                    foreach ($array as $e) { ?>
                                        <tr class="<?php echo $text_class; ?>">
                                            <?php for ($i = 0; $i < count($e); $i++) { ?>
                                                <td class='text-center'><?php echo $e[$i] ?></td>
                                            <?php } ?>
                                        </tr>
                                    <?php }
                                } else if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad') {
                                    $datosChart = array();
                                    $datosLabel = array();
                                    foreach ($array as $e) { 
                                        if ($e[4] > 0 && $e[3]<= $e[2]) { ?>
                                        <tr class="<?php echo $text_class; ?>">
                                            <td class='text-center'><?php echo $e[0] ?></td>
                                            <td class='text-center'><?php echo $e[1] ?></td>
                                            <td class='text-center'><?php echo $e[2] ?></td>
                                            <td class='text-center'><?php echo $e[3] ?></td>
                                            <td class='text-center'><?php echo $e[4] ?>%</td>
                                        </tr>
                                <?php array_push($datosLabel, $e[0]);
                                        array_push($datosChart, intval($e[4]));
                                    }
                                }
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <form action="reportesPDF.php" method="get">

                    <input type="text" hidden name="parametro" value="<?php echo $parametro; ?>">
                   
                    <button type="submit" style="float:right" class="btn btn-success">Descargar PDF del
                        reporte</button>
                </form>
            </div>




            </form>
        </div>
    </div>
</div>
<!-- /page content -->
<?php include("fin.php"); ?>

<?php if ($parametro == 'stockMinimo' || $parametro == 'stockSeguridad'  ){ 

 include("chartRiesgoStock.php"); }
 ?>


<?php if ( $parametro == 'sucursal' || $parametro == 'vendedor' ){ 

include("chartRankingSucursal.php"); }
?>



<script type="text/javascript">
    <?php if ($parametro == 'sucursal' || $parametro == 'vendedor') { ?>
        window.onload = cambiarTitulo("Reporte de ventas");
    <?php } else if ($parametro == 'stockFaltante' || $parametro == 'stockSeguridad' || $parametro == 'stockMinimo') { ?>
        window.onload = cambiarTitulo("Reporte de stock");
    <?php } else if ($parametro == 'cliente') { ?>
        window.onload = cambiarTitulo("Reporte de clientes");
    <?php } else if ($parametro == 'compras') { ?>
        window.onload = cambiarTitulo("Reporte de compras");
    <?php } else if ($parametro == 'traspasos') { ?>
        window.onload = cambiarTitulo("Reporte de traspasos de stock");
    <?php } ?>
</script>