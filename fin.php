<?php include("modal.php"); ?>

 <!-- footer  -->
 <footer>
    <div class="pull-right">
        Peralta Corp. (Grupo 1)
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer  -->
    </div>
</div>


   	<!-- jquery importado online porque algunas cosas no andaban -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

   	<!-- Validator -->
    <script src="vendors/validator/multifield.js"></script>
    <script src="vendors/validator/validator.js"></script>

  <script> $('#exampleModal').on('shown.bs.modal', function () {
  $('#myInput').trigger('focus')
}) </script>  
    
    <!-- los ojitos para ocultar la password en cambiar contraseña	-->
	<script>
		function hideshow(){
			var password = document.getElementById("password1");
			var slash = document.getElementById("slash");
			var eye = document.getElementById("eye");
			
			if(password.type === 'password'){
				password.type = "text";
				slash.style.display = "block";
				eye.style.display = "none";
			}
			else{
				password.type = "password";
				slash.style.display = "none";
				eye.style.display = "block";
			}
		}
	</script>

    	<script>
		function hideshow2(){
			var password = document.getElementById("passwordv");
			var slash = document.getElementById("slashv");
			var eye = document.getElementById("eyev");
			
			if(password.type === 'password'){
				password.type = "text";
				slash.style.display = "block";
				eye.style.display = "none";
			}
			else{
				password.type = "password";
				slash.style.display = "none";
				eye.style.display = "block";
			}
		}
	</script>

    <script>
        // initialize a validator instance from the "FormValidator" constructor.
        // A "<form>" element is optionally passed as an argument, but is not a must
        var validator = new FormValidator({
            "events": ['blur', 'input', 'change']
        }, document.forms[0]);
        // on form "submit" event
        document.forms[0].onsubmit = function(e) {
            var submit = true,
                validatorResult = validator.checkAll(this);
            console.log(validatorResult);
            return !!validatorResult.valid;
        };
        // on form "reset" event
        document.forms[0].onreset = function(e) {
            validator.reset();
        };
        // stuff related ONLY for this demo page:
        $('.toggleValidationTooltips').change(function() {
            validator.settings.alerts = !this.checked;
            if (this.checked)
                $('form .alert').remove();
        }).prop('checked', false);

    </script>

    <!-- js principal del sistema -->
    <script src="js/main.js" ></script>
	<!-- Switchery -->
	<script src="vendors/switchery/dist/switchery.min.js"></script>
    <!-- jQuery -->
    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- NProgress -->
    <script src="vendors/nprogress/nprogress.js"></script>
    <!-- Chosen -->
    <script src="vendors/chosen/chosen.jquery.js"></script>
    	<!-- Select2 -->
	<script src="vendors/select2/dist/js/select2.full.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".select2_single").select2();
       });
    </script>
    <!-- Custom del tema-->
    <script src="build/js/custom.min.js"></script>
	
  </body>
</html>
