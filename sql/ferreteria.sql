DROP DATABASE IF EXISTS ferreteria;
CREATE DATABASE ferreteria;
USE ferreteria;
set GLOBAL event_scheduler=ON;
--
-- Estructura de tabla para la tabla `auditoria_caja`
--

CREATE TABLE `auditoria_caja` (
  `id_acaja` int(11) NOT NULL,
  `descripcion_acaja` varchar(500) NOT NULL,
  `monto_acaja` double NOT NULL,
  `montoAnterior_acaja` varchar(255) NOT NULL,
  `sucursal_acaja` int(11) NOT NULL,
  `usuario_acaja` int(11) DEFAULT NULL,
  `fecha_acaja` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `montoPosterior_acaja` varchar(255) NOT NULL,
  `moneda_acaja` varchar(255) NOT NULL DEFAULT 'peso',
  `medio_acaja` varchar(255) NOT NULL DEFAULT 'efectivo'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria_caja`
--

INSERT INTO `auditoria_caja` (`id_acaja`, `descripcion_acaja`, `monto_acaja`, `montoAnterior_acaja`, `sucursal_acaja`, `usuario_acaja`, `fecha_acaja`, `montoPosterior_acaja`, `moneda_acaja`, `medio_acaja`) VALUES
(1, 'Se abonó la venta número 5', 3300, '54109', 1, 10, '2021-06-28 01:27:01', '57409', 'peso', 'efectivo'),
(2, 'Se abonó la venta número 4', 95, '95', 1, 10, '2021-06-28 01:27:45', '190', 'dolar', 'efectivo'),
(3, 'Se abonó la venta número 4', 1405.95, '55514.95', 1, 10, '2021-06-28 01:27:45', '56920.9', 'peso', 'efectivo'),
(4, 'Se abonó la venta número 3', 5600, '61114.95', 1, 10, '2021-06-28 01:28:49', '66714.95', 'peso', 'efectivo'),
(5, 'Se abonó la venta número 2', 200, '200', 1, 10, '2021-06-28 01:29:10', '400', 'peso', 'credito'),
(6, 'Se abonó la venta número 1', 4900, '66014.95', 1, 10, '2021-06-28 01:29:17', '70914.95', 'peso', 'efectivo'),
(7, 'Se abonó la venta número 9', 7200, '67200', 2, 10, '2021-06-28 01:32:16', '74400', 'peso', 'efectivo'),
(8, 'Se abonó la venta número 8', 16500, '16500', 2, 10, '2021-06-28 01:32:48', '33000', 'peso', 'cheque'),
(9, 'Se abonó la venta número 7', 40, '40', 2, 10, '2021-06-28 01:33:29', '80', 'dolar', 'efectivo'),
(10, 'Se abonó la venta número 7', 260.4, '67460.4', 2, 10, '2021-06-28 01:33:29', '67720.8', 'peso', 'efectivo'),
(11, 'Se abonó la venta número 6', 4400000, '4467460.4', 2, 10, '2021-06-28 01:33:37', '8867460.4', 'peso', 'efectivo'),
(12, 'Se abonó la venta número 12', 400, '400', 4, 10, '2021-06-28 01:35:44', '800', 'dolar', 'efectivo'),
(13, 'Se abonó la venta número 12', 11264, '111264', 4, 10, '2021-06-28 01:35:44', '122528', 'peso', 'efectivo'),
(14, 'Se abonó la venta número 11', 6600, '117864', 4, 10, '2021-06-28 01:35:51', '124464', 'peso', 'efectivo'),
(15, 'Se abonó la venta número 10', 33500, '33500', 4, 10, '2021-06-28 01:36:08', '67000', 'peso', 'credito'),
(16, 'Se ingresó stock del producto Cutter', 1200, '77209.2', 6, 8, '2021-06-28 09:27:50', '76009.2', 'peso', 'efectivo'),
(17, 'Se ingresó stock del producto Destornillador Phillips', 500, '76009.2', 6, 8, '2021-06-28 09:31:21', '75509.2', 'peso', 'efectivo'),
(18, 'Se ingresó stock del producto Destornillador Phillips', 300, '75509.2', 6, 8, '2021-06-28 09:32:35', '75209.2', 'peso', 'efectivo'),
(19, 'Se ingresó stock del producto Llave Allen', 2400, '75209.2', 6, 8, '2021-06-28 09:33:19', '72809.2', 'peso', 'efectivo'),
(20, 'Se ingresó stock del producto Cutter', 200, '72809.2', 6, 8, '2021-06-28 09:34:44', '72609.2', 'peso', 'efectivo'),
(21, 'Se ingresó stock del producto Llave Allen', 6000, '117864', 4, 8, '2021-06-28 09:37:38', '111864', 'peso', 'efectivo'),
(22, 'Se ingresó stock del producto Pintura Blanca', 5000, '72509.2', 6, 8, '2021-06-28 09:42:20', '67509.2', 'peso', 'efectivo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_cc_clientes`
--

CREATE TABLE `auditoria_cc_clientes` (
  `id_accc` int(11) NOT NULL,
  `fecha_accc` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cliente_accc` int(11) NOT NULL,
  `descripcion_accc` varchar(500) NOT NULL,
  `monto_accc` varchar(255) NOT NULL,
  `montoPrevio_accc` varchar(255) NOT NULL,
  `montoActual_accc` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria_cc_clientes`
--

INSERT INTO `auditoria_cc_clientes` (`id_accc`, `fecha_accc`, `cliente_accc`, `descripcion_accc`, `monto_accc`, `montoPrevio_accc`, `montoActual_accc`) VALUES
(1, '2021-06-28 01:28:49', 4, 'Se abonó la venta número 3', '1000', '0', '1000'),
(2, '2021-06-28 09:10:30', 1, 'Se abonó la venta número 14', '420', '0', '420'),
(3, '2021-06-28 09:11:17', 1, 'Pago de deuda', '420', '420', '0'),
(4, '2021-06-28 09:11:33', 1, 'Cambio de límite', '0', '6000', '6500');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_cc_proveedores`
--

CREATE TABLE `auditoria_cc_proveedores` (
  `id_accp` int(11) NOT NULL,
  `fecha_accp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `proveedor_accp` int(11) NOT NULL,
  `descripcion_accp` varchar(500) NOT NULL,
  `monto_accp` varchar(255) NOT NULL,
  `montoPrevio_accp` varchar(255) NOT NULL,
  `montoActual_accp` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria_cc_proveedores`
--

INSERT INTO `auditoria_cc_proveedores` (`id_accp`, `fecha_accp`, `proveedor_accp`, `descripcion_accp`, `monto_accp`, `montoPrevio_accp`, `montoActual_accp`) VALUES
(1, '2021-06-28 09:34:44', 7, 'Pago parcial de pedido', '100', '40', '140'),
(2, '2021-06-28 09:35:05', 7, 'Pago de deuda', '100', '140', '40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_cliente`
--

CREATE TABLE `auditoria_cliente` (
  `id_ac` int(11) NOT NULL,
  `fecha_ac` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cliente_ac` int(255) NOT NULL,
  `comentario_ac` varchar(500) NOT NULL,
  `responsable_ac` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria_cliente`
--

INSERT INTO `auditoria_cliente` (`id_ac`, `fecha_ac`, `cliente_ac`, `comentario_ac`, `responsable_ac`) VALUES
(1, '2021-06-28 09:27:17', 6, 'Se creó el cliente', 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auditoria_stock`
--

CREATE TABLE `auditoria_stock` (
  `id_as` int(11) NOT NULL,
  `fecha_as` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_as` int(11) NOT NULL,
  `accion_as` varchar(255) NOT NULL,
  `comentario_as` varchar(500) NOT NULL,
  `sucursal_as` int(11) NOT NULL,
  `producto_as` int(11) NOT NULL,
  `sucursalDestino_as` varchar(11) DEFAULT NULL,
  `stockPrevio_as` int(11) NOT NULL,
  `stockFinal_as` int(11) NOT NULL,
  `cantidad_as` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `auditoria_stock`
--

INSERT INTO `auditoria_stock` (`id_as`, `fecha_as`, `usuario_as`, `accion_as`, `comentario_as`, `sucursal_as`, `producto_as`, `sucursalDestino_as`, `stockPrevio_as`, `stockFinal_as`, `cantidad_as`) VALUES
(1, '2021-06-28 02:09:46', 8, 'Envio a otra Sucursal', 'Se mueve a otra sucursal a pedido del Gerente', 2, 4, '6', 1717, 17, 1700),
(2, '2021-06-28 02:11:15', 8, 'Envio a otra Sucursal', 'Test', 1, 3, '6', 998, 98, 900),
(3, '2021-06-28 09:32:35', 8, 'Agregar Stock', 'El stock fue proveído por Black & Decker.\nEl valor del pedido fue $300 y el valor pagado fue $300.', 6, 4, '', 4870, 4980, 2),
(4, '2021-06-28 09:33:19', 8, 'Agregar Stock', 'El stock fue proveído por Black & Decker.\nEl valor del pedido fue $2400 y el valor pagado fue $2400.', 6, 12, '', 0, 20, 20),
(5, '2021-06-28 09:33:44', 8, 'Retirar Stock', 'Se retira a modo de prueba', 6, 4, '', 4980, 4480, 500),
(6, '2021-06-28 09:34:44', 8, 'Agregar Stock', 'El stock fue proveído por Black & Decker.\nEl valor del pedido fue $300 y el valor pagado fue $200.', 6, 10, '', 1384, 1386, 2),
(7, '2021-06-28 09:37:38', 8, 'Agregar Stock', 'El stock fue proveído por El tornillito.\nEl valor del pedido fue $6000 y el valor pagado fue $6000.', 4, 12, '', 0, 200, 200),
(8, '2021-06-28 09:37:59', 8, 'Envio a otra Sucursal', 'Envio de prueba', 6, 3, '3', 11029, 9029, 2000),
(9, '2021-06-28 09:40:59', 8, 'Envio a otra Sucursal', 'Se mueve sucursal a pedido del gerenbte', 6, 3, '1', 9029, 8529, 500),
(10, '2021-06-28 09:41:54', 8, 'Retirar Stock', 'Retiro a pedido del gerente', 4, 2, '', 95, 45, 50),
(11, '2021-06-28 09:42:20', 8, 'Agregar Stock', 'El stock fue proveído por Martillos \"Dedos rotos\".\nEl valor del pedido fue $5000 y el valor pagado fue $5000.', 6, 7, '', 472, 623, 151);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL,
  `nombre_categoria` varchar(255) NOT NULL,
  `estado_categoria` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id_categoria`, `nombre_categoria`, `estado_categoria`) VALUES
(1, 'Herramientas', 1),
(2, 'Pintura', 1),
(3, 'Iluminación', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `id_cliente` int(255) NOT NULL,
  `nombre_cliente` varchar(255) NOT NULL,
  `dni_cliente` int(255) NOT NULL,
  `cuil_cliente` varchar(12) NOT NULL,
  `telefono_cliente` int(255) NOT NULL,
  `mail_cliente` varchar(255) NOT NULL,
  `domicilio_cliente` varchar(255) NOT NULL,
  `condicionFiscal_cliente` varchar(255) NOT NULL,
  `tipo_cliente` varchar(255) NOT NULL,
  `estado_cliente` int(255) NOT NULL,
  `deuda_cliente` varchar(255) NOT NULL DEFAULT '0',
  `limite_cliente` varchar(255) NOT NULL,
  `responsableIva_cliente` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre_cliente`, `dni_cliente`, `cuil_cliente`, `telefono_cliente`, `mail_cliente`, `domicilio_cliente`, `condicionFiscal_cliente`, `tipo_cliente`, `estado_cliente`, `deuda_cliente`, `limite_cliente`, `responsableIva_cliente`) VALUES
(1, 'Alan Britos', 25624501, '40256245010', 1165048924, 'alanbritos@live.com', '14', 'condicion1', 'fisico', 1, '0', '6500', 'Si'),
(2, 'Esteban Quito', 15325712, '60153257120', 1197524820, 'estebanco@gmail.com', '15', 'Consumidor Final', 'fisico', 3, '200', '5000', 'No'),
(3, 'Estela Dronazo', 29657047, '60296570470', 1198750571, 'esteladronazo@hotmail.com', '16', 'Consumidor Final', 'fisico', 3, '100', '1000', 'No'),
(4, 'Juan Carlos Marinelli', 39600810, '40396008100', 1169781235, 'jcmarinelli@hotmail.com', '19', 'condicion1', 'fisico', 1, '1000', '2000', 'No'),
(5, 'Jorge Perez', 12345678, '20123456785', 46647894, 'mailprueba@prueba.com', '20', 'Consumidor Final', 'fisico', 1, '0', '2000', 'Si'),
(6, 'Juan Gonzalez', 36251478, '20362514786', 44640234, 'juan@gonzalez.com', '21', 'Consumidor Final', 'fisico', 1, '0', '2000', 'No');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_venta`
--

CREATE TABLE `detalle_venta` (
  `id_detalle` int(11) NOT NULL,
  `venta_detalle` int(11) NOT NULL,
  `producto_detalle` int(11) NOT NULL,
  `cantidad_detalle` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `detalle_venta`
--

INSERT INTO `detalle_venta` (`id_detalle`, `venta_detalle`, `producto_detalle`, `cantidad_detalle`) VALUES
(1, 1, 4, '43'),
(2, 1, 3, '2'),
(3, 2, 4, '2'),
(4, 3, 1, '33'),
(5, 4, 2, '2'),
(6, 4, 6, '5'),
(7, 5, 4, '33'),
(8, 6, 6, '2200'),
(9, 7, 4, '43'),
(10, 8, 2, '33'),
(11, 9, 3, '24'),
(12, 10, 10, '50'),
(13, 10, 2, '25'),
(14, 11, 3, '22'),
(15, 12, 10, '123'),
(16, 13, 5, '15'),
(17, 13, 4, '20'),
(18, 13, 2, '5'),
(19, 14, 10, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `domicilio`
--

CREATE TABLE `domicilio` (
  `id_domicilio` int(100) NOT NULL,
  `calle_domicilio` varchar(100) NOT NULL,
  `altura_domicilio` int(100) NOT NULL,
  `piso_domicilio` varchar(100) DEFAULT NULL,
  `departamento_domicilio` varchar(100) DEFAULT NULL,
  `cp_domicilio` varchar(100) NOT NULL,
  `localidad_domicilio` varchar(100) NOT NULL,
  `provincia_domicilio` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `domicilio`
--

INSERT INTO `domicilio` (`id_domicilio`, `calle_domicilio`, `altura_domicilio`, `piso_domicilio`, `departamento_domicilio`, `cp_domicilio`, `localidad_domicilio`, `provincia_domicilio`) VALUES
(1, 'Altube', 5241, '', '', '1665', 'José C. Paz', 'Buenos Aires'),
(2, 'Italia', 805, '', '', '1663', 'San Miguel', 'Buenos Aires'),
(3, 'Granaderos a caballo', 1654, '', '', '1744', 'Moreno', 'Buenos Aires'),
(4, 'Rivadavia', 506, '', '', '1846', 'Malvinas Argentinas', 'Buenos Aires'),
(5, 'Belgrano', 1145, '', '', '1642', 'San Isidro', 'Buenos Aires'),
(6, 'Chacabuco', 806, '', '', '1615', 'Grand Bourg', 'Buenos Aires'),
(7, 'Rodolfo Alsina', 256, '', '', '1665', 'José C. Paz', 'Buenos Aires'),
(8, 'La escultura', 1168, '', '', '1744', 'Moreno', 'Buenos Aires'),
(9, 'Presidente Perón', 2051, '', '', '1663', 'San Miguel', 'Buenos Aires'),
(10, 'General Mosconi', 1050, '', '', '1744', 'Moreno', 'Buenos Aires'),
(11, 'Sarmiento', 1337, '1', 'B', '1663', 'San Miguel', 'Buenos Aires'),
(12, 'Laverni', 320, '', '', '1812', 'Tres Arroyos', 'Buenos Aires'),
(13, 'Alem', 406, '', '', '1665', 'José C. Paz', 'Buenos Aires'),
(14, 'Calle falsa', 123, '', '', '1234', 'Berazategui', 'Buenos Aires'),
(15, 'París', 500, '', '', '1744', 'Moreno', 'Buenos Aires'),
(16, 'La música', 1150, '', '', '1744', 'Moreno', 'Buenos Aires'),
(18, 'Samay Huasi', 500, '', '', '1744', 'Moreno', 'Buenos Aires'),
(19, 'Samay Huasi', 503, '', '', '1744', 'Moreno', 'Buenos Aires'),
(20, 'Calle prieba', 123, '', '', '1663', 'San Miguel', 'Buenos Aires'),
(21, 'Calle de Prueba', 123, '', '', '1663', 'San Miguel', 'Buenos Aires');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_cheque`
--

CREATE TABLE `pago_cheque` (
  `id_cheque` int(11) NOT NULL,
  `banco_cheque` varchar(255) NOT NULL,
  `nro_cheque` varchar(255) NOT NULL,
  `fecha_cheque` varchar(255) NOT NULL,
  `monto_cheque` varchar(255) NOT NULL,
  `venta_cheque` int(11) NOT NULL,
  `moneda_cheque` varchar(255) DEFAULT 'peso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pago_cheque`
--

INSERT INTO `pago_cheque` (`id_cheque`, `banco_cheque`, `nro_cheque`, `fecha_cheque`, `monto_cheque`, `venta_cheque`, `moneda_cheque`) VALUES
(1, 'Galicia', '12345678', '2021-06-01', '16500', 8, 'peso'),
(2, 'Galicia', '1111', '10/6/2021', '50', 13, 'peso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_credito`
--

CREATE TABLE `pago_credito` (
  `id_credito` int(11) NOT NULL,
  `monto_credito` varchar(255) NOT NULL,
  `cliente_credito` varchar(255) NOT NULL,
  `venta_credito` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_efectivo`
--

CREATE TABLE `pago_efectivo` (
  `id_efectivo` int(11) NOT NULL,
  `monto_efectivo` varchar(255) NOT NULL,
  `venta_efectivo` int(11) NOT NULL,
  `moneda_efectivo` varchar(255) DEFAULT 'peso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pago_efectivo`
--

INSERT INTO `pago_efectivo` (`id_efectivo`, `monto_efectivo`, `venta_efectivo`, `moneda_efectivo`) VALUES
(1, '3300', 5, 'peso'),
(2, '95', 4, 'dolar'),
(3, '1405.95', 4, 'peso'),
(4, '5600', 3, 'peso'),
(5, '4900', 1, 'peso'),
(6, '7200', 9, 'peso'),
(7, '40', 7, 'dolar'),
(8, '260.4', 7, 'peso'),
(9, '4400000', 6, 'peso'),
(10, '400', 12, 'dolar'),
(11, '11264', 12, 'peso'),
(12, '6600', 11, 'peso'),
(13, '100', 13, 'peso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pago_tarjeta`
--

CREATE TABLE `pago_tarjeta` (
  `id_tarjeta` int(11) NOT NULL,
  `monto_tarjeta` varchar(255) NOT NULL,
  `banco_tarjeta` varchar(255) NOT NULL,
  `compania_tarjeta` varchar(255) NOT NULL,
  `nro_tarjeta` varchar(255) NOT NULL,
  `transaccion_tarjeta` varchar(255) NOT NULL,
  `vto_tarjeta` varchar(255) NOT NULL,
  `venta_tarjeta` int(11) NOT NULL,
  `moneda_tarjeta` varchar(255) DEFAULT 'peso'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pago_tarjeta`
--

INSERT INTO `pago_tarjeta` (`id_tarjeta`, `monto_tarjeta`, `banco_tarjeta`, `compania_tarjeta`, `nro_tarjeta`, `transaccion_tarjeta`, `vto_tarjeta`, `venta_tarjeta`, `moneda_tarjeta`) VALUES
(1, '200', 'Galicia', 'visa', '12345678', '1234', '0222', 2, 'peso'),
(2, '33500', 'Galicia', 'visa', '45678945', '4444', '0221', 10, 'peso');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `id_producto` int(11) NOT NULL,
  `nombre_producto` varchar(255) NOT NULL,
  `precio_producto` varchar(255) NOT NULL,
  `proveedorPredeterminado_producto` varchar(11) DEFAULT NULL,
  `stockMinimo_producto` int(11) DEFAULT '0',
  `estado_producto` int(11) NOT NULL DEFAULT '1',
  `cantidadASolicitar_producto` varchar(11) DEFAULT NULL,
  `categoria_producto` int(11) NOT NULL,
  `cantidadLote_producto` varchar(255) DEFAULT NULL,
  `stockSeguridad_producto` int(11) DEFAULT '0',
  `subcategoria_producto` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`id_producto`, `nombre_producto`, `precio_producto`, `proveedorPredeterminado_producto`, `stockMinimo_producto`, `estado_producto`, `cantidadASolicitar_producto`, `categoria_producto`, `cantidadLote_producto`, `stockSeguridad_producto`, `subcategoria_producto`) VALUES
(1, 'Pinza mango rojo', '200', '3', 100, 1, '100', 1, '50', 50, 1),
(2, 'Sierra fina', '500', '3', 100, 1, '100', 1, '10', 50, 6),
(3, 'Martillo c/ sacaclavos', '300', '5', 150, 1, '200', 1, '200', 75, 10),
(4, 'Destornillador Phillips', '100', '1', 100, 1, '100', 1, '55', 50, 2),
(5, 'Pintura roja', '2100', '0', 0, 1, '0', 2, 'Litro', 0, 9),
(6, 'Tira LED 5 metros', '2000', '2', 75, 1, '100', 3, '50', 50, 5),
(7, 'Pintura Blanca', '900', '0', 0, 1, '0', 2, 'Galon', 0, 9),
(8, 'Pintura verde', '950', '0', 0, 1, '0', 2, 'Galon', 0, 9),
(9, 'Pintura Negra', '585', '0', 0, 1, '0', 2, 'Litro', 0, 9),
(10, 'Cutter', '420', '', 0, 1, '', 1, '1', 0, 6),
(11, 'Llave Inglesa', '400', '', 0, 1, '', 1, '20', 0, 2),
(12, 'Llave Allen', '387', '', 0, 1, '', 1, '1', 0, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `id_proveedor` int(11) NOT NULL,
  `nombre_proveedor` varchar(255) NOT NULL,
  `estado_proveedor` int(11) NOT NULL DEFAULT '1',
  `apoderado_proveedor` varchar(255) NOT NULL,
  `dni_proveedor` int(10) NOT NULL,
  `domicilio_proveedor` int(100) NOT NULL,
  `cuil_proveedor` varchar(12) NOT NULL,
  `mail_proveedor` varchar(255) NOT NULL,
  `telefono_proveedor` int(100) NOT NULL,
  `deuda_proveedor` varchar(255) NOT NULL DEFAULT '0',
  `limite_proveedor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`id_proveedor`, `nombre_proveedor`, `estado_proveedor`, `apoderado_proveedor`, `dni_proveedor`, `domicilio_proveedor`, `cuil_proveedor`, `mail_proveedor`, `telefono_proveedor`, `deuda_proveedor`, `limite_proveedor`) VALUES
(1, 'El tornillito', 1, 'Ramón Chánsez', 20657128, 7, '20206571286', 'eltornillito@gmail.com', 1112341234, '0', '5000'),
(2, 'Los hermanos SA', 1, 'Agustín Rodríguez', 35456123, 8, '40354561235', 'loshermanossa@gmail.com', 1169841250, '5000', '10000'),
(3, 'González & CO', 1, 'Adrián González', 13925704, 9, '50139257041', 'gonzalezyco@hotmail.com', 1196350240, '0', '50000'),
(4, 'Pinturillo SA', 1, 'Leandro Galati', 34325740, 10, '30343257406', 'pinturillo@live.com', 1162043040, '0', '10000'),
(5, 'Martillos \"Dedos rotos\"', 1, 'Marcelo Bonetti', 29789456, 11, '40297894563', 'dedosrotos@hotmail.com', 1145810504, '400', '15000'),
(6, 'Stihl', 1, 'Enrique Chancalai', 13204505, 12, '30132045057', 'stihl@gmail.com', 1160514852, '0', '100000'),
(7, 'Black & Decker', 1, 'Jurman Klinic', 34620490, 13, '10346204906', 'blackanddecker@gmail.com', 1196051750, '40', '500000');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `stock_sucursal`
--

CREATE TABLE `stock_sucursal` (
  `id` int(11) NOT NULL,
  `id_producto` int(11) NOT NULL,
  `id_sucursal` int(11) NOT NULL,
  `stock_sucursal` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `stock_sucursal`
--

INSERT INTO `stock_sucursal` (`id`, `id_producto`, `id_sucursal`, `stock_sucursal`) VALUES
(1, 1, 6, '2936'),
(2, 1, 1, '217'),
(3, 1, 4, '2250'),
(4, 1, 3, '4250'),
(5, 1, 5, '0'),
(6, 1, 2, '3750'),
(7, 2, 6, '19995'),
(8, 2, 1, '1998'),
(9, 2, 4, '45'),
(10, 2, 3, '780'),
(11, 2, 5, '0'),
(12, 2, 2, '3177'),
(13, 3, 6, '8529'),
(14, 3, 1, '568'),
(15, 3, 4, '64178'),
(16, 3, 3, '8400'),
(17, 3, 5, '0'),
(18, 3, 2, '2376'),
(19, 4, 6, '4480'),
(20, 4, 1, '6687'),
(21, 4, 4, '4290'),
(22, 4, 3, '2525'),
(23, 4, 5, '0'),
(24, 4, 2, '17'),
(25, 5, 6, '2029.9'),
(26, 5, 1, '800'),
(27, 5, 4, '75'),
(28, 5, 3, '654'),
(29, 5, 5, '0'),
(30, 5, 2, '245'),
(31, 6, 6, '125'),
(32, 6, 1, '1145'),
(33, 6, 4, '1000'),
(34, 6, 3, '2600'),
(35, 6, 5, '0'),
(36, 6, 2, '50'),
(37, 7, 6, '623.1'),
(38, 7, 1, '370.44'),
(39, 7, 4, '2086.56'),
(40, 7, 3, '2967.3'),
(41, 7, 5, '0'),
(42, 7, 2, '2963.52'),
(43, 8, 6, '555.1'),
(44, 8, 1, '1512'),
(45, 8, 4, '464.94'),
(46, 8, 3, '1723.68'),
(47, 8, 5, '0'),
(48, 8, 2, '2967.3'),
(49, 9, 6, '455'),
(50, 9, 1, '450'),
(51, 9, 4, '53'),
(52, 9, 3, '788'),
(53, 9, 5, '0'),
(54, 9, 2, '786'),
(55, 10, 6, '1386'),
(56, 10, 1, '599'),
(57, 10, 4, '7723'),
(58, 10, 3, '7895'),
(59, 10, 5, '0'),
(60, 10, 2, '1485'),
(61, 11, 6, '0'),
(62, 11, 1, '0'),
(63, 11, 4, '0'),
(64, 11, 3, '0'),
(65, 11, 5, '0'),
(66, 11, 2, '0'),
(67, 12, 6, '20'),
(68, 12, 1, '0'),
(69, 12, 4, '200'),
(70, 12, 3, '0'),
(71, 12, 5, '0'),
(72, 12, 2, '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subcategoria`
--

CREATE TABLE `subcategoria` (
  `id_subcategoria` int(11) NOT NULL,
  `nombre_subcategoria` varchar(255) NOT NULL,
  `categoria_subcategoria` int(11) NOT NULL,
  `estado_subcategoria` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `subcategoria`
--

INSERT INTO `subcategoria` (`id_subcategoria`, `nombre_subcategoria`, `categoria_subcategoria`, `estado_subcategoria`) VALUES
(1, 'Pinzas', 1, 1),
(2, 'Destornilladores', 1, 1),
(3, 'Lamparitas', 3, 1),
(4, 'Tubos fluorescentes', 3, 1),
(5, 'Tiras LED', 3, 1),
(6, 'Sierras y serruchos', 1, 1),
(7, 'Aguarrás', 1, 1),
(8, 'Brochas y rodillos', 1, 1),
(9, 'Interior', 2, 1),
(10, 'Martillos', 1, 1),
(11, 'Exterior', 2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id_sucursal` int(11) NOT NULL,
  `nombre_sucursal` varchar(255) NOT NULL,
  `domicilio_sucursal` int(100) NOT NULL,
  `estado_sucursal` int(11) NOT NULL DEFAULT '1',
  `montoEfectivo_sucursal` varchar(255) NOT NULL DEFAULT '0',
  `montoCheques_sucursal` varchar(255) NOT NULL DEFAULT '0',
  `montoCredito_sucursal` varchar(255) NOT NULL DEFAULT '0',
  `montoEfectivoDolar_sucursal` varchar(255) DEFAULT '0',
  `montoChequesDolar_sucursal` varchar(255) DEFAULT '0',
  `montoCreditoDolar_sucursal` varchar(255) DEFAULT '0',
  `montoEfectivoEuro_sucursal` varchar(255) DEFAULT '0',
  `montoChequesEuro_sucursal` varchar(255) DEFAULT '0',
  `montoCreditoEuro_sucursal` varchar(255) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id_sucursal`, `nombre_sucursal`, `domicilio_sucursal`, `estado_sucursal`, `montoEfectivo_sucursal`, `montoCheques_sucursal`, `montoCredito_sucursal`, `montoEfectivoDolar_sucursal`, `montoChequesDolar_sucursal`, `montoCreditoDolar_sucursal`, `montoEfectivoEuro_sucursal`, `montoChequesEuro_sucursal`, `montoCreditoEuro_sucursal`) VALUES
(1, 'José C. Paz', 1, 1, '66434.95', '0', '200', '95', '0', '0', '0', '0', '0'),
(2, 'San Miguel', 2, 1, '4467460.4', '16500', '0', '40', '0', '0', '0', '0', '0'),
(3, 'Moreno', 3, 1, '40000', '0', '0', '0', '0', '0', '0', '0', '0'),
(4, 'Malvinas Argentinas', 4, 1, '111864', '0', '33500', '400', '0', '0', '0', '0', '0'),
(5, 'San Isidro', 5, 0, '0', '0', '0', '10', '0', '0', '0', '0', '0'),
(6, 'Grand Bourg', 6, 1, '67509.2', '723', '10367', '49', '0', '0', '2', '0', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL,
  `nombre_usuario` varchar(255) NOT NULL,
  `descripcion_usuario` varchar(255) NOT NULL,
  `sucursal_usuario` int(11) DEFAULT NULL,
  `password_usuario` varchar(255) NOT NULL,
  `estado_usuario` int(11) NOT NULL DEFAULT '1',
  `perfil_usuario` varchar(255) NOT NULL,
  `mail_usuario` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id_usuario`, `nombre_usuario`, `descripcion_usuario`, `sucursal_usuario`, `password_usuario`, `estado_usuario`, `perfil_usuario`, `mail_usuario`) VALUES
(1, 'ivansayko', 'Iván Sayko', 4, '$2y$10$B0dqjB9vWXuh/d7Idw.QluzGtd0o2ojyLGfJgqFmp7B2Nnc0XGlQS', 1, 'Administrador', ''),
(2, 'carlosautalan', 'Carlos Autalán', 6, '$2y$10$D6ltQTlfHHMBiurhLicN5eOijruk9xnZNte5a40JgYFNCIPQUzfOe', 1, 'Gerente', 'carlosautalan@gmail.com'),
(3, 'ramirogomez', 'Ramiro Gomez', 2, '$2y$10$zFH4WfdMKNYsETLLqRKxrOmk/6avxV.s8GIpWicCdSGffLRNu6nwa', 1, 'Supervisor', ''),
(4, 'nicolascespede', 'Nicolás Céspede', 3, '$2y$10$P8I5zmZ6RXRV4FtQsPxSzeVgPadzGLc3GmA/nwM6UrYbRorPqj2/6', 1, 'Vendedor', 'nicolascespede@gmail.com'),
(5, 'juancruzmolina', 'Juan Cruz Molina', 1, '$2y$10$sNeKvPC1phZbxN9qlQDmxOMRi5LdE1Ty5azzL/kXkG5T6xQClaQxm', 1, 'Cajero', ''),
(6, 'maximilianoperalta', 'Maximiliano Peralta', 1, '$2y$10$8pff7JgfGA9zgHsrPBfwqe6HTDak.rAFmn.Z9NPLnjZrurDzMPcnG', 0, 'Gerente', 'maximilianoperalta@gmail.com'),
(7, 'franciscoorozco', 'Francisco Orozco', 4, '$2y$10$rcrwDXjMzkW5IiRij7BPyObLLaCGbFXeXvUdc5QVZx32oZJO6avBu', 1, 'Administrador', ''),
(8, 'administrador', 'Administrador default', 6, '$2y$10$NeL6TnHEuAvfvZqUFy8VGO7oWG3I4x3cYzcza4C2QXut7MjLMMZAy', 1, 'Administrador', 'admin@admin.com'),
(9, 'vendedor', 'Vendedor Default', 1, '$2y$10$L1XKoAnptgpCaXUzGAkLduiPNUN0ep5oH83dVnRH8nV2iiURXlAZm', 1, 'Vendedor', 'vendedor@prueba.com'),
(10, 'cajero', 'Cajero Default', 4, '$2y$10$L/4UVA/6uTOX1xpuIeJAZuyX6mC30MczFIU4KSvGPK4s.1Ydgwb/2', 1, 'Cajero', 'cajero@prueba.com'),
(11, 'supervisor', 'Supervisor Default', 2, '$2y$10$.lkqWYaSdV1Rr2l2Xczpted1jZufsGZKyqkJzZ8p.paIOtdshv74G', 1, 'Supervisor', 'supervisor@prueba.com'),
(12, 'gerente', 'Gerente Default', 4, '$2y$10$LDMhmPwpOgAEy8NjaXr5ouZWpIR1n3blVJ7p5Y07GMI4ZhjszJrHO', 1, 'Gerente', 'gerente@prueba.com'),
(13, 'vendedor2', 'Vendedor Default 2', 2, '$2y$10$lDHtCpkcKjjOOzogx.E.heSRjvFVrG3ho484P7IL10xgOfZJgcF.G', 1, 'Vendedor', 'vendedor@prueba.com'),
(14, 'vendedor3', 'Vendedor Default 3', 4, '$2y$10$XzCW7pDfTLxvNwP13cSxNu5voT.wJS5lGg2/fZn3uwHtnyuVraEPS', 1, 'Vendedor', 'vendedor@prueba.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE `venta` (
  `id_venta` int(11) NOT NULL,
  `cliente_venta` int(11) NOT NULL,
  `vendedor_venta` int(11) NOT NULL,
  `monto_venta` int(11) NOT NULL,
  `fechahora_venta` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estado_venta` varchar(255) NOT NULL,
  `sucursal_venta` int(11) NOT NULL,
  `cobrador_venta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`id_venta`, `cliente_venta`, `vendedor_venta`, `monto_venta`, `fechahora_venta`, `estado_venta`, `sucursal_venta`, `cobrador_venta`) VALUES
(1, 0, 9, 4900, '2021-06-28 01:25:21', 'Abonado', 1, 10),
(2, 2, 9, 200, '2021-06-28 01:25:32', 'Abonado', 1, 10),
(3, 4, 9, 6600, '2021-06-28 01:25:39', 'Abonado', 1, 10),
(4, 5, 9, 11000, '2021-06-28 01:25:50', 'Abonado', 1, 10),
(5, 5, 9, 3300, '2021-06-28 01:26:00', 'Abonado', 1, 10),
(7, 0, 13, 4300, '2021-06-28 01:31:12', 'Abonado', 2, 10),
(8, 5, 13, 16500, '2021-06-28 01:31:21', 'Abonado', 2, 10),
(9, 0, 13, 7200, '2021-06-28 01:31:41', 'Abonado', 2, 10),
(10, 2, 14, 33500, '2021-06-28 01:34:20', 'Abonado', 4, 10),
(11, 4, 14, 6600, '2021-06-28 01:34:33', 'Abonado', 4, 10),
(12, 5, 14, 51660, '2021-06-28 01:34:39', 'Abonado', 4, 10),
(13, 3, 8, 150, '2021-06-28 02:08:24', 'Abonada - Virtual', 6, NULL),
(14, 1, 9, 420, '2021-06-28 09:10:17', 'Abonado', 1, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_virtual`
--

CREATE TABLE `venta_virtual` (
  `id_ventaVirtual` int(11) NOT NULL,
  `nro_ventaVirtual` int(11) NOT NULL,
  `fechahora_ventaVirtual` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `usuario_ventaVirtual` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `venta_virtual`
--

INSERT INTO `venta_virtual` (`id_ventaVirtual`, `nro_ventaVirtual`, `fechahora_ventaVirtual`, `usuario_ventaVirtual`) VALUES
(1, 378, '2021-06-28 02:08:24', 8);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auditoria_caja`
--
ALTER TABLE `auditoria_caja`
  ADD PRIMARY KEY (`id_acaja`);

--
-- Indices de la tabla `auditoria_cc_clientes`
--
ALTER TABLE `auditoria_cc_clientes`
  ADD PRIMARY KEY (`id_accc`);

--
-- Indices de la tabla `auditoria_cc_proveedores`
--
ALTER TABLE `auditoria_cc_proveedores`
  ADD PRIMARY KEY (`id_accp`);

--
-- Indices de la tabla `auditoria_cliente`
--
ALTER TABLE `auditoria_cliente`
  ADD PRIMARY KEY (`id_ac`);

--
-- Indices de la tabla `auditoria_stock`
--
ALTER TABLE `auditoria_stock`
  ADD PRIMARY KEY (`id_as`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indices de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  ADD PRIMARY KEY (`id_domicilio`);

--
-- Indices de la tabla `pago_cheque`
--
ALTER TABLE `pago_cheque`
  ADD PRIMARY KEY (`id_cheque`);

--
-- Indices de la tabla `pago_credito`
--
ALTER TABLE `pago_credito`
  ADD PRIMARY KEY (`id_credito`);

--
-- Indices de la tabla `pago_efectivo`
--
ALTER TABLE `pago_efectivo`
  ADD PRIMARY KEY (`id_efectivo`);

--
-- Indices de la tabla `pago_tarjeta`
--
ALTER TABLE `pago_tarjeta`
  ADD PRIMARY KEY (`id_tarjeta`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`id_producto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`id_proveedor`);

--
-- Indices de la tabla `stock_sucursal`
--
ALTER TABLE `stock_sucursal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id_sucursal`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_usuario`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`id_venta`);

--
-- Indices de la tabla `venta_virtual`
--
ALTER TABLE `venta_virtual`
  ADD PRIMARY KEY (`id_ventaVirtual`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auditoria_caja`
--
ALTER TABLE `auditoria_caja`
  MODIFY `id_acaja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de la tabla `auditoria_cc_clientes`
--
ALTER TABLE `auditoria_cc_clientes`
  MODIFY `id_accc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `auditoria_cc_proveedores`
--
ALTER TABLE `auditoria_cc_proveedores`
  MODIFY `id_accp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `auditoria_cliente`
--
ALTER TABLE `auditoria_cliente`
  MODIFY `id_ac` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `auditoria_stock`
--
ALTER TABLE `auditoria_stock`
  MODIFY `id_as` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `id_cliente` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `detalle_venta`
--
ALTER TABLE `detalle_venta`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `domicilio`
--
ALTER TABLE `domicilio`
  MODIFY `id_domicilio` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `pago_cheque`
--
ALTER TABLE `pago_cheque`
  MODIFY `id_cheque` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `pago_credito`
--
ALTER TABLE `pago_credito`
  MODIFY `id_credito` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pago_efectivo`
--
ALTER TABLE `pago_efectivo`
  MODIFY `id_efectivo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `pago_tarjeta`
--
ALTER TABLE `pago_tarjeta`
  MODIFY `id_tarjeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `id_producto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `id_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `stock_sucursal`
--
ALTER TABLE `stock_sucursal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT de la tabla `subcategoria`
--
ALTER TABLE `subcategoria`
  MODIFY `id_subcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id_sucursal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `id_venta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `venta_virtual`
--
ALTER TABLE `venta_virtual`
  MODIFY `id_ventaVirtual` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `update_moroso` ON SCHEDULE EVERY 24 HOUR STARTS '2021-06-26 08:00:23' ON COMPLETION NOT PRESERVE ENABLE DO update cliente set estado_cliente = '2' where (SELECT TIMESTAMPDIFF(DAY, (SELECT fecha_accc from auditoria_cc_clientes where cliente_accc = 1 and descripcion_accc = 'Pago de deuda' order by fecha_accc desc LIMIT 1), CURDATE()))>45 and deuda_cliente > 0 and estado_cliente = '1'$$

CREATE DEFINER=`root`@`localhost` EVENT `update_incobrable` ON SCHEDULE EVERY 24 HOUR STARTS '2021-06-26 08:00:43' ON COMPLETION NOT PRESERVE ENABLE DO update cliente set estado_cliente = '3' where (SELECT TIMESTAMPDIFF(DAY, (SELECT fecha_accc from auditoria_cc_clientes where cliente_accc = 1 and descripcion_accc = 'Pago de deuda' order by fecha_accc desc LIMIT 1), CURDATE()))>90 and deuda_cliente > 0 and estado_cliente = '2'$$

DELIMITER ;