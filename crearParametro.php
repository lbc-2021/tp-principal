<?php
include('conexion.php');
include('usuario.php');
include('manual.php');

$parametro = $_GET['parametro'];

//traigo el contenido de la tabla Proveedor para mostrarlo en el combobox
$query = "SELECT * FROM proveedor where estado_proveedor= 1 ORDER BY nombre_proveedor ASC";
$resultado = mysqli_query($con, $query);

//traigo el contenido de la tabla Categoria para mostrarlo en el combobox
$queryCat = "SELECT * FROM categoria where estado_categoria= 1 ORDER BY nombre_categoria ASC";
$resultadoCat = mysqli_query($con, $queryCat);
include("inicio.php");
if($parametro == 'cliente'){
    $texto = $manualCrearParametroCliente;
}
if($parametro == 'producto'){
    $texto = $manualCrearParametroProducto;
}
if($parametro == 'sucursal'){
    $texto = $manualCrearParametroSucursal;
}
if($parametro == 'proveedor'){
    $texto = $manualCrearParametroProveedor;
}
if($parametro == 'categoria'){
    $texto = $manualCrearParametroCategoria;
}
if($parametro == 'subcategoria'){
    $texto = $manualCrearParametroSubCategoria;
}
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cargar Parametro (<?php echo $parametro ?>)</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                   <div class="x_content">
                        <form method="post" action="funciones/crearParametro_funcion.php?parametro=<?php echo $parametro ?>" novalidate>

                            <span class="section">Completar Datos</span>
                            <div class="field item form-group">

                                <?php if ($parametro != 'proveedor') { ?>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Nombre<span class="required">*</span></label>
                                <?php } else { ?>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Razón Social<span class="required">*</span></label>
                                <?php } ?>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" data-validate-length-range="3" data-validate-words="1" name="nombre" id="nombre" required="required" />
                                </div>
                            </div>


                            <!-- Si es una subcategoria-->
                            <?php if ($parametro == 'subcategoria') { ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Categoria a la que pertenece<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" name="categoria2" id="categoria2" required="required">
                                            <option value="">Seleccione una categoria</option>
                                            <?php while ($row = mysqli_fetch_array($resultadoCat)) { ?>
                                                <option value="<?php echo $row['id_categoria']; ?>">
                                                    <?php echo $row['nombre_categoria']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            <?php  } ?>
                            <!-- Si es una subcategoria-->



                            <!-- Si es un proveedor o sucursal o cliente-->
                            <?php if ($parametro == 'proveedor' || $parametro == 'sucursal' || $parametro == 'cliente') {
                                if ($parametro == 'proveedor') { ?>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Apoderado<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control" data-validate-length-range="6" data-validate-words="1" name="apoderado" id="apoderado" required="required" />
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($parametro == 'proveedor' || $parametro == 'cliente') { ?>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align">DNI<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control" type="number" class='number' name="dni" id="dni" data-validate-length-range="8,8" required='required'>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3 label-align">CUIL/CUIT<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control" name="cuil" id="cuil" required='required' data-validate-length-range="11,11" type="number" />
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($parametro == 'cliente') { ?>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Responsable Iva<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <select <?php echo $read; ?> name="iva" class="form-control" required="required">
                                                <option value="No">No</option>
                                                <option value="Si">Si</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Domicilio:</span></label>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Calle<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="calle" id="calle" required='required' type="text" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Altura<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="altura" id="altura" required='required' type="number" min="1" data-validate-minmax="1,999999999" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Piso</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="piso" id="piso" type="number" min="0" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Departamento</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="depto" id="depto" type="text" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Código postal<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="cp" id="cp" required='required' type="text" pattern="^[A-Za-z0-9_-]*$" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Localidad<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="localidad" id="localidad" required='required' type="text" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3 label-align">Provincia<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="provincia" id="provincia" required='required' type="text" />
                                    </div>
                                </div>
                                <?php if ($parametro == 'proveedor' || $parametro == 'cliente') { ?>

                                    <div class="field item form-group">
                                        <br>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Telefono<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control tel" type="tel" n name="telefono" id="telefono" required='required' data-validate-length-range="8,20" />
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Mail<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control email" name="mail" id="mail" required="required" type="email" />
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align">Límite de cuenta corriente<span class="required">*</span></label>
                                        <div class="col-md-6 col-sm-6">
                                            <input class="form-control" name="limite" id="limite" required='required' type="number" min="0" value="2000" />
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($parametro == 'cliente') { ?>
                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"> Condición fiscal<span class="required">*</span> </label>
                                        <div class="col-md-6 col-sm-6">
                                            <select name="condicionFiscal" class="form-control" required="required">
                                                <option value="Consumidor Final">Consumidor Final</option>
                                                <option value="IVA Responsable Inscripto">IVA Responsable Inscripto</option>
                                                <option value="IVA Responsable no Inscripto">IVA Responsable no Inscripto</option>
                                                <option value="IVA no Responsable">IVA no Responsable</option>
                                                <option value="IVA Sujeto Exento">IVA Sujeto Exento</option>
                                                <option value="Consumidor Final">Consumidor Final</option>
                                                <option value="Responsable Monotributo">Responsable Monotributo</option>
                                                <option value="Sujeto no Categorizado">Sujeto no Categorizado</option>
                                                <option value="Proveedor del Exterior">Proveedor del Exterior</option>
                                                <option value="Cliente del Exterior">Cliente del Exterior</option>
                                                <option value="IVA Liberado – Ley Nº 19.640">IVA Liberado – Ley Nº 19.640</option>
                                                <option value="IVA Responsable Inscripto – Agente de Percepción">IVA Responsable Inscripto – Agente de Percepción</option>
                                                <option value="Pequeño Contribuyente Eventual">Pequeño Contribuyente Eventual</option>
                                                <option value="Monotributista Social">Monotributista Social</option>
                                                <option value="Pequeño Contribuyente Eventual Social">Pequeño Contribuyente Eventual Social</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="field item form-group">
                                        <label class="col-form-label col-md-3 col-sm-3  label-align"> Tipo<span class="required">*</span> </label>
                                        <div class="col-md-6 col-sm-6">
                                            <select name="tipo" class="form-control" required="required">
                                                <option value="fisico">Físico</option>
                                                <option value="juridico">Jurídico</option>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <!-- Fin proveedor o sucursal o cliente-->

                            <!-- Si es un producto -->
                            <?php if ($parametro == 'producto') { ?>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Precio<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="precio" id="precio" required='required' data-validate-minmax="0,999999999" type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Categoria del Producto<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" name="categoria" id="categoria" required="required" onchange="versubcategoria();">
                                            <option value="">Seleccione una categoria</option>
                                            <?php while ($row = mysqli_fetch_array($resultadoCat)) { ?>
                                                <option value="<?php echo $row['id_categoria']; ?>">
                                                    <?php echo $row['nombre_categoria']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" id="subcataux" value="<?php echo $subcategoria_producto ?>">
                                <input type="hidden" id="subcataux2" value="<?php echo $nombre_subcategoria ?>">
                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Subcategoria del Producto<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <div id="myDiv">
                                            <select class="form-control" name="subcategoria" id="subcategoria" required="required" disabled="">
                                                <option value="">Seleccione una Categoria Primero</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="field item form-group" id="lotediv">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Cantidad por lote<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="cantidadLote" id="cantidadLote" required='required' type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group" id="unidaddiv" hidden>
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Unidad de Compra<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" name="unidaddecompra" id="unidaddecompra">
                                            <option value="Galon">Galon (1 Galon = 3,78 Litros)</option>
                                            <option value="Litro">Litro</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align"><?php echo "¿Este producto tiene validación de Stock Minimo? <br><small> (Solicitud automatica por mail al Proveedor/Gerente) </small>" ?> </label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="js-switch" type="checkbox" name="checkstock" id="checkstock" onchange="mostrardivs()">

                                    </div>
                                </div>


                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Proveedor Predeterminado<span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6">
                                        <select class="form-control" required name="proveedor" id="proveedor">
                                            <option value="">Seleccionar Proveedor</option>
                                            <?php while ($row = mysqli_fetch_array($resultado)) { ?>
                                                <option value="<?php echo $row['id_proveedor']; ?>">
                                                    <?php echo $row['nombre_proveedor']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>



                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Stock Minimo</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="stockminimo" id="stockminimo" type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Cantidad a Solicitar</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="cantidadasolicitar" id="cantidadasolicitar" type="number" />
                                    </div>
                                </div>

                                <div class="field item form-group">
                                    <label class="col-form-label col-md-3 col-sm-3  label-align">Stock de Seguridad</label>
                                    <div class="col-md-6 col-sm-6">
                                        <input class="form-control" name="stockseguridad" id="stockseguridad" type="number" />
                                    </div>
                                </div>
                            <?php } ?>
                            <!-- Fin producto -->
                    </div>
                
                    <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <button type='submit' class="btn btn-primary">Cargar</button>
                            <button type='reset' class="btn btn-success">Limpiar Campos</button>
                        </div>
                    </div>
                
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    var par = "<?php echo $parametro ?>";
    if (par === "sucursal" || par === "categoria" || par === "subcategoria") {
        window.onload = cambiarTitulo("Nueva <?php echo $parametro ?>");
    } else {
        window.onload = cambiarTitulo("Nuevo <?php echo $parametro ?>");
    }
</script>