<?php 
include('conexion.php');
include('usuario.php');                    
include('manual.php');
include("inicio.php"); 
$texto = $manualVentaVirtual;
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Ventas Virtuales</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
               <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h4>Carga de archivo .csv</h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                       <p>El archivo a adjuntar debe contener la extension ".csv" y el formato Sucursal / Producto / Cantidad / Cliente / Precio Unitario / Fecha / Medio de Pago</p>
                       <p>Aclaracion: No se lee la cabecera del archivo. Se leerá desde la fila 2 hacia abajo (Se recomienda descargar el ejemplo y modificar el mismo). En caso de faltar stock de un producto, se cargará la cantidad máxima posible</p>
                       <h4>-Click en <a href="formato.csv" style="color:blue">FORMATO</a> para descargar el formato del archivo</h4>
                        <h4>-Click en <a href="ejemplo.csv" style="color:blue">EJEMPLO</a> para descargar un ejemplo</h4>
<hr>

<?php if($sucursal_usuario_log == 6 ) { ?>
  
                      <form method="post" action="funciones/ventaVirtual_funcion.php" method="POST" enctype="multipart/form-data" novalidate>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Adjuntar Archivo .csv<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="file" name="uploadedFile" required="required"  >
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                  <input type="submit" class="btn btn-primary"  name="uploadBtn" value="Procesar Archivo" />
                                </div>
                            </div>
                        </form>
<?php }  else {?>

<h5>Atención: Esta sucursal no admite la carga de Ventas Virtuales. Solo se puede cargar en Grand Bourg (Casa Central)</h5>
<?php }  ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Cargar venta virtual");
</script>