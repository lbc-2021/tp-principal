<?php

include('conexion.php');
include('usuario.php');
include('funciones.php');
include('manual.php');
$texto = $manualModificarPrecio;

//traigo el contenido de la tabla Categoria para mostrarlo en el combobox
$queryCat = "SELECT * FROM categoria where estado_categoria= 1 ORDER BY nombre_categoria ASC";
$resultadoCat = mysqli_query($con, $queryCat);

include('inicio.php');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Modificación Masiva de Precios</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                    <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                    <form method="post" action="funciones/modificarPrecio_funcion.php" novalidate>
                        <span class="section">Completar Datos</span>

                        <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Categoria<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <select class="form-control" name="categoria" id="categoria">
                                        <option value="">Todas las categorias</option>
                                        <?php while($row = mysqli_fetch_array($resultadoCat)) { ?>
			                            <option value="<?php echo $row['id_categoria']; ?>"><?php echo $row['nombre_categoria']; ?></option>
				                        <?php } ?>
                                    </select>
                                </div>
                            </div>



                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Accion<span
                                        class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="accion" id="accion1" value="aumento" checked>
                                        <label class="form-check-label" for="accion1">Aumentar</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="accion" id="accion2" value="descuento">
                                        <label class="form-check-label" for="accion2">Descontar</label>
                                    </div> 
                                </div>
                            </div>


                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Valor de Modificación<span
                                        class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input class="form-control" name="monto" id="monto" required="required" data-validate-minmax="0,999999999"
                                         type="number" />
                                </div>
                            </div>

                            <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Forma<span
                                        class="required">*</span></label> 
                                <div class="col-md-6 col-sm-6">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="forma" id="forma1" value="fijo" checked>
                                        <label class="form-check-label" for="forma1">Monto fijo</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="forma" id="forma2" value="porcentaje">
                                        <label class="form-check-label" for="forma2">% Porcentaje</label>
                                    </div>                  
                                </div> 
                            </div> 


                            <div class="form-group">
                        <div class="col-md-6 offset-md-3">
                            <button type='submit' class="btn btn-primary">Cargar</button>
                            <button type='reset' class="btn btn-success">Limpiar Campos</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Modificar precios");
</script>

