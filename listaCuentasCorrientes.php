<?php
//traigo el parametro que se manda por GET
$parametro = $_GET['parametro'];
include('usuario.php');
include('manual.php');
if($parametro == 'clientes')
    $texto = $manualCuentaCorrienteClientes;
else
    $texto = $manualCuentaCorrienteProveedores;
//llamo a inicio.php para que incluir la cabecera y los recursos que usamos en la pagina.
include("inicio.php");
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cuentas corrientes de <?php echo $parametro ?></h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Listado</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                </div>
                                <div class='col-sm-2 pull-right'>
                                </div>
                                <div class='col-sm-4 pull-right'>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Buscar Nombre" id="q" onkeyup="loadparametro(1);" />
                                        <input type="hidden" id="v" value="<?php echo $parametro; ?>" />
                                        <button class="btn btn-buscar" type="button" onclick="loadparametro(1);">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class='clearfixparam'></div>
                            <div id="resultadosparam"></div><!-- Carga de datos ajax aqui -->
                            <div class='outer_divparam'></div><!-- Carga de datos ajax aqui -->
                            <div id="loaderparam"></div><!-- Carga de datos ajax aqui -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php
include("fin.php");
?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Listado de cuentas corrientes");
</script>