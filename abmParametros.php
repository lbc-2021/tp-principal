<?php
//traigo el parametro que se manda por GET
$parametro = $_GET['parametro'];
include('usuario.php');
include('manual.php');

if($parametro == 'cliente'){
    $texto = $manualABMParametroCliente;
}
if($parametro == 'producto'){
    $texto = $manualABMParametroProducto;
}
if($parametro == 'sucursal'){
    $texto = $manualABMParametroSucursal;
}
if($parametro == 'proveedor'){
    $texto = $manualABMParametroProveedor;
}
if($parametro == 'categoria'){
    $texto = $manualABMParametroCategoria;
}
if($parametro == 'subcategoria'){
    $texto = $manualABMParametroSubCategoria;
}
//llamo a inicio.php para que incluir la cabecera y los recursos que usamos en la pagina.
include("inicio.php");
?>



<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>ABM de <?php echo $parametro ?></h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Listado (<?php echo $parametro ?>)</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                </div>
                                <div class='col-sm-2 pull-right'>
                                </div>
                                <div class='col-sm-4 pull-right'>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Buscar Nombre" id="q" onkeyup="loadparametro(1);" />
                                        <input type="hidden" id="v" value="<?php echo $parametro; ?>" />
                                        <button class="btn btn-buscar" type="button" onclick="loadparametro(1);">
                                            <span class="fa fa-search"></span>
                                        </button>
                                        
                                        <?php if ($perfil_usuario_log != "Vendedor") { ?>
                                        <a class="btn btn-info" href="crearParametro.php?parametro=<?php echo $parametro; ?>"> Agregar </a>
                                        <?php }?>
                                    </div>
                                    <?php if ($parametro == 'producto' && $perfil_usuario_log != "Vendedor") { ?>
                                        <br>
                                        <a class="btn btn-warning" style='float:right' href="modificarPrecio.php"> Modificar
                                            Precios Masivamente </a>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class='clearfixparam'></div>
                            <div id="resultadosparam"></div><!-- Carga de datos ajax aqui -->
                            <div class='outer_divparam'></div><!-- Carga de datos ajax aqui -->
                            <div id="loaderparam"></div><!-- Carga de datos ajax aqui -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php
include("fin.php");
?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Listado de <?php echo $parametro ?>");
</script>