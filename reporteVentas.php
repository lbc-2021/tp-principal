<?php

include('conexion.php');
include('usuario.php');
include('inicio.php');
include('manual.php');
$texto = $manualReporteVentas;
$aviso = '';

$querySucursales = "SELECT * from sucursal where estado_sucursal = '1'";
$ejecutarSucursales = mysqli_query($con, $querySucursales);

$queryVendedor = "SELECT * from usuario where estado_usuario = '1'";
$ejecutarVendedor = mysqli_query($con, $queryVendedor);


$disabledBtn = "disabled";

$sucursal = $_GET['sucursal'];
$vendedor = $_GET['vendedor'];
$fecha_fine = $_GET['fecha_fin'];
$fecha_inicioe = $_GET['fecha_inicio'];
$fecha_fin = date("d/m/Y", strtotime($fecha_fine));
$fecha_fin2 = date("Y/m/d", strtotime($fecha_fine));
$fecha_inicio = date("d/m/Y", strtotime($fecha_inicioe));
$fecha_inicio2 = date("Y/m/d", strtotime($fecha_inicioe));


if ($sucursal != ''){
    $querySucursales2 = "SELECT * from sucursal where estado_sucursal = '1' and id_sucursal = $sucursal";
    $ejecutarSucursales2 = mysqli_query($con, $querySucursales2);
    while ($row = mysqli_fetch_array($ejecutarSucursales2)) {
        $nombre_sucursal = $row['nombre_sucursal'];
    } 
}

else { $nombre_sucursal = "Todas las Sucursales";}

$laquery = "SELECT *, a1.descripcion_usuario as vende, a2.descripcion_usuario as cobra from venta left join usuario a1 on vendedor_venta = a1.id_usuario left join usuario a2 on vendedor_venta = a2.id_usuario  left join sucursal on sucursal_venta = id_sucursal  where vendedor_venta like '%$vendedor%' and  sucursal_venta like '%$sucursal%' and fechahora_venta between '$fecha_inicio2' and '$fecha_fin2' and estado_venta <> 'Pendiente' and estado_venta <> 'Cancelada' order by id_venta desc";
$ejecutar = mysqli_query($con, $laquery);


$row_count = mysqli_num_rows($ejecutar);
if ($row_count == 0) {
    $aviso = "No hay resultados para mostrar, por favor pruebe con otro periodo";
}

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Reporte de Ventas</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal"
                data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <form method="get" action="" novalidate>
                        <span class="section">Selección de sucursal y de rango de fechas</span>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <select name="sucursal" class="form-control" >
                                    <option value="">Todas las Sucursales</option>
                                    <?php while ($row = mysqli_fetch_array($ejecutarSucursales)) { ?>
                                    <option value="<?php echo $row['id_sucursal']; ?>">
                                        <?php echo $row['nombre_sucursal']  ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Vendedor<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <select name="vendedor" class="form-control">
                                    <option value="">Todos los Vendedores</option>
                                    <?php while ($row = mysqli_fetch_array($ejecutarVendedor)) { ?>
                                    <option value="<?php echo $row['id_usuario']; ?>">
                                        <?php echo $row['descripcion_usuario']  ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha de inicio<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="fecha_inicio" id="fecha_inicio"
                                    onchange="validarFechas()" required="required" type="date" />
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3 label-align">Fecha de fin<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input class="form-control" name="fecha_fin" id="fecha_fin" onchange="validarFechas()"
                                    required="required" type="date" />
                            </div>
                        </div>
                        <div class="field item form-group">
                            <div class="col-md-4 col-sm-4 label-align">
                                <button type='submit' class="btn btn-primary" disabled id="myBtn">Generar
                                    reporte</button>
                            </div>
                        </div>
<!--                        <div id="mjeError" style="position: auto; left: 20%;" class="col-md-12 col-sm-12"></div> -->
                    </form>
                </div>
                <?php if($fecha_fin != '' && $fecha_fin !='01/01/1970' && $fecha_inicio !='') { ?>
                
                
                
                
                
                
                
                
                <div class="x_panel">
                    
                    <span class="section">Reporte de Ventas de <?php echo $nombre_sucursal." (del ".$fecha_inicio." al ".$fecha_fin.")" ?> </span>
                    <div class="field item form-group">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>                                    
                                    <th class='text-center'>#</th>
                                    <th class='text-center'>Fecha</th>
                                    <th class='text-center'>Tipo</th>
                                        <th class='text-center'>Productos</th>
                                        <th class='text-center'>Precio Total</th>
                                        <th class='text-center'>Vendedor</th>
                                        <th class='text-center'>Sucursal</th>
                                        <th class='text-center'>Cajero</th>
                                        <th class='text-center'>Medio de Pago/Moneda</th>
                                    </tr>
                                </thead>
                                <tbody>





                                <?php  while ($row = mysqli_fetch_array($ejecutar)) {
                                    $id_venta = $row['id_venta'];
                                    $vendedor_venta = $row['vendedor_venta'];
                                    $monto_venta = $row['monto_venta'];
                                    $fechahora_venta = $row['fechahora_venta'];
                                    $fechahora_venta = date("d/m/Y - H:i", strtotime($fechahora_venta));
                                    $estado_venta = $row['estado_venta'];
                                    $tipo = "Local";
                                    if($estado_venta == 'Abonada - Virtual') $tipo = 'Virtual';
                                    $sucursal_venta = $row['sucursal_venta'];
                                    $cobrador_venta = $row['cobrador_venta'];
                                    $nombre_usuario = $row['vende'];
                                    $nombre_usuario2 = $row['cobra'];
                                    $nombre_sucursal = $row['nombre_sucursal'];  
                                    
                                    $queryDetalle = "SELECT * from detalle_venta left join producto on id_producto = producto_detalle where venta_detalle = $id_venta";
                                    $ejecutarDetalle = mysqli_query($con, $queryDetalle);


                                    $forma='';

                                    $medio = "SELECT * from pago_cheque where venta_cheque = $id_venta";
                                    $eject = mysqli_query($con, $medio);
                                    $row_count = mysqli_num_rows($eject);
                                    if ($row_count != 0) {
                                        while ($row = mysqli_fetch_array($eject)) { 
                                        $forma = $forma."<li> Cheque (".$row['moneda_cheque'].")</li>";
                                        }
                                    }

                                    $medio = "SELECT * from pago_credito where venta_credito = $id_venta";
                                    $eject = mysqli_query($con, $medio);
                                    $row_count = mysqli_num_rows($eject);
                                    if ($row_count != 0) {
                                        while ($row = mysqli_fetch_array($eject)) { 
                                            $forma = $forma."<li> Credito</li>";
                                            }

                                    }

                                    $medio = "SELECT * from pago_efectivo where venta_efectivo = $id_venta";
                                    $eject = mysqli_query($con, $medio);
                                    $row_count = mysqli_num_rows($eject);
                                    if ($row_count != 0) {
                                        while ($row = mysqli_fetch_array($eject)) { 
                                            $forma = $forma."<li> Efectivo (".$row['moneda_efectivo'].")</li>";
                                            }

                                    }

                                    $medio = "SELECT * from pago_tarjeta where venta_tarjeta = $id_venta";
                                    $eject = mysqli_query($con, $medio);
                                    $row_count = mysqli_num_rows($eject);
                                    if ($row_count != 0) {
                                        while ($row = mysqli_fetch_array($eject)) { 
                                            $forma = $forma."<li> Tarjeta (".$row['moneda_tarjeta'].")</li>";
                                            }

                                    }
                                    
                                    if ($forma == '') $forma = "<li> Cta Corriente </li>";

                                    ?>
                                    <tr>
                                    <td class='text-center'><?php echo $id_venta ?></td>
                                    <td class='text-center'><?php echo $fechahora_venta ?></td>
                                    <td class='text-center'><?php echo $tipo ?></td>
                                    <td ><?php while ($row = mysqli_fetch_array($ejecutarDetalle)) { 
                                        echo "<li>";
                                        echo $row['nombre_producto']." x".$row['cantidad_detalle'];
                                        echo "</li>";

                                    } ?></td>
                                    <td class='text-center'><?php echo "$".$monto_venta ?></td>
                                    <td class='text-center'><?php echo $nombre_usuario ?></td>
                                    <td class='text-center'><?php echo $nombre_sucursal ?></td>
                                    <td class='text-center'><?php echo $nombre_usuario2 ?></td>
                                    <td><?php echo $forma ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php echo $aviso ?>

                        </div>
                    </div>
                </div>
               <?php } ?>


<?php if($fecha_fine != ""){ ?>
 <form action="reporteVentasPDF.php" method="get">

                    <input type="text" hidden name="sucursal" value="<?php echo $sucursal; ?>">
                    <input type="text" hidden name="vendedor" value="<?php echo $vendedor; ?>">
                    <input type="date" hidden name="fecha_fin" value="<?php echo $fecha_fine; ?>">
                    <input type="date" hidden name="fecha_inicio" value="<?php echo $fecha_inicioe; ?>">
                    <button type="submit" style="float:right" class="btn btn-success">Descargar PDF del
                        reporte</button>
                </form>
<?php } ?>




            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<script>
function validarFechas() {
    console.log('Entrando en validar fechas');
    var fechainicial = document.getElementById("fecha_inicio").value;
    var fechafinal = document.getElementById("fecha_fin").value;
    if (fechafinal != '' && fechafinal != '') {
        if (Date.parse(fechafinal) >= Date.parse(fechainicial)) {
            document.getElementById("myBtn").disabled = false;
            document.getElementById("mjeError").innerHTML = "";
        } else {
            document.getElementById("myBtn").disabled = true;
            document.getElementById("mjeError").innerHTML =
                "<div class='alert alert-danger alert-dismissable col-md-6 col-sm-6'>" +
                "<button type='button' class='close' data-dismiss='alert'>&times;</button>" +
                "<strong>Error: La Fecha de inicio debe ser anterior o igual a la Fecha de fin.</strong>" +
                "</div>";
        }
    }

}
</script>
<?php include("fin.php"); ?>

<script type="text/javascript">
window.onload = cambiarTitulo("Gestión de cuenta corriente");
</script>