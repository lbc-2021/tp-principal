<?php 
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualListadoVentaVirtual;

$sucursal = $sucursal_usuario_log;


if($sucursal_usuario_log != 6 ) {
  $error = 1;
  $alerta = "Sucursal Invalida";                             
}

$estado = $_GET['estado'];
$idventa = $_GET['id'];
$message = $_GET['mensaje'];

$error = 0;
function verStock($producto, $sucursal, $cantidad, $con ){
  $query="SELECT * from stock_sucursal where id_sucursal='$sucursal' and id_producto='$producto'";
  $ejecutar= mysqli_query($con, $query);

  while ($row = mysqli_fetch_array($ejecutar)){
    $stock = $row['stock_sucursal'];;
  }

  if ($stock >= $cantidad){
    return true;
  } else{
  return false;
  }

}
function nombreTipo($tipo, $idtipo, $con){
  $tipo_query   =  mysqli_query($con, "SELECT nombre_$tipo FROM $tipo where id_$tipo = '$idtipo'");
  $resultadotipo = '';
                          if($tipo_query){
                            while($row = mysqli_fetch_array($tipo_query)){                  
                              $resultadotipo = $row["nombre_$tipo"];
                            }
                          }
  return $resultadotipo;
}

function stockMaximo($producto, $sucursal, $cantidad, $con ){
  $query="SELECT stock_sucursal from stock_sucursal where id_sucursal='$sucursal' and id_producto='$producto'";
  $ejecutar= mysqli_query($con, $query);

  while ($row = mysqli_fetch_array($ejecutar)){
    $stock = $row['stock_sucursal'];
  }

  return $stock;  
}



$fila = 1;

      
$mensaje = "Observaciones: <br>";

include("inicio.php"); ?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Ventas Virtuales</h3>
            </div>           
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
              <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                   <div class="x_title">
                        <h4>Carga de archivo .csv</h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                      <h4>Listado</h4>
                      
                        <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Producto</th>
                            <th>Cantidad cargada</th>

                          </tr>
                        </thead>
                  <?php
                  $fila = 1;
                  if (($gestor = fopen("funciones/ventaVirtual.csv", "r")) !== FALSE) {
                    $aux = 0;

                             
                              while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {  
                        
                               //datos de la operacion-----------------------------------------------------------------------------------------------------------
                        
                               if($aux == 0 ){
                                if($datos[1] != ''){
                                  $nrodeVentaVirtual = $datos[1];   
                                  $checkVtaVirtual = "SELECT * FROM venta_virtual WHERE nro_ventaVirtual = $nrodeVentaVirtual ";
                                  $resultadocheck = mysqli_query($con, $checkVtaVirtual);
                                  $row_count = mysqli_num_rows($resultadocheck);
                                  if ($row_count > 0 && $estado !='ok') {
                                    $alerta = "Nro de Venta Virtual ya fue procesado";
                                    $error = 1;
                                  } 
                                }
                                else { 
                                  $error = 1;
                                  $alerta = "Nro de Venta Virtual ya procesado";
                                }                              
                              }

                               //guardo la fecha de venta
                               if($aux == 1 ){
                                 if($datos[1] != ''){
                                   $fechaVenta = $datos[1];   
                                 }
                                 else { 
                                   $error = 1;
                                   $alerta = "No se pudo procesar el archivo. Motivo: Fecha Invalida";
                                 }                              
                               }
                        
                               //guardo el ciente
                               if($aux == 2 ){
                                 if($datos[1] != null){
                                   $cliente = $datos[1];
                                 }
                                 else { 
                                   $error = 1;
                                   $alerta = "No se pudo procesar el archivo. Motivo: Cliente Invalido";}                              
                               }
                        
                               //guardo la sucursal
                              /* if($aux == 3 ){
                                 if($datos[1] != null){
                                   $sucursal = $datos[1];
                                 }
                                 else { 
                                   $error = 1;
                                   $alerta = "No se pudo procesar el archivo. Motivo: Sucursal Invalida";}                              
                               } */
                        
                               //medios de pago-----------------------------------------------------------------------------------------------------------
                        
                               //efectivo
                               if($aux == 3 ){
                                   $monto_efectivo = $datos[1];  
                                   if($monto_efectivo == '') $monto_efectivo = 0;
                                   $suma = $suma + $monto_efectivo;                          
                               }
                        
                               //tarjeta                           
                               if($aux == 4 ){
                                 $monto_tarjeta1 = $datos[1];  
                                 if($monto_tarjeta1 == '') $monto_tarjeta1 = 0;                          
                                 $banco_tarjeta1 = $datos[2];                            
                                 $compania_tarjeta1 = $datos[3];                            
                                 $nro_tarjeta1 = $datos[4];                            
                                 $transaccion_tarjeta1 = $datos[5];                            
                                 $vto_tarjeta1 = $datos[6];   
                                 $suma = $suma + $monto_tarjeta1;                         
                               }
                        
                               //tarjeta 2 
                               if($aux == 5 ){
                                 $monto_tarjeta2 = $datos[1];   
                                 if($monto_tarjeta2 == '') $monto_tarjeta2 = 0;                        
                                 $banco_tarjeta2 = $datos[2];                            
                                 $compania_tarjeta2 = $datos[3];                            
                                 $nro_tarjeta2 = $datos[4];                            
                                 $transaccion_tarjeta2 = $datos[5];                            
                                 $vto_tarjeta2 = $datos[6];  
                                 $suma = $suma + $monto_tarjeta2;                         
                               
                               }
                        
                               //cheque 
                               if($aux == 6 ){
                                 $monto_cheque = $datos[1];  
                                 if($monto_cheque == '') $monto_cheque = 0;                                              
                                 $banco_cheque = $datos[2];                            
                                 $nro_cheque = $datos[3];                            
                                 $fecha_cheque = $datos[4]; 
                                 $suma = $suma + $monto_cheque;                         
                              
                               }
                        
                               //credito 
                               if($aux == 7 ){
                                 $monto_credito = $datos[1];   
                                 if($monto_credito == '') $monto_credito = 0;                                              
                        
                                 
                                 //chequeo si el limite le alcanza
                                 if($monto_credito != null and $monto_credito > 0){
                        
                                   $checkLimite = "SELECT deuda_cliente, limite_cliente from cliente where id_cliente = $cliente";
                                   $ejecutarCheckLimite = mysqli_query($con, $checkLimite);
                        
                                   while($row = mysqli_fetch_array($ejecutarCheckLimite)){	
                                     $deuda = $row['deuda_cliente'];
                                     $limite = $row['limite_cliente'];
                                   }
                                   
                                   if (($monto_credito + $deuda) >= $limite){
                                     $error = 1;
                                     $alerta = "Credito Insuficiente";
                                   }
                                   $suma = $suma + $monto_credito;                         
                        
                        
                                 }
                               } 
                        

                               /* if($aux >= 10){                    
                         
                                  if(verStock($datos[0], $sucursal, $datos[1], $con )){
                                    $cant = $datos[1];
                                  } else{
                                    $cant = stockMaximo($datos[0], $sucursal, $datos[1], $con);
                                    $filaaux = $aux+1;
                                    $alerta = $alerta."- Stock insuficiente del producto $datos[0] (fila $filaaux), se cargaron $cant <br>";
                                  }
                                  $tipo = 'producto';
                                  $nombreProducto = nombreTipo($tipo, $datos[0], $con);
                                  echo "<tr>";                            
                                  echo "<td>". $datos[0]. " ("  .$nombreProducto. ")"."</td>";
                                  echo "<td>".$cant."</td>";
                                  echo "</tr>";

                            }*/
                        
                            
                        
                            $aux = $aux + 1;
                          }

                          if($error == 0 && $idventa != ''){

                            $querydetalle = "SELECT * from detalle_venta left join producto on producto_detalle = id_producto where venta_detalle = $idventa";
                            $ejecquerydetalle =  mysqli_query($con, $querydetalle);


                            while ($row = mysqli_fetch_array($ejecquerydetalle)){
                                                                
                              echo "<tr>";                            
                              echo "<td>". $row['producto_detalle']. " ("  .$row['nombre_producto']. ")"."</td>";
                              echo "<td>".$row['cantidad_detalle']."</td>";
                              echo "</tr>";

                            }
                          }
                            fclose($gestor);
                        }
                        //$message = $message."<br>".$alerta;
                       // $message = $alerta;
                        ?>
                        </table>  
<?php                        if($error == 0){ ?>
                        <?php 
                        $tipo = 'sucursal';
                        $nombresucursal = nombreTipo($tipo, $sucursal, $con);
                        $tipo = 'cliente';
                        $nombrecliente = nombreTipo($tipo, $cliente, $con);


                        ?>
                     <!--  <h5>- Sucursal: <?php echo  $sucursal ." (".$nombresucursal .")"?> </h5> -->
                      <h5>- Cliente: <?php echo $cliente ." (".$nombrecliente .")"?></h5>
                      <h5>- Forma de Pago</h5> 
                      <ul>
                        <?php if($monto_cheque!= '0') echo "<li>Cheque: $$monto_cheque</li>"; ?>
                        <?php if($monto_efectivo!= '0') echo "<li>Efectivo: $$monto_efectivo</li>"; ?>
                        <?php if($monto_credito!= '0') echo "<li>Credito: $$monto_credito</li>"; ?>
                        <?php if($monto_tarjeta1!= '0') echo "<li>Tarjeta: $$monto_tarjeta1</li>"; ?>
                        <?php if($monto_tarjeta2!= '0') echo "<li>Tarjeta 2: $$monto_tarjeta2</li>"; ?>                    
                      </ul>
                      <?php } ?>
                      <?php  if ($estado != 'ok')  $message = $alerta; ?>
                        <p style="color:red">
                          <?php  if($message != '') echo "<br>Observaciones: <br>".$message; ?> 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Ver venta virtual");
</script>