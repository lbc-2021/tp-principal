<?php
include('conexion.php');
include('usuario.php');
include('manual.php');

$texto = $manualStock;

//traigo el contenido de la tabla Sucursal para mostrarlo en el combobox
$query = "SELECT * FROM sucursal where estado_sucursal= 1 ORDER BY nombre_sucursal ASC";
$resultado = mysqli_query($con, $query);



include('inicio.php');
?>

<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Stock</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Listado de Stock por Sucursal</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-md-4 pull-right">
                                    <select class="form-control" name="p" id="p" onchange="load(1);">
                                        <?php while ($row = mysqli_fetch_array($resultado)) { ?>
                                            <option value="<?php echo $row['id_sucursal']; ?>"> Sucursal:
                                                <?php echo $row['nombre_sucursal']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class='col-sm-8 pull-right'>
                                    <div id="custom-search-input">
                                        <div class="input-group col-md-12">
                                            <div class="col-md-11">
                                                <input type="text" class="form-control" placeholder="Buscar por Nombre de Producto" id="q" onkeyup="load(1);" />
                                            </div>
                                            <span class="input-group-btn">
                                                <button class="btn btn-info" id='bton' type="button" onclick="load(1);">
                                                    <span class="fa fa-search" aria-hidden="true"></span>
                                                </button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class='clearfix'></div> <br>
                            <div id="loader"></div><!-- Carga de datos ajax aqui -->
                            <div id="resultados"></div><!-- Carga de datos ajax aqui -->
                            <div class='outer_div'></div><!-- Carga de datos ajax aqui -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include('fin.php');
?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Listado de stock");
</script>