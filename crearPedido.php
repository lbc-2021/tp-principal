<?php //query para traer el contenido de la tabla Sector para ser usando en el ComboBox
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualCrearPedido;
$cliente = $_POST['cliente'];
$subcategoria = $_POST['subcategoria'];
echo $subcategoria;

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Nueva Venta</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                    <button class="btn btn-danger" style="float:right" onclick="goBack()">Regresar</button> 

                        <form method="post" action="funciones/crearPedido_funcion.php" novalidate>

                            <input type="hidden" name="cliente" value="<?php echo $cliente ?>">
                            <br>
                            <span class="section">Pedidos</span>
                            <div class="field item form-group">
                                <div class="col-md-12 col-sm-12">
                                <?php 

                                for ($i = 1 ; $i <= 20; $i++){
                                    if ($_POST['producto'.$i] != '' || $_POST['producto'.$i] != null ){
                                        $prod= $_POST["producto$i"];                                        
                                        $cant= $_POST["cantidad$i"];
                                        if($subcategoria == '') $cant= round($cant);
                                        $cantInicial= $cant;
                                        $id= $_POST["id".$i];

                                        ?>



                                        <?php
                                        
                                        $query= "SELECT * FROM producto left join stock_sucursal on producto.id_producto = stock_sucursal.id_producto where producto.id_producto = $prod and id_sucursal = $sucursal_usuario_log";
                                        $ejecutar = mysqli_query($con, $query);

                                        while($row = mysqli_fetch_array($ejecutar)){
                                            $nombreProd = $row['nombre_producto'];
                                            $stockProd = $row ['stock_sucursal'];
                                            $precio = $row ['precio_producto'];
                                            $id= $row['id'];

                                        }

                                        $total = $precio * $cant;
                                        $aclaracion = '';
                                        if($cant > $stockProd) {
                                            $cant = $stockProd;
                                            if($subcategoria == '') $cant= round($cant);
                                            $aclaracion = " <font style ='color:red'>(en stock $stockProd unidades unicamente, se presupuestan las disponibles)</font>";
                                            $total = $precio * $stockProd;
                                        }

                                        if($stockProd == 0) {
                                            $cant = 0;
                                            if($subcategoria == '') $cant= round($cant);
                                            $aclaracion = " <font style ='color:red'> (Sin Stock) </font>" ;
                                            $total = 0;
                                        }

                                        $suma = $suma + $total;

                                        ?>

                                        <input type="hidden" name='<?php echo "id$i" ?>' value="<?php echo $id?>">

                                        <div class="col-md-8 col-sm-8">
                                            <li> <label>
                                                <?php 
                                                echo "<b>".$nombreProd."</b> x".$cantInicial." unidades".$aclaracion." ($".$precio." c/u)";
                                                ?>
                                            </label> </li>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <?php echo "<b>$".$total."</b>" ?>
                                        </div>

                                        <input type="hidden" name='<?php echo "producto$i" ?>' value="<?php echo $prod?>">
                                        <input type="hidden" name='<?php echo "cantidad$i" ?>' value="<?php echo $cant?>">

                                        <?php
                                        }
                                    }
                                    ?>

                                    <div class="col-md-12 col-sm-12"> <hr> 
                                        <?php echo "<h3>Total a Abonar: $".$suma."</h3>"; ?>
                                    </div>
                                    <input type="hidden" name="suma" id="suma" value="<?php echo $suma; ?>">


                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-success">Confirmar Pedido</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<?php include("fin.php");?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Confirmar pedido");
</script>