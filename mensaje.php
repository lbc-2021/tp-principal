<?php
include("usuario.php"); 

$class=$_GET['class'];
$message=$_GET['message'];
$message = utf8_decode($message);
$destino=$_GET['destino'];

if($destino == 'listadoVentaVirtual.php?estado=ok') $destino = $destino."&id=".$_GET['id']."&mensaje=".$message;

header("refresh:2; $destino");
include("inicio.php"); 
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                    <div class="x_title">
                        <h4>Atención</h4>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="<?php echo $class?>">
                            <b><?php echo $message;?></b>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Mensaje");
</script>