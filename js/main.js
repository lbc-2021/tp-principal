$(function () {
	load(1);
});
function load(page) {
	var query = $("#q").val();
	var p = $("#p").val();

	var per_page = 10;
	var parametros = { "action": "ajax", "page": page, 'query': query, 'p': p, 'per_page': per_page };
	$("#loader").fadeIn('slow');
	$.ajax({
		url: 'ajax/listar_stock.php',
		data: parametros,
		beforeSend: function (objeto) {
			$("#loader").html("Cargando...");
		},
		success: function (data) {
			$(".outer_div").html(data).fadeIn('slow');
			$("#loader").html("");
		}
	})
}

$(function () {
	loadparametro(1);
});
function loadparametro(page) {
	var query = $("#q").val();
	var v = $("#v").val();
	var per_page = 10;
	var parametros = { "action": "ajax", "page": page, 'query': query, 'per_page': per_page, 'v': v };
	$("#loaderparam").fadeIn('slow');
	$.ajax({
		url: 'ajax/listar_parametro.php',
		data: parametros,
		beforeSend: function (objeto) {
			$("#loaderparam").html("Cargando...");
		},
		success: function (data) {
			$(".outer_divparam").html(data).fadeIn('slow');
			$("#loaderparam").html("");
		}
	})
}


$(function () {
	loadusuario(1);
});
function loadusuario(page) {
	var query = $("#q").val();
	var per_page = 10;
	var parametros = { "action": "ajax", "page": page, 'query': query, 'per_page': per_page };
	$("#loaderusu").fadeIn('slow');
	$.ajax({
		url: 'ajax/listar_usuarios.php',
		data: parametros,
		beforeSend: function (objeto) {
			$("#loaderusu").html("Cargando...");
		},
		success: function (data) {
			$(".outer_divusu").html(data).fadeIn('slow');
			$("#loaderusu").html("");
		}
	})
}

$(function () {
	loadventa(1);
});
function loadventa(page) {
	var query = $("#q").val();
	var per_page = 10;
	var parametros = { "action": "ajax", "page": page, 'query': query, 'per_page': per_page };
	$("#loaderventa").fadeIn('slow');
	$.ajax({
		url: 'ajax/listar_ventas.php',
		data: parametros,
		beforeSend: function (objeto) {
			$("#loaderventa").html("Cargando...");
		},
		success: function (data) {
			$(".outer_divventa").html(data).fadeIn('slow');
			$("#loaderventa").html("");
		}
	})
}

$(function () {
	loadventasr(1);
});
function loadventasr(page) {
	var query = $("#q").val();
	var p = $("#p").val();

	var per_page = 10;
	var parametros = { "action": "ajax", "page": page, 'query': query, 'p': p, 'per_page': per_page };
	$("#loaderventasr").fadeIn('slow');
	$.ajax({
		url: 'ajax/listar_ventasRealizadas.php',
		data: parametros,
		beforeSend: function (objeto) {
			$("#loaderventasr").html("Cargando...");
		},
		success: function (data) {
			$(".outer_divventasr").html(data).fadeIn('slow');
			$("#loaderventasr").html("");
		}
	})
}

$(document).ready(function () {
	mostrardivs();
});

function mostrardivs() {

	var isChecked = document.getElementById('checkstock').checked;
	if (isChecked) {
		$('input[name=stockminimo]').prop('required', true);
		$('input[name=stockminimo]').prop('readonly', false);
		$('input[name=cantidadasolicitar]').prop('required', true);
		$('input[name=cantidadasolicitar]').prop('readonly', false);
		$('input[name=stockseguridad]').prop('required', true);
		$('input[name=stockseguridad]').prop('readonly', false);
		$('select[name=proveedor]').prop('required', true);
		$('select[name=proveedor]').prop('disabled', false);
	} else {
		$('input[name=stockminimo]').prop('readonly', true);
		$('input[name=stockminimo]').prop('required', false);
		$('input[name=cantidadasolicitar]').prop('readonly', true);
		$('input[name=cantidadasolicitar]').prop('required', false);
		$('input[name=stockseguridad]').prop('readonly', true);
		$('input[name=stockseguridad]').prop('required', false);
		$('select[name=proveedor]').prop('disabled', true);
		$('select[name=proveedor]').prop('required', false);
		var stockseguridad = document.getElementById("stockseguridad");
		stockseguridad.value = "";
		var stockminimo = document.getElementById("stockminimo");
		stockminimo.value = "";
		var cantidadasolicitar = document.getElementById("cantidadasolicitar");
		cantidadasolicitar.value = "";
		//document.getElementById("proveedor").options.item(0).selected = 'selected';

	}
}
function editarsubcategoria() {
	var srt = document.getElementById('categoria').value;
	var scaux = document.getElementById('subcataux').value;
	var scaux2 = document.getElementById('subcataux2').value;

	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			$("#myDiv")[0].html(xmlhttp.responseText);
		}
	}
	xmlhttp.open("POST", "get_subcategoriaAux.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("q=" + srt + "&k=" + scaux + "&i=" + scaux2);



}


function versubcategoria() {
	var srt = document.getElementById('categoria').value;
	const $lotediv = $('#lotediv');
	const $unidaddiv = $('#unidaddiv');


	if (srt == 2){
		$unidaddiv.removeAttr('hidden');
		$lotediv.attr('hidden', true);
		$('#cantidadLote').removeAttr('required');
		$('#unidaddecompra').attr('required', true);
	} else {
		$lotediv.removeAttr('hidden');
		$unidaddiv.attr('hidden', true);
		$('#unidaddecompra').removeAttr('required');
		$('#cantidadLote').attr('required', true);
	}

	var scaux = document.getElementById('subcataux').value;
	var scaux2 = document.getElementById('subcataux2').value;
	var xmlhttp;

	if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else {// code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function () {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
		}
	}
	xmlhttp.open("POST", "get_subcategoria.php", true);
	xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	xmlhttp.send("q=" + srt + "&k=" + scaux + "&i=" + scaux2);
}
$(document).ready(function () {
	$('.js-example-basic-single').select2();
	//versubcategoria();
	editarsubcategoria();
});


function agregarProducto(k) {
	var select = document.getElementById(k);
	var producto = document.getElementById("producto" + k);
	var cantidad = document.getElementById("cantidad" + k);
	var a = document.getElementById("a" + k);
	var aux = k + 1;
	var b = document.getElementById("a" + aux);
	select.style.display = '';
	a.style.display = 'none';
	b.style.display = '';
	producto.required = true;
	cantidad.required = true;
}

function eliminarProducto(k) {
	var select = document.getElementById(k);
	var producto = document.getElementById("producto" + k);
	var cantidad = document.getElementById("cantidad" + k);
	var a = document.getElementById("a" + k);
	var aux = k + 1;
	var b = document.getElementById("a" + aux);
	producto.required = false;
	cantidad.required = false;
	select.style.display = 'none';
	producto.options.item(0).selected = 'selected';


	//a.style.display = '';
}

function goBack() {
	window.history.back();
}

function switchRequiredEmail() {
	const $profile = $('#perfil');
	const $mailDiv = $('#mailDiv');
	const mailValue = $('#mail')[0].value;
	if ($profile[0].value === 'Gerente') {
		$mailDiv.html(`<label class="col-form-label col-md-3 col-sm-3 label-align">Mail<span class="required">*</span></label>
		<div class="col-md-6 col-sm-6">
			<input class="form-control email" type="email" required="required" name="mail" id="mail" value='${mailValue}'>
		</div>`);
	} else {
		$mailDiv.html(`<label class="col-form-label col-md-3 col-sm-3 label-align">Mail</label>
		<div class="col-md-6 col-sm-6">
			<input class="form-control email optional" type="email" name="mail" id="mail" value='${mailValue}'>
		</div>`);
	}
}

function switchAgregarRetirar() {
	const isChecked = $('#checkcaja')[0].checked;
	const $btn = $('#cajaBtn');
	const $accion = $('#accion');
	const $max_value = $('#max_value');
	const $tope_value = $('#tope_value');
	const $monto_sucursal = $('#monto_sucursal');
	const $value_monto = $('#value_monto');

	if (isChecked) {
		$btn.text("Retirar");
		$tope_value.val($monto_sucursal.val());
		$accion.html(`<input type="text" name="accion" hidden value="retirar">`);
	} else {
		$btn.text("Agregar");
		$tope_value.val($max_value.val());
		$accion.html(`<input type="text" name="accion" hidden value="agregar">`);
	}
	$value_monto.html(`<input class="form-control" name="monto" id="monto" required="required" step="0.01" data-validate-minmax="1,` + $tope_value.val() + `" type="number" placeholder="Monto" />`);

}

function mostrarDatosMedioDePago(str) {
	const $medioPagoCB = $('#medio_pago')[0];
	const $bancoDiv = $('#banco_div');
	const $bancoLabel = $('#banco_label');
	const $companiaDiv = $('#compania_div');
	const $numeroDiv = $('#numero_div');
	const $numeroLabel = $('#numero_label');
	const $numeroTransaccionDiv = $('#numero_transaccion_div');
	const $vtoDiv = $('#vto_div');
	const $fechaDiv = $('#fecha_div');
	if (str === "venta") {
		var $montoPagado = $('#monto_pagado');
		var total = $('#total')[0].value;
	}

	//Oculto todas
	$bancoDiv.attr('hidden', true);
	$('#banco').removeAttr('required');
	$vtoDiv.attr('hidden', true);
	$('#vto').removeAttr('required');
	$numeroDiv.attr('hidden', true);
	$('#numero').removeAttr('required');
	$fechaDiv.attr('hidden', true);
	$('#fecha').removeAttr('required');
	$companiaDiv.attr('hidden', true);
	$('#compania').removeAttr('required');
	$numeroTransaccionDiv.attr('hidden', true);
	$('#transaccion').removeAttr('required');
	$('#monedadiv').attr('hidden', false);

	if (str === "venta") {
		$montoPagado.removeAttr('readonly');
		$montoPagado.val("");
	}

	if ($medioPagoCB.value === "ctacorriente") {
		$('#calculadoradiv').attr('hidden', true);
		$('#monedadiv').attr('hidden', true);
		//$('#calculadora').attr('hidden', true);
	}

	if ($medioPagoCB.value === "credito") {
		$bancoLabel.html(`Banco al que pertenece la tarjeta<span class="required">*</span>`);
		$bancoDiv.removeAttr('hidden');
		$('#banco').attr('required', true);
		$companiaDiv.removeAttr('hidden');
		$('#compania').attr('required', true);
		$vtoDiv.removeAttr('hidden');
		$('#vto').attr('required', true);
		$numeroLabel.html(`Número de la tarjeta<span class="required">*</span>`);
		$numeroDiv.removeAttr('hidden');
		$('#numero').attr('required', true);
		$numeroTransaccionDiv.removeAttr('hidden');
		$('#transaccion').attr('required', true);



		if (str === "venta") {
			//$montoPagado.val(total.substring(1, total.length));
			//$montoPagado.attr('readonly', true);
		}
	} else if ($medioPagoCB.value === "cheque") {
		$bancoLabel.html(`Banco al que pertenece el cheque<span class="required">*</span>`);
		$bancoDiv.removeAttr('hidden');
		$('#banco').attr('required', true);
		$numeroLabel.html(`Número del cheque<span class="required">*</span>`);
		$numeroDiv.removeAttr('hidden');
		$('#numero').attr('required', true);
		$fechaDiv.removeAttr('hidden');
		$('#fecha').attr('required', true);
	}
}



function mostrarDatosMedioDePago2(str) {
	//console.log('asd');
	const $medioPagoCB2 = $('#medio_pago2')[0];
	const $bancoDiv2 = $('#banco_div2');
	const $bancoLabel2= $('#banco_label2');
	const $companiaDiv2 = $('#compania_div2');
	const $numeroDiv2 = $('#numero_div2');
	const $numeroLabel2 = $('#numero_label2');
	const $numeroTransaccionDiv2 = $('#numero_transaccion_div2');
	const $fechaDiv2 = $('#fecha_div2');
	const $vtoDiv2 = $('#vto_div2');
	$('#monedadiv2').attr('hidden', false);

	if (str === "venta2") {
		var $montoPagado2 = $('#monto_pagado2');
		var total = $('#total')[0].value;
	}

	if ($medioPagoCB2.value === "ctacorriente") {
		$('#calculadoradiv2').attr('hidden', true);
		$('#monedadiv2').attr('hidden', true);
		//$('#calculadora').attr('hidden', true);
	}

	//Oculto todas
	$vtoDiv2.attr('hidden', true);
	$('#vto2').removeAttr('required');
	$bancoDiv2.attr('hidden', true);
	$('#banco2').removeAttr('required');
	$numeroDiv2.attr('hidden', true);
	$('#numero2').removeAttr('required');
	$fechaDiv2.attr('hidden', true);
	$('#fecha2').removeAttr('required');
	$companiaDiv2.attr('hidden', true);
	$('#compania2').removeAttr('required');
	$numeroTransaccionDiv2.attr('hidden', true);
	$('#transaccion2').removeAttr('required');
	if (str === "venta2") {
		$montoPagado2.removeAttr('readonly');
		$montoPagado2.val("");
	}

	if ($medioPagoCB2.value === "credito") {
		$bancoLabel2.html(`Banco al que pertenece la tarjeta<span class="required">*</span>`);
		$bancoDiv2.removeAttr('hidden');
		$('#banco2').attr('required', true);
		$vtoDiv2.removeAttr('hidden');
		$('#vto2').attr('required', true);
		$companiaDiv2.removeAttr('hidden');
		$('#compania2').attr('required', true);
		$numeroLabel2.html(`Número de la tarjeta<span class="required">*</span>`);
		$numeroDiv2.removeAttr('hidden');
		$('#numero2').attr('required', true);
		$numeroTransaccionDiv2.removeAttr('hidden');
		$('#transaccion2').attr('required', true);
		if (str === "venta2") {
			//$montoPagado.val(total.substring(1, total.length));
			//$montoPagado.attr('readonly', true);
		}
	} else if ($medioPagoCB2.value === "cheque") {
		$bancoLabel2.html(`Banco al que pertenece el cheque<span class="required">*</span>`);
		$bancoDiv2.removeAttr('hidden');
		$('#banco2').attr('required', true);
		$numeroLabel2.html(`Número del cheque<span class="required">*</span>`);
		$numeroDiv2.removeAttr('hidden');
		$('#numero2').attr('required', true);
		$fechaDiv2.removeAttr('hidden');
		$('#fecha2').attr('required', true);
	}
}


function agregarMedio(i) {
	var agregarmedio = document.getElementById("#agregarmedio");	

	if(i == 1){
		$('#agregarmedio').attr('hidden', true);
		$('#nuevomedio').attr('hidden', false);
		$('#medio_pago2').attr('required', true);
	$('#monto_pagado2').attr('required', true);

	}
	else{
		$('#agregarmedio').attr('hidden', false);
		$('#nuevomedio').attr('hidden', true);
		$('#medio_pago2').attr('required', false);
		$('#monto_pagado2').attr('required', false);
		//$('#medio_pago2').options.item(0).selected = 'selected';
		//$('#medio_pago2').value= '';
		document.getElementById('medio_pago2').getElementsByTagName('option')[0].selected = 'selected'
		mostrarDatosMedioDePago2('venta');

	}
}




function cambiarTitulo(str) {
	const $title = $('#tab_title');
	$title.html(`Ferretería | ${str}`);
}

function actualizarMax() {
	valorPedido = $('#monto_pedido')[0].value;
	$valorPagado = $('#monto_pagado');
	$valorPagado.attr('data-validate-minmax', `0,${valorPedido}`);
}


function verMoneda() {
	//var calculadoradiv = $('#calculadoradiv').val();
	var sel = $('#moneda').val();
	var medio = $('#medio_pago').val();
	var dolar = $('#dolar').val();
	var euro = $('#euro').val();
	var monto = $('#total').val();
	var monto_pagado = $('#monto_pagado').val();

	$('#calculadoradiv').attr('hidden', true);

	if (medio.value != "ctacorriente") {

	if (sel == 'dolar') {
		var resultado = monto_pagado*dolar;
		var resulfinal = (Math.round(resultado * 100) / 100);
		var restante =  (Math.round((monto - resulfinal) * 100) / 100);
		document.getElementById("montomoneda").value= parseFloat(resulfinal);
		if ((Math.round(resultado * 100) / 100) <=monto){
		document.getElementById("calculadora").value= "$"+resulfinal + ' (Restan: $' + (restante) + ')';
		}
		else{
			document.getElementById("calculadora").value="Se esta abonando de más";
		}
	$('#calculadoradiv').attr('hidden', false);

	}

	if (sel == 'euro') {
		var resultado = monto_pagado*euro;
		var resulfinal = (Math.round(resultado * 100) / 100);
		var restante =  (Math.round((monto - resulfinal) * 100) / 100);
		document.getElementById("montomoneda").value= parseFloat(resulfinal);
		if ((Math.round(resultado * 100) / 100) <=monto){
		document.getElementById("calculadora").value= "$"+resulfinal + ' (Restan: $' + (restante) + ')';
		}
		else{
			document.getElementById("calculadora").value="Se esta abonando de más";
		}
	$('#calculadoradiv').attr('hidden', false);


	}
}

}


function verMoneda2() {
	//var calculadoradiv = $('#calculadoradiv').val();
	var sel = $('#moneda2').val();
	var medio = $('#medio_pago2').val();
	var dolar = $('#dolar').val();
	var euro = $('#euro').val();
	var monto = $('#total').val();
	var monto_pagado = $('#monto_pagado2').val();

	$('#calculadoradiv2').attr('hidden', true);

	if (medio.value != "ctacorriente") {

	if (sel == 'dolar') {
		var resultado = monto_pagado*dolar;
		var resulfinal = (Math.round(resultado * 100) / 100);
		var restante =  (Math.round((monto - resulfinal) * 100) / 100);
		document.getElementById("montomoneda2").value= parseFloat(resulfinal);		
		document.getElementById("calculadora2").value="$"+Math.round(resultado * 100) / 100 ;
	$('#calculadoradiv2').attr('hidden', false);

	}

	if (sel == 'euro') {
		var resultado = monto_pagado*euro;
		var resulfinal = (Math.round(resultado * 100) / 100);
		var restante =  (Math.round((monto - resulfinal) * 100) / 100);
		document.getElementById("montomoneda2").value= parseFloat(resulfinal);		
		document.getElementById("calculadora2").value=Math.round(resultado * 100) / 100 ;
		$('#calculadoradiv2').attr('hidden', false);

	}
}

}
/*
// ver que verga paso aca

function agregarProducto() {


var div = document.createElement("div");
div.id="1";
div.name="asd";
div.setAttribute('class', 'field item form-group'); // and make sure myclass has some styles in css
document.getElementById("main").appendChild(div);

var div2 = document.createElement("label");
div2.id="2";
div2.name="asd2";
div2.innerHTML = "Producto";
div2.setAttribute('class', 'col-form-label col-md-3 col-sm-3  label-align'); // and make sure myclass has some styles in css
document.getElementById("1").appendChild(div2);

var div3 = document.createElement("div");
div3.id="3";
div3.name="asd2";
div3.setAttribute('class', 'col-md-3 col-sm-3'); // and make sure myclass has some styles in css
document.getElementById("1").appendChild(div3);

var div4 = document.createElement("SELECT");
div4.id="4";
div4.name="asd2";
div4.setAttribute('class', 'chosen'); // and make sure myclass has some styles in css
document.getElementById("3").appendChild(div4);

var divOp1 = document.createElement("option");
divOp1.id="4";
divOp1.name="asd2";
divOp1.innerHTML = "asdsa";
divOp1.setAttribute('class', 'chosen'); // and make sure myclass has some styles in css
document.getElementById("4").appendChild(divOp1);

$(".chosen").chosen();

}
*/

