<?php

include('usuario.php');
include('manual.php');
$texto = $manualVentasACobrar;

/*if($perfil_ventaario_log != 'Cajero'){
  $message="No posee permisos para realizar la acción";
  $class="alert alert-danger";
  header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
} */

//llamo a inicio.php para que incluir la cabecera y los recursos que usamos en la pagina.
include("inicio.php");
?>



<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Ventas a Cobrar</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12  ">
                <div class="x_panel">
                   <div class="x_title">
                        <h2>Listado de Ventas pendientes de esta sucursal</h2>
                        <ul class="nav navbar-right panel_toolbox">
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Ventas <b>Pendientes</b></h2>
                                </div>
                                <div class='col-sm-2 pull-right'>
                                </div>
                                <div class='col-sm-4 pull-right'>
                                    <div class="input-group col-md-12">
                                        <input type="text" class="form-control" placeholder="Buscar Nombre de Cliente"
                                            id="q" onkeyup="loadventa(1);" />
                                        <button class="btn btn-buscar" type="button" onclick="loadventa(1);">
                                            <span class="fa fa-search"></span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class='clearfixventa'></div>
                        <div id="resultadosventa"></div><!-- Carga de datos ajax aqui -->
                        <div class='outer_divventa'></div><!-- Carga de datos ajax aqui -->
                        <div id="loaderventa"></div><!-- Carga de datos ajax aqui -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /page content -->

<?php
include("fin.php");
?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Listado de ventas a cobrar");
</script>