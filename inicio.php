<!DOCTYPE html>
<html lang="en">

<?php

include('conexion.php');            ///////////////////////////////////////////////////por ahi esto rompa algo despues. <3
include('usuario.php');

$query = "SELECT * FROM usuario WHERE id_usuario = $id_usuario_log";
$ejecutar = mysqli_query($con, $query);
while ($row = mysqli_fetch_array($ejecutar)) {
    $descripcion_usuario_log = $row['descripcion_usuario'];
}


$queryPint = "SELECT * FROM subcategoria WHERE categoria_subcategoria = 2";
$ejecutarPint = mysqli_query($con, $queryPint);


?>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title id="tab_title"></title>

    <link rel="icon" href="assets/icon.png">

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- Chosen -->
    <link rel="stylesheet" href="vendors/chosen/chosen.css" />
    <!-- Select2 -->
    <link href="vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Css Custom del Tema -->
    <link href="build/css/custom.css" rel="stylesheet">
    <!-- chart.js  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.3.2/chart.js"></script>    
</head>

<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.php" class="site_title"> <span>Ferretería</span></a>
                    </div>
                    <div class="clearfix"></div>
                    <br />
                    <!-- menu izquierdo -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li><a href="index.php"><i class="fa fa-home"></i> Inicio </a></li>
                                <?php if ($perfil_usuario_log != "Administrativo") { ?>
                                    <li><a><i class="fa fa-arrow-right"></i> Ventas <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="venta.php">Cargar Venta</a></li>
                                            <li><a href="ventasACobrar.php">Ventas a Cobrar</a></li>
                                            <li><a href="ventaVirtual.php">Cargar Venta Virtual</a></li>
                                            <li><a>Venta y Preparación de Pinturas<span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                               <?php  while ($row = mysqli_fetch_array($ejecutarPint)) { 
                                                   $nombreSC = $row['nombre_subcategoria']; ?>
                                                    <li><a href="pinturas.php?subcategoria=<?php echo $nombreSC?>"><?php echo $nombreSC ?></a></li>
                                                    <?php } ?>

                                                </ul>
                                            <li><a href="listadoDeVentas.php">Ventas Realizadas</a></li>
                                            <li><a href="abmParametros.php?parametro=cliente">Clientes</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="stock.php"><i class="fa fa-table"></i>Stock</a>

                                    <li><a><i class="fa fa-bar-chart-o"></i> Reportes <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a>Reportes Operativos<span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                                    <li><a href="seleccionSucursal.php">Reporte de caja diaria</a></li>
                                                    <li><a href="reporteVentas.php">Reporte de Ventas</a></li>
                                                    <li><a href="reportes.php?parametro=stockFaltante">Reporte de stock faltante</a></li>
                                                    <li><a>Reporte de ítems con riesgo de stock<span class="fa fa-chevron-down"></span></a>
                                                        <ul class="nav child_menu">
                                                            <li><a href="reportes.php?parametro=stockMinimo">Stock mínimo</a></li>
                                                            <li><a href="reportes.php?parametro=stockSeguridad">Stock de seguridad</a></li>
                                                        </ul>
                                                </ul>
                                            <li><a>Reportes Gerenciales<span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                                    <li><a href="reporteComision.php">Reporte de Comisiones por Vendedor</a></li>
                                                    <li><a href="reportes.php?parametro=sucursal">Ranking de ventas por sucursal</a></li>
                                                    <li><a href="reportes.php?parametro=vendedor">Ranking de ventas por vendedor</a></li>
                                                </ul>

                                            <li><a>Reportes Generales<span class="fa fa-chevron-down"></span></a>
                                                <ul class="nav child_menu">
                                                    <li><a href="reportes.php?parametro=compras">Reporte de compras</a></li>
                                                    <li><a href="reportes.php?parametro=traspasos">Reporte de traspasos de mercadería entre sucursales</a></li>
                                                    <li><a href="reportes.php?parametro=cliente">Reporte de clientes</a></li>

                                                </ul>
                                        </ul>
                                    </li>

                                    <?php if ($perfil_usuario_log != "Vendedor") { ?>
                                        <li><a href="caja.php"><i class="fa fa-credit-card-alt"></i> Caja </a>
                                        <?php } ?>

                                        <?php if ($perfil_usuario_log == "Supervisor" || $perfil_usuario_log == "Gerente") { ?>
                                        <li><a href="abmParametros.php?parametro=proveedor"><i class="fa fa-cogs"></i>ABM Proveedores</a>
                                        <?php } ?>

                                        <li><a><i class="fa fa-usd"></i>Cuentas corrientes<span class="fa fa-chevron-down"></span></a>
                                            <ul class="nav child_menu">
                                                <li><a href="listaCuentasCorrientes.php?parametro=clientes">Clientes</a></li>
                                                <li><a href="listaCuentasCorrientes.php?parametro=proveedores">Proveedores</a></li>
                                            </ul>
                                        </li>

                            </ul>

                            <br>

                            <?php if ($perfil_usuario_log == "Administrador") { ?>
                                <h3>Administrador</h3>

                                <ul class="nav side-menu">

                                    <li><a><i class="fa fa-cogs"></i>Parametros <span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="abmParametros.php?parametro=producto">ABM Productos</a></li>
                                            <li><a href="abmParametros.php?parametro=sucursal">ABM Sucursales</a></li>
                                            <li><a href="abmParametros.php?parametro=proveedor">ABM Proveedores</a></li>
                                            <li><a href="abmParametros.php?parametro=categoria">ABM Categorias</a></li>
                                            <li><a href="abmParametros.php?parametro=subcategoria">ABM SubCategorias</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="abmUsuarios.php"><i class="fa fa-user"></i>Usuarios</a>
                                </ul>

                            <?php } ?>

                            <?php if ($perfil_usuario_log == "Vendedor") { ?>
                                <h3>Información</h3>

                                <ul class="nav side-menu">

                                    <li><a><i class="fa fa-question-circle"></i>Parametros<span class="fa fa-chevron-down"></span></a>
                                        <ul class="nav child_menu">
                                            <li><a href="abmParametros.php?parametro=producto">ABM Productos</a></li>
                                            <li><a href="abmParametros.php?parametro=sucursal">ABM Sucursales</a></li>
                                            <li><a href="abmParametros.php?parametro=proveedor">ABM Proveedores</a></li>
                                            <li><a href="abmParametros.php?parametro=categoria">ABM Categorias</a></li>
                                            <li><a href="abmParametros.php?parametro=subcategoria">ABM SubCategorias</a></li>
                                        </ul>
                                    </li>

                                </ul>

                            <?php } ?>

                        <?php } else { ?>
                            <li><a href="caja.php"><i class="fa fa-credit-card-alt"></i>Caja</a>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- /menu izquierdo -->
                </div>
            </div>

            <!-- menu de arriba -->
            <div class="top_nav">
                <div class="nav_menu">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="nav navbar-nav">
                        <ul class=" navbar-right">
                            <li class="nav-item dropdown open" style="padding-left: 15px;">
                                <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-user mr-1"></i><b> <?php echo $descripcion_usuario_log; ?> </b>
                                </a>
                                <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="cambiarPass.php">Cambiar Contraseña</a>
                                    <a class="dropdown-item" href="login.php"><i class="fa fa-sign-out pull-right"></i>
                                        Salir</a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /menu de arriba -->