<?php
include('conexion.php');
include('usuario.php');
include('manual.php');

// $texto = $manualVerSucursal;
if ($perfil_usuario_log == 'Vendedor') {
    $message = "No posee permisos para realizar la acción";
    $class = "alert alert-danger";
    header("refresh:0; mensaje.php?class=$class&message=$message&destino=index.php");
}

$parametro = $_GET['parametro'];
if ($parametro == 'reporte') {
    $id_suc = $_POST['sucursal'];
    $texto = $manualVerSucursalReporte;
} else {
    $id_suc = $_GET['id'];
    $texto = $manualVerSucursal;
}

if ($id_suc != 0)
    $querySucursal = mysqli_query($con, "SELECT * FROM sucursal where id_sucursal = $id_suc");
else
    $querySucursal = mysqli_query($con, "SELECT *, 
    sum(montoEfectivo_sucursal) AS total1,
    sum(montoCheques_sucursal) AS total2,
    sum(montoCredito_sucursal) AS total3,
    sum(montoEfectivoDolar_sucursal) AS total4,
    sum(montoChequesDolar_sucursal) AS total5,
    sum(montoCreditoDolar_sucursal) AS total6,
    sum(montoEfectivoEuro_sucursal) AS total7,
    sum(montoChequesEuro_sucursal) AS total8,
    sum(montoCreditoEuro_sucursal) AS total9 FROM sucursal");

if ($parametro == 'reporte') {
    $fecha_inicio = $_POST['fecha_inicio'];
    $fecha_fin = $_POST['fecha_fin'];
    $queryACaja = mysqli_query($con, "SELECT *, nombre_usuario FROM auditoria_caja left join usuario ON id_usuario = usuario_acaja where sucursal_acaja = $id_suc AND '$fecha_inicio 00:00:00' < fecha_acaja AND '$fecha_fin 23:59:59' > fecha_acaja ORDER BY fecha_acaja DESC");
} else {
    if ($id_suc != 0)
        $queryACaja = mysqli_query($con, "SELECT *, nombre_usuario FROM auditoria_caja left join usuario ON id_usuario = usuario_acaja where sucursal_acaja = $id_suc ORDER BY fecha_acaja DESC");
    else
        $queryACaja = mysqli_query($con, "SELECT *, nombre_usuario, nombre_sucursal FROM auditoria_caja left join usuario ON id_usuario = usuario_acaja LEFT JOIN sucursal ON id_sucursal = sucursal_acaja ORDER BY fecha_acaja DESC");
}
$max_value = 999999;
while ($row = mysqli_fetch_array($querySucursal)) {
    $nombre_sucursal = $row['nombre_sucursal'];
    if ($id_suc != 0){
    $monto_sucursal = $row['montoEfectivo_sucursal'];
    $montoCredito_sucursal = $row['montoCredito_sucursal'];
    $montoCheques_sucursal = $row['montoCheques_sucursal'];
    $montoEfectivoDolar_sucursal = $row['montoEfectivoDolar_sucursal'];
    $montoChequesDolar_sucursal = $row['montoChequesDolar_sucursal'];
    $montoCreditoDolar_sucursal = $row['montoCreditoDolar_sucursal'];
    $montoEfectivoEuro_sucursal = $row['montoEfectivoEuro_sucursal'];
    $montoChequesEuro_sucursal = $row['montoChequesEuro_sucursal'];
    $montoCreditoEuro_sucursal = $row['montoCreditoEuro_sucursal'];
    } else {
    $monto_sucursal = $row['total1'];
    $montoCredito_sucursal = $row['total2'];
    $montoCheques_sucursal = $row['total3'];
    $montoEfectivoDolar_sucursal = $row['total4'];
    $montoChequesDolar_sucursal = $row['total5'];
    $montoCreditoDolar_sucursal = $row['total6'];
    $montoEfectivoEuro_sucursal = $row['total7'];
    $montoChequesEuro_sucursal = $row['total8'];
    $montoCreditoEuro_sucursal = $row['total9'];
    $tope_value = $max_value;
    }
}

//llamo a inicio.php para que incluir la cabecera y los recursos que usamos en la pagina.
include("inicio.php");
?>

<!-- page content -->
<div class="right_col" role="main">
    <div>
        <div class="page-title">
            <div class="title_left">
                <?php if ($parametro == 'reporte') { ?>
                <h3>Reporte diario de caja de <b><?php echo $nombre_sucursal; ?></b></h3>
                <?php } else {
                    if ($id_suc != 0) { ?>
                <h3>Gestión de caja de <b><?php echo $nombre_sucursal; ?></b></h3>
                <?php } else { ?>
                <h3>Gestión de caja (consolidación)</b></h3>
                <?php }
                } ?>
            </div>

            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal"
                data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <?php if ($parametro != 'reporte') { ?>
                    <div class="x_content">
                        <div class="x_title">
                        <ul> <h4>Moneda: Peso</h4>
                            <li>Dinero en Efectivo actual en caja: <b style="color:red"><?php echo '$' . $monto_sucursal; ?></b> </li>
                            <li>Dinero en Cheque actual de la caja: <b style="color:red"><?php echo '$' . $montoCheques_sucursal; ?></b></li>
                            <li>Dinero en Tarjeta actual de la caja: <b style="color:red"><?php echo '$' . $montoCredito_sucursal; ?></b></li>
                            <hr>
                        </ul>
                        <ul> <h4>Moneda: Dolar</h4>
                            <li>Dinero en Efectivo actual de la caja: <b style="color:red"><?php echo '$' . $montoEfectivoDolar_sucursal; ?></b></li>
                            <li>Dinero en Cheque actual de la caja: <b style="color:red"><?php echo '$' . $montoChequesDolar_sucursal; ?></b></li>
                            <li>Dinero en Tarjeta actual de la caja: <b style="color:red"><?php echo '$' . $montoCreditoDolar_sucursal; ?></b></li>
                            <hr>
                        </ul>
                        <ul> <h4>Moneda: Euro</h4>
                            <li>Dinero en Efectivo actual de la caja: <b style="color:red"><?php echo '$' . $montoEfectivoEuro_sucursal; ?></b></li>
                            <li>Dinero en Cheque actual de la caja: <b style="color:red"><?php echo '$' . $montoChequesEuro_sucursal; ?></b></li>
                            <li>Dinero en Tarjeta actual de la caja: <b style="color:red"><?php echo '$' . $montoCreditoEuro_sucursal; ?></b></li>
                        </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <?php if ($id_suc != 0) { ?>
                    <div class="x_content">
                        <div class="x_title">
                            <h2>Agregar/Retirar dinero de la caja</h2>
                            <div class="clearfix"></div>
                        </div>
                        <form method="post" action="funciones/verSucursal_funcion.php" novalidate>
                            <input type="text" name="id" hidden value="<?php echo $id_suc; ?>">
                            <div id="accion">
                                <input type="text" name="accion" hidden value="agregar">
                            </div>
                            <div class="field item form-group align-items-center">
                                <label class="col-form-label label-align">Agregar</label>
                                <div class="col-md-6 col-sm-6 align-items-center">
                                    <input class="js-switch" type="checkbox" name="checkcaja" id="checkcaja"
                                        onchange="switchAgregarRetirar()">
                                    <label class="col-form-label ml-1">Retirar</label>
                                </div>
                            </div>
                            <input type="hidden" id="max_value" value="<?php echo $max_value; ?>" />
                            <input type="hidden" id="tope_value" value="<?php echo $tope_value; ?>" />
                            <input type="hidden" id="monto_sucursal" value="<?php echo $monto_sucursal; ?>" />
                            <div class="field item form-group">
                                <label class="col-form-label col-md-0 col-sm-0 col-1  label-align">Monto<span
                                        class="required">*</span></label>
                                <div id="value_monto" class="col-md-2 col-sm-2 col-12  ">
                                    <input class="form-control" name="monto" id="monto" required="required" step="0.01"
                                        data-validate-minmax="1,<?php echo $tope_value ?>" type="number"
                                        placeholder="Monto" />
                                </div>
                            <!-- <div class="field item form-group"> -->
                                <label class="col-form-label col-md-0 col-sm-1 label-align">Observación<span
                                        class="required">*</span></label>
                                <div class="col-md-6 col-sm-5 col-12">
                                    <input type="text" placeholder="Observación de la operación" name="descripcion"
                                        class="form-control" required="required">
                                </div>
                            <!-- </div> -->
                            </div> 
                            <div class="field item form-group col-md-12 col-sm-12 align-items-center">
                                <button type="submit" style="font-size: 13px" class="btn btn-success"
                                    id="cajaBtn">Agregar</button>
                            </div>
                        </form>
                    </div>
                    <?php }
                    } ?>
                    <div class="x_content">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Ingresos y egresos de dinero</h2>
                                </div>
                                <div class="table-responsive ">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th class='text-center'>ID de la operación</th>
                                                <?php if ($id_suc == 0) { ?>
                                                <th class='text-center'>Sucursal</th>
                                                <?php } ?>
                                                <th class='text-center'>Medio de Pago</th>
                                                <th class='text-center'>Monto anterior a la operación</th>
                                                <th class='text-center'>Monto</th>
                                                <th class='text-center'>Monto posterior a la operación</th>
                                                <th class='text-center'>Responsable</th>
                                                <th class='text-center'>Descripción</th>
                                                <th class='text-center'>Fecha</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            while ($row = mysqli_fetch_array($queryACaja)) {
                                                $signo = "";
                                                $id_acaja = $row['id_acaja'];
                                                if ($id_suc == 0)
                                                    $nombre_sucursal = $row['nombre_sucursal'];
                                                    $medio_acaja = $row['medio_acaja'];
                                                    $moneda_acaja = $row['moneda_acaja'];
                                                $monto_acaja = $row['monto_acaja'];
                                                $montoAnterior_acaja = $row['montoAnterior_acaja'];
                                                $nombre_usuario = $row['nombre_usuario'];
                                                $montoPosterior_acaja = $row['montoPosterior_acaja'];
                                                $descripcion_acaja = $row['descripcion_acaja'];
                                                $fecha_acaja = $row['fecha_acaja'];
                                                $fecha_acaja = date("d/m/Y - H:i", strtotime($fecha_acaja));

                                            ?>
                                            <tr class="<?php echo $text_class; ?>">
                                                <td class='text-center'><?php echo $id_acaja; ?></td>
                                                <?php if ($id_suc == 0) { ?>
                                                    <td class='text-center'><?php echo $nombre_sucursal; ?></td>
                                                <?php } ?>
                                                <td class='text-center'><?php echo $medio_acaja."/".$moneda_acaja; ?></td>

                                                <?php 
                                                    $colorTd = "";
                                                    if ($montoAnterior_acaja < $montoPosterior_acaja){
                                                        $colorTd = 'ForestGreen';
                                                    }else{
                                                        $colorTd = 'red';
                                                        $signo = "-";
                                                    } ?>
                                                <td class='text-center'>$<?php echo $montoAnterior_acaja; ?></td>
                                                <td class='text-center' style='color:<?php echo $colorTd?>'>
                                                    <?php echo $signo."$".$monto_acaja; ?></td>
                                                <td class='text-center'>$<?php echo $montoPosterior_acaja; ?></td>
                                                <td class='text-center'><?php echo $nombre_usuario; ?></td>
                                                <td class='text-center'><?php echo $descripcion_acaja; ?></td>
                                                <td class='text-center'><?php echo $fecha_acaja; ?></td>
                                            </tr>
                                            <?php
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
                <?php if ($parametro == 'reporte') { ?>
                <form action="reporteCajaPDF.php?parametro=reporte" method="post">

                    <input type="date" hidden name="fecha_inicio" value="<?php echo $fecha_inicio; ?>">
                    <input type="date" hidden name="fecha_fin" value="<?php echo $fecha_fin; ?>">
                    <input type="text" hidden name="sucursal" value="<?php echo $id_suc; ?>">
                    <button type="submit" style="float:right" class="btn btn-success">Descargar PDF del
                        reporte</button>
                </form>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</div>
<!-- /page content -->

<?php
include("fin.php");
?>

<script type="text/javascript">
<?php if ($parametro == 'reporte') { ?>
window.onload = cambiarTitulo("Reporte diario de caja");
<?php } else { ?>
window.onload = cambiarTitulo("Caja de sucursal <?php echo $nombre_sucursal ?>");
<?php } ?>
</script>