<?php 
$manualVenta = "
        <h6>Cargar una venta a un cliente no registrado:</h6>
            <li>Seleccione 'Venta a Cliente no registrado', si el cliente que desea comprar no esta registrado en el sistema.</li>
        <br>
        <h6>Cargar una venta a un cliente registrado:</h6>
            <li>Ingrese en el cuadro de texto el Nombre o DNI para buscar el cliente que desea comprar.</li>
        <br>
        <h6>Boton 'Siguiente':</h6>
            <li>Una vez seleccionado el cliente o 'Venta a Cliente no registrado' presione el boton 'Siguiente' para continuar con la operación.</li>
        <br>
        <h6>Cargar productos y sus cantidades al pedido:</h6>
            <li>Podrá agregar los productos y sus respectivas cantidades en la pantalla siguiente.</li>
        <br>
";
$manualVentaCliente = "
        <h6>Cargar productos y sus cantidades al pedido:</h6> 
            <li>Seleccione los productos y las cantidades que desea agregar al pedido.</li>
            <li>Haga clic en '+ Agregar producto' para cargar mas productos al pedido.</li>
        <br>
        <h6>Botón 'Procesar Pedido':</h6> 
            <li>Haga clic en 'Procesar Pedido' para pasar a la pantalla de confirmacion del pedido.</li>
        <br>
        <h6>ATENCION:</h6>
            <li>En caso de que se solicite una cantidad mayor al stock disponible en la sucursal, se otorgará la mayor cantidad posible.</li>
        <br>
            <li>Los datos marcados con * son obligatorios.</li>
";
$manualCrearPedido = "
        <h6>Para finalizar la carga de una venta:</h6> 
            <li>Revise que los datos del pedido son correctos.</li>
            <li>Haga clic en 'Confirmar pedido' para finalizar la carga del pedido.</li>
        <br>
        <h6>Botón 'Regresar':</h6>      
            <li>Haga clic en 'Regresar' para volver a la pantalla anterior o si desea continuar cargando productos.</li>
        <br>
        ATENCION: 
            <li>Una vez confirmado el pedido comunicar al cliente que pase por caja para abonar el mismo al administrador de caja.</li>
        <br>
";
$manualVentasACobrar = "
        <h6>Ver detalle de venta cargada:</h6>
            <li>Haga click sobre una venta cargada. El sistema lo redirigirá al detalle de la venta seleccionada.</li>
        <br>
        <h6>Confirmar la venta:</h6>
            <li>En la pantalla siguiente podrá confirmar la venta que seleccionó.</li>
        <br>
        <h6>Buscar venta por nombre de cliente:</h6>      
            <li>En el cuadro de busqueda ingrese el nombre del cliente. La lista 'Ventas Pendientes' mostrará los resultados encontrados.</li>
        <br>
";
$manualVerVenta = "
        <h6>Completar datos de la venta:</h6>
        <li>Revise que los datos ingresados en los pasos anteriores sean correctos.</li>
        <br>
        <h6>Medios de Pago:</h6>
        <li>Seleccione un medio de pago.</li>
        <li>Ingrese el monto que abonará el cliente (ATENCIÓN: En caso de que el monto abonado sea menor al valor del pedido, se sumará la diferencia a la deuda del cliente)</li>
        <li>Presione '+Agregar otro medio de pago' para sumar medios de pago.</li>
        <li>Complete los campos obligatorios.</li>
        <br>
        <h6>Abonar Venta: </h6>
        <li>Presione 'Abonar Venta' para finalizar la operación.</li>
        <li>Presione 'Cancelar Venta' para cancelar la venta. Tenga en cuenta que esta operación no se puede deshacer.</li>
        <br><br>
        <li>Los datos marcados con * son obligatorios.</li>
";
 
$manualStock = "
        <h6>Consultar stock de sucursal:</h6>
            <li>Seleccione en el campo 'Sucursal' la sucursal a consultar.</li>
            <li>La lista mostrará el stock de los productos de la sucursal seleccionada.</li>
        <br>
        <h6>Buscar Stock por nombre de producto:</h6>
            <li>Ingrese en el cuadro de busqueda el nombre del producto a buscar</li>
            <li>La lista de stock de productos mostrará los resultados encontrados</li>
        <br>
        <h6>Ingresar stock en la sucursal seleccionada:</h6>
            <li>Haga click en el botón de la derecha de un item para desplegar las opciones.</li>
            <li>Seleccione 'Ingresar Stock'</li>
            <li>El sistema lo redirigirá a la pantalla 'Ingresar Stock' donde podrá ingresar stock del item seleccionado.</li>
        <br>
        <h6>Retirar stock de la sucursal seleccionada:</h6>
            <li>Haga click en el botón de la derecha de un item para desplegar las opciones.</li>
            <li>Seleccione 'Retirar Stock'</li>
            <li>El sistema lo redirigirá a la pantalla 'Retirar Stock' donde podrá retirar stock del item seleccionado.</li>
        <br>
        <h6>Enviar stock a otra sucursal:</h6>
            <li>Haga click en el botón de la derecha de un item para desplegar las opciones.</li>
            <li>Seleccione 'Enviar Stock a otra Sucursal'</li>
            <li>El sistema lo redirigirá a la pantalla 'Mover Stock de Sucursal' donde podrá direccionar stock del item seleccionado a otra sucursal.</li>
            <br>
            <li>Los datos marcados con * son obligatorios.</li>
";

$manualIngresarStock ="
        <h6>Ingresar Stock:</h6>
        <li>Seleccione un proveedor de la lista desplegable.</li>
        <li>Ingrese el valor del pedido.</li>
        <li>Ingrese cuando dinero se le ha pagado al proveedor. (*)</li>
        <li>Seleccione un medio de pago.(**)</li>
        <li>Ingrese cantidad a agregar.(***)</li>
        <li>En 'Observacion' puede cargar un texto con información sobre la operación actual.</li>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        (*) La diferencia negativa se sumará a la deuda con el proveedor.<br>
        (**) Los campos obligatorios serán distintos dependiendo del tipo de medio de pago.<br>
        (***) Tenga en cuenta que el lote será el mismo con el que se configuró el producto.<br>
";
$manualRetirarStock ="
        <h6>Retirar Stock:</h6>
        <li>Ingrese en el cuadro de texto la cantidad que desea retirar. El valor debe estar entre 0 y el stock existente.</li>
        <li>En 'observacion' puede cargar un texto con información sobre la operación actual.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos que aparecen en color gris no pueden modificarse en esta pantalla.</li>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualCambioSucursalStock ="
        <h6>Enviar stock a otra sucursal:</h6>
        <li>En 'Sucursal de Destino' seleccione la sucursal a la que desea enviar el stock.</li>
        <li>En 'Cantidad a Mover' ingrese la cantidad del producto que desea que sea transferida. El valor debe estar entre 0 y el stock existente.</li>
        <li>En 'Observacion' puede cargar un texto con información sobre la operación actual.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos que aparecen en color gris no pueden modificarse en esta pantalla.</li>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCaja = "
        <h6>Gestion de Caja de una sucursal: </h6>
        <li>Presione el nombre de la Sucursal que desea gestionar.</li>
        <li>Será redirigido a la pagina siguiente donde podrá:
        <ul>
        <li>Ver los movimientos de caja.</li>
        <li>Agregar o retirar dinero de la Sucursal.</li>
        </li>
        </ul>
        <br>
        ATENCION
        <li>No puede efectuar movimientos en Sucursales inactivas.</li>
";

$manualVerSucursal = "
        <h5>Gestión de caja de sucursal: </h5>
        <h6>Agregar dinero:</h6>
        <li>Presione el Switch 'Agregar/Retirar' de modo que quede en 'Agregar'.</li>
        <li>Ingrese el monto que desea agregar.</li>
        <li>En 'Observacion' cargue un texto con información sobre la operación actual.</li>
        <li>Presione el botón Agregar para finalizar la operación.</li>
        <br>
        <h6>Retirar dinero:</h6>
        <li>Presione el Switch 'Agregar/Retirar' de modo que quede en 'Retirar'.</li>
        <li>Ingrese el monto que desea retirar. Debe ser menor que el que existe en la sucursal.</li>
        <li>En 'Observacion' cargue un texto con información sobre la operación actual.</li>
        <li>Presione el botón Retirar para finalizar la operación.</li>
        <br>
        <h6>Ingresos y egresos de dinero:</h6>   
        <li>En la lista se presentan los movimientos de caja de la sucursal.</li>
        <li>Los montos en VERDE son ingresos de dinero.</li>
        <li>Los montos en ROJO son egresos de dinero.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualVentaVirtual = "
        <h6>Venta Virtual:</h6>
        <li>Para cargar una venta virtual presione el boton 'Seleccionar Archivo'.</li>
        <li>Se abrira una ventana del explorador para que seleccione el archivo csv que desea cargar.</li>
        <li>Presione el boton 'Procesar el archivo' para finalizar la cargar del CSV.</li>
        <li>En la pantalla siguiente podrá verificar los datos procesados.</li>
        <br>
        <H6>Ejemplos del archivo CSV.</H6>
        <li>Puede descargar el formato del archivo presionando la palabra FORMATO.</li>
        <li>Puede descargar un ejemplo del archivo presionando la palabra EJEMPLO.</li>
";

$manualListadoVentaVirtual = "
        <h6>Ventas Virtuales: detalle</h6>
        <li>En esta pantalla se muestran ya procesados los datos del archivo csv cargado en el paso anterior.</li>
        <li>En caso de no contar con stock suficiente se cargará la cantidad maxima existente.</li>
        <li>En caso de no contar con stock suficiente el dinero abonado quedará como saldo positivo en la cuenta corriente del cliente.</li>
";

$manualABMParametroProducto = "
        <h6>Agregar un nuevo Producto:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de Producto.</li>
        <br>
        <h6>Editar Producto:</h6>
        <li>Presione el nombre del Producto que desea editar. Será redirigido a la pantalla de edición de Producto.</li>
        <br>
        <h6>Modificar precios masivamente:</h6>
        <li>Presione el boton 'Modificar Precios Masivamente'. Será redirigido a la pagina de 'Modificacion masiva de precios'.</li>
        <br>
        <h6>Buscar Producto:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre del producto que desea consultar. El listado mostrará los productos encontrados.</li>
";

$manualABMParametroSucursal = "
        <h6>Agregar un nuevo Sucursal:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de Sucursal.</li>
        <br>
        <h6>Editar Sucursal:</h6>
        <li>Presione el nombre de la Sucursal que desea editar. Será redirigido a la pantalla de edición de Sucursal.</li>
        <br>
        <h6>Buscar Sucursal:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre del Sucursal que desea consultar. El listado mostrará las Sucursales encontradas.</li>
        <br>
";

$manualABMParametroCliente = "
        <h6>Agregar un nuevo cliente:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de Cliente.</li>
        <br>
        <h6>Editar cliente:</h6>
        <li>Presione el nombre del cliente que desea editar. Será redirigido a la pantalla de edición de clientes.</li>
        <br>
        <h6>Buscar cliente:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre del cliente que desea consultar. El listado mostrará los clientes encontrados.</li>
";

$manualABMParametroCategoria = "
        <h6>Agregar una nueva Categoria:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de Categorias.</li>
        <br>
        <h6>Editar Categoria:</h6>
        <li>Presione el nombre de la Categoria que desea editar. Será redirigido a la pantalla de edición de Categorias.</li>
        <br>
        <h6>Buscar Categoria:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre de la Categoria que desea consultar. El listado mostrará las Categorias encontradas.</li>
";

$manualABMParametroSubCategoria = "
        <h6>Agregar una nueva SubCategoria:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de SubCategorias.</li>
        <br>
        <h6>Editar SubCategoria:</h6>
        <li>Presione el nombre de la SubCategoria que desea editar. Será redirigido a la pantalla de edición de SubCategorias.</li>
        <br>
        <h6>Buscar SubCategoria:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre de la SubCategoria que desea consultar. El listado mostrará las SubCategorias encontradas.</li>
        <br>
        <h6>Subcategorias de Pintura:</h6>
        <li>Las subcategorias de la categoria 'Pintura' se agregarán automaticamente a la lista 'Venta y Preparación de Pinturas' de la seccion de 'Ventas'</li>
";

$manualABMParametroProveedor = "
        <h6>Agregar un nuevo Proveedor:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de Proveedores.</li>
        <br>
        <h6>Editar Proveedor:</h6>
        <li>Presione el nombre del Proveedor que desea editar. Será redirigido a la pantalla de edición de Proveedores.</li>
        <br>
        <h6>Buscar Proveedor:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre del Proveedor que desea consultar. El listado mostrará los Proveedores encontrados.</li>
";

$manualCrearParametroCliente = "
        <h6>Cargar Cliente nuevo:</h6>
        <li>Complete los campos obligatorios.</li>
        <li>Ingrese el limite de cuenta corriente que desea asignar al cliente.</li>
        <li>Seleccione una condición fiscal de la lista desplegable 'Condicion fiscal'.</li>
        <li>Seleccione un TIPO de persona de la lista desplegable 'Tipo'.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCrearParametroCategoria = "
        <h6>Cargar Categoria nueva:</h6>
        <li>Ingrese el nombre de la categoria nueva.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCrearParametroSubCategoria = "
        <h6>Cargar SubCategoria nueva:</h6>
        <li>Ingrese el nombre de la SubCategoria nueva.</li>
        <li>Ingrese a que Categoria pertenece.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Subcategorias de Pintura:</h6>
        <li>Las subcategorias de la categoria 'Pintura' se agregarán automaticamente a la lista 'Venta y Preparación de Pinturas' de la seccion de 'Ventas'</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCrearParametroProveedor = "
        <h6>Cargar Proveedor nuevo:</h6>
        <li>Complete los campos obligatorios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCrearParametroProducto = "
        <h6>Cargar producto nuevo:</h6>
        <li>Complete los campos obligatorios.</li>
        <li>Seleccione una categoria de la lista desplegable 'Categoria del Producto'. Se habilitarán las subcategorias de la categoria seleccionada.</li>
        <li>Seleccione una subcategoria de la lista desplegable 'SubCategoria del Producto'.</li>
        <li>Ingrese cantidad por lote.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Validacion de Stock minimo:</h6>
        <li>Los productos que tienen la validacion de stock minimo activada, generan automaticamente una solicitud de reposicion de stock</li>
        <li>Seleccione un proveedor de la lista desplegable. A este se le enviará la solicitud automatica de reposición de stock.</li>
        <li>Ingrese el Stock minimo. Al alcanzar este valor de stock se activará la solicitud automatica de reposición de stock.</li>
        <li>Ingrese la cantidad a solicitar automaticamente.</li>
        <li>Ingrese el Stock de seguridad. Al alcanzar este valor se notificará automaticamente al Gerente para que realice un pedido de Stock manual.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCrearParametroSucursal = "
        <h6>Cargar Sucursal nueva:</h6>
        <li>Complete los campos obligatorios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualEditarParametroCliente = "
        <h6>Editar cliente:</h6>
        <li>En esta pantalla puede editar los datos del cliente seleccionado en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualEditarParametroCategoria = "
        <h6>Editar Categoria:</h6>
        <li>En esta pantalla puede editar los datos de la Categoria seleccionada en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Activar / desactivar una Categoria:</h6>
        <li>Seleccione un valor de la lista desplegable 'Estado'. Aqui puede activar o desactivar una Categoria.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualEditarParametroSubCategoria = "
        <h6>Editar SubCategoria:</h6>
        <li>En esta pantalla puede editar los datos de la SubCategoria seleccionada en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Activar / desactivar una SubCategoria:</h6>
        <li>Seleccione un valor de la lista desplegable 'Estado'. Aqui puede activar o desactivar una Categoria.</li>
        <br>
        <h6>Subcategorias de Pintura:</h6>
        <li>Las subcategorias de la categoria 'Pintura' se agregarán automaticamente a la lista 'Venta y Preparación de Pinturas' de la seccion de 'Ventas'</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualEditarParametroProveedor = "
        <h6>Editar Proveedor:</h6>
        <li>En esta pantalla puede editar los datos del cliente seleccionado en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Activar / desactivar un Proveedor:</h6>
        <li>Seleccione un valor de la lista desplegable 'Estado'. Aqui puede activar o desactivar un Proveedor.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualEditarParametroSucursal = "
        <h6>Editar Sucursal:</h6>
        <li>En esta pantalla puede editar los datos de la Sucursal seleccionada en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualEditarParametroProducto = "
        <h6>Editar Producto:</h6>
        <li>En esta pantalla puede editar los datos del Producto seleccionado en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Validacion de Stock minimo:</h6>
        <li>Los productos que tienen la validacion de stock minimo activada, generan automaticamente una solicitud de reposicion de stock</li>
        <li>Seleccione un proveedor de la lista desplegable. A este se le enviará la solicitud automatica de reposición de stock.</li>
        <li>Ingrese el Stock minimo. Al alcanzar este valor de stock se activará la solicitud automatica de reposición de stock.</li>
        <li>Ingrese la cantidad a solicitar automaticamente.</li>
        <li>Ingrese el Stock de seguridad. Al alcanzar este valor se notificará automaticamente al Gerente para que realice un pedido de Stock manual.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualModificarPrecio = "
        <h6>Modificar precios masivamente:</h6>
        <li>Seleccione una categoria de la lista desplegable. Si selecciona 'Todas las categorias' la modificacion afectará a todas las categorias de sistema.</li><br>
        <li>Seleccione una accion. 
        <ul>
        <li>Si desea incrementar los precios seleccione 'Aumentar'.</li>
        <li>Si desea reducir los precios seleccione 'Descontar'.</li>
        </ul>
        </li>
        <li>Ingrese el valor que tendran los precios.</li><br>
        <li>Seleccione la forma en que se realizará la modificacion masiva: 
        <ul>
        <li>Seleccione 'Monto fijo' si desea que el valor de modificacion sea independiente de los precios actuales. Se sumara o descontará este valor de los precios actuales.</li>
        <li>Seleccione '% Porcentaje' si desea que el valor de modificacion este ligado al valor de los precios actuales. Se sumara o descontará el porcentaje del precio actual.</li>
        </ul>
        </li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualABMUsuarios = "
        <h6>Agregar un nuevo Usuario:</h6>
        <li>Presione el boton 'Agregar'. Será redirigido a la pagina de creación de Usuario.</li>
        <br>
        <h6>Editar Usuario:</h6>
        <li>Presione el icono <i class=' fa fa-pencil'></i>  que se encuentra a la derecha del Usuario que desea editar. Será redirigido a la pantalla de edición de Usuarios.</li>
        <br>
        <h6>Blanquear contraseña:</h6>
        <li>Presione el icono <i class='fa fa-check'></i> que se encuentra a la derecha del usuario cuya contraseña desea blanquear. Se blanqueará la contraseña del mismo.</li>
        <br>
        <h6>Buscar Usuario:</h6>
        <li>Ingrese en el cuadro de busqueda el nombre del Usuario que desea consultar. El listado mostrará los Usuarios encontrados.</li>
";

$manualEditarUsuario = "
        <h6>Editar usuario:</h6>
        <li>En esta pantalla puede editar los datos del usuario seleccionado en el paso anterior.</li>
        <li>Modifique los datos que desee. Tenga en cuenta que los campos obligatorios no pueden quedar vacios.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <h6>Activar / desactivar un Usuario:</h6>
        <li>Seleccione un valor de la lista desplegable 'Estado'. Aqui puede activar o desactivar un Usuario.</li>
        <br>
        <h6>Cambiar sucursal de un Usuario:</h6>
        <li>Seleccione un valor de la lista desplegable 'Sucursal'.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";

$manualCrearUsuario = "
        <h6>Cargar Proveedor Usuario:</h6>
        <li>Complete los campos obligatorios.</li>
        <li>Seleccione un valor de la lista desplegable 'Sucursal'.</li>
        <li>Seleccione un valor de la lista desplegable 'Perfil'.</li>
        <br>
        <li>Presione el boton 'Cargar' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualReporteStockFaltante = "
        <h6>Reporte de stock faltante:</h6>
        <li>En esta pantalla podrá ver los items activos con stock faltante</li>
";
$manualIndex = "
        <h5>Bienvenido al sistema de Gestion integral de Ferreterias.</h5>
        <h6>Seleccione una operación:</h6>
                <li>Seleccione del panel lateral izquierdo la operación que desea realizar.</li>
        <br>
        <h6>Ayuda del sistema:</h6>
                <li>Presione el simbolo <i class='fa fa-question-circle'></i> para visualizar la ayuda.</li>
                <li>Encontrará en simbolo  <i class='fa fa-question-circle'></i> en todas las pantallas del sistema.</li>
        <br>
        <h6>Cambio de contraseña:</h6>
                <li>Haga click en su nombre de usuario en la parte superior derecha de la pantalla</li>
                <li>Haga click en cambiar contraseña</li>
                <li>El sistema lo redirigirá a la pantalla de cambio de contraseña.</li>
        <br>
        <h6>Salir del sistema:</h6>
                <li>Presione su nombre de usuario ubicado en la parte superior derecha de la pantalla</li>
                <li>Presione en 'salir'</li>
                <li>El sistema lo redirigirá a la pantalla de Login.</li>
";
$manualCambiarPass = "
        <h6>Cambiar contraseña:</h6>
        <li>En el campo 'Contraseña Actual' ingrese la contraseña que posee actualmente.</li>
        <li>En el campo 'Nueva Contraseña' ingrese una nueva contraseña. Tenga en cuenta que la misma debe contener: </li>
        <ul>
                <li>Al menos 8 caracteres.</li> 
                <li>Al menos una mayuscula.</li>
                <li>Al menos una minuscula.</li>
                <li>Al menos un numero.</li>
        </ul>
        <li>En el campo 'Repetir Nueva Contraseña' ingrese nuevamente la nueva contraseña.</li>
        <li>Presione el simbolo <i class='fa fa-eye'></i> para mostrar el texto introducido.</li>
        <li>Presione el simbolo <i class='fa fa-eye-slash'></i> para ocultar el texto introducido.</li>
        <br>
        <li>Presione el boton 'Cambiar Contraseña' para finalizar la operación.</li>
        <li>Presione el boton 'Limpiar Campos' si desea eliminar los datos cargados en el formulario.</li>
        <br>
        ";
$manualPintura = "
        <h5>Nueva Venta de Pintura:</h5>
        <h6>Cargar una venta a un cliente no registrado:</h6>
            <li>Seleccione 'Venta a Cliente no registrado', si el cliente que desea comprar no esta registrado en el sistema.</li>
        <br>
        <h6>Cargar una venta a un cliente registrado:</h6>
            <li>Ingrese en el cuadro de texto el Nombre o DNI para buscar el cliente que desea comprar.</li>
        <br>
        <h6>Boton 'Siguiente':</h6>
        <li>Una vez seleccionado el cliente o 'Venta a Cliente no registrado' presione el boton 'Siguiente' para continuar con la operación.</li>
        <br>
        <h6>Cargar productos y sus cantidades al pedido:</h6>
        <li>Podrá agregar los productos y sus respectivas cantidades en la pantalla siguiente.</li>
        <br>
";
$manualPinturaCliente = "
        <h6>Nueva Venta de Pintura:</h6>
        <li>De la lista desplegable 'Pintura 1' seleccione una pintura.</li>
        <li>En 'cantidad de litros' ingrese la cantidad deseada.</li>
        <br>
        <h6>Pintura 2 - Pintura 3 (opcionales):</h6>
        <li>Opcionalmente puede agregar dos pinturas mas ademas de la principal.</li>
        <li>Debe ingresar cantidad para las pinturas opcionales agregadas.</li>
        <br>
        <h6>Boton 'Procesar pedido':</h6>
        <li>Presione el boton 'Procesar pedido' para ir al siguiente paso de la operación.</li>
        <li>En la pantalla siguiente podrá ver un resumen de la operación y confirmar la misma.</li>
";
$manualSeleccionSucursal = "
        <h6>Reporte de caja diaria:</h6>
        <li>Seleccione una sucursal de la lista desplegable 'Sucursal'.</li>
        <li>Ingrese en el cuadro de texto 'Fecha de inicio' la fecha de inicio del rango de consulta.</li>
        <li>Ingrese en el cuadro de texto 'Fecha de Fin' la fecha de fin del rango de consulta.</li>
        <li>Tambien puede seleccionar una fecha presionando <i class='fa fa-calendar-o' aria-hidden='true'></i></li>
        <li>Presione el boton 'Generar reporte' para ser visualizar el reporte generado</li>
        <br>
        <h6>Atencion:</h6>
        <li>La fecha de inicio debe ser anterior o igual a la fecha de fin.</li>
        <li>Si desea consultar un dia en particular la fecha de inicio y la fecha de fin deben ser iguales.</li>
        <li>Los datos marcados con * son obligatorios.</li>
        
";
$manualVerSucursalReporte = "
        <h5>Reporte diario de caja</h5>
        <h6>Listado 'Ingresos y egresos de dinero:</h6>
        <li>Aqui podra ver el detalle de los movimientos de caja del rango de fechas seleccionado en la pantalla anterior.</li>
        <br>
        <h6>Monto:</h6>
        <li>Los valores resaltados en color rojo son egresos de dinero (retiro manual/pago a proveedores).</li>
        <li>Los valores resaltados en color verde con ingresos de dinero (ingreso manual/ventas realizadas).</li>
        <li>Los valores en monedas extrajeras figuran convertidos a moneda local con el valor del dia de emision del reporte.</li>
        <br>
        <h6>Boton 'Descargar PDF del reporte':</h6>
        <li>Presione el boton 'Descargar PDF del reporte' para generar el reporte en PDF del reporte actual.</li>
";
$manualReporteVentas = "
        <h6>Reporte de ventas:</h6>
        <li>Seleccione una sucursal de la lista desplegable 'Sucursal' si desea consultar una sucursal en particular.</li>
        <li>Seleccione 'Todas las Sucursales' de la lista desplegable 'Sucursal' si desea consultar todas las sucursales.</li>
        <li>Seleccione un Vendedor de la lista desplegable 'Vendedor' si desea consultar un vendedor en particular.</li>
        <li>Seleccione 'Todas los vendedores' de la lista desplegable 'Vendedor' si desea consultar todas los vendedores.</li>  
        <li>Ingrese en el cuadro de texto 'Fecha de inicio' la fecha de inicio del rango de consulta.</li>
        <li>Ingrese en el cuadro de texto 'Fecha de Fin' la fecha de fin del rango de consulta.</li>
        <li>Tambien puede seleccionar una fecha presionando <i class='fa fa-calendar-o' aria-hidden='true'></i></li>
        <li>Presione el boton 'Generar reporte' para visualizar el reporte generado</li>
        <br>
        <br>
        <h6>Atencion:</h6>
        <li>La fecha de inicio debe ser anterior o igual a la fecha de fin.</li>
        <li>Si desea consultar un dia en particular seleccione la misma fecha para 'Fecha de inicio' y 'Fecha de fin'.</li>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualReporteStockFaltante = "
        <h6>Reporte de stock faltante:</h6>
        <li>En esta pantalla podrá visualizar los productos que tienen 0 stock.</li>
";
$manualReporteStockMinimo = "
        <h6>Reporte de items con riesgo de stock minimo</h6>
        <li>En esta pantalla podrá visualizar los items con riesgo por stock minimo.</li>
        <li>El grafico superior muestra los primeros diez items con mayor porcentaje de riesgo (porcentaje por debajo del stock minimo).</li>
";
$manualReporteStockSeguridad = "
        <h6>Reporte de items con riesgo de stock de seguridad</h6>
        <li>En esta pantalla podrá visualizar los items con riesgo por stock de seguridad.</li>
        <li>El grafico superior muestra los primeros diez items con mayor porcentaje de riesgo (porcentaje por debajo del stock de seguridad).</li>
";
$manualReporteComision = "
        <h6>Reporte de Comisiones de vendedores:</h6>
        <li>Seleccione un mes de la lista desplegable 'Mes'.</li>
        <li>Seleccione un año de la lista desplegable 'Año'.</li>
        <li>En el campo 'Porcentaje de comision' ingrese el porcentaje que desea consultar.</li>
        <br>
        <li>Presione el boton 'Generar reporte' para visualizar el reporte generado.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualReporteSucursal = "
        <h6>Reporte de ventas por sucursal:</h6>
        <li>En esta pantalla podrá visualizar el ranking de ventas por sucursal.</li>
";
$manualReporteVendedor = "
        <h6>Reporte de ventas por vendedor:</h6>
        <li>En esta pantalla podrá visualizar el ranking de ventas por vendedor.</li>
"; 
$manualReporteCompras = "
        <h6>Reporte de compras:</h6>
        <li>En esta pantalla podrá visualizar el listado de compras realizadas.</li>   
";
$manualReporteTraspasos = "
        <h6>Reporte de traspasos de stock entre sucursales:</h6>
        <li>En esta pantalla podrá visualizar los movimientos de stock entre sucursales.</li>
";
$manualReporteCliente = "
        <h6>Reporte de operaciónes con clientes:</h6>
        <li>En esta pantalla podrá visualizar las operaciónes realizadas con clientes.</li>
";
$manualCuentaCorrienteClientes = "
        <h6>Consultar la cuenta corriente de un cliente:</h6>
        <li>Presione el nombre del cliente cuya cuenta corriente desea consultar. Será redirigido a la pantalla de detalles de cuenta corriente.</li>
        <br>
        <h6>Buscar cliente por Nombre o DNI:</h6>
        <li>Ingrese en el cuadro de texto el Nombre o DNI para buscar un cliente.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";
 $manualCuentaCorrienteProveedores = "
        <h6>Consultar la cuenta corriente de un Proveedor:</h6>
        <li>Presione el nombre del proveedor cuya cuenta corriente desea consultar. Será redirigido a la pantalla de detalles de cuenta corriente.</li>
        <br>
        <h6>Buscar proveedor por Nombre o DNI:</h6>
        <li>Ingrese en el cuadro de texto el Nombre o DNI para buscar un proveedor.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualCuentaCorrienteCliente = "
        
";
$manualCuentaCorrienteProveedor = "
        <h5>Cuenta corriente de proveedor</h5>
        <h6>Listado 'Historial de movimientos':</h6>
        <li>Aqui podra ver el detalle de los movimientos en la cuenta corriente del proveedor.</li>
        <br>
        <h6>Monto:</h6>
        <li>Los valores resaltados en color rojo son egresos de dinero (retiro manual/pago a proveedores).</li>
        <li>Los valores resaltados en color verde con ingresos de dinero (ingreso manual/ventas realizadas).</li>
        <br>
        <h6>Boton 'Realizar operaciónes':</h6>
        <li>Presione el boton 'Realizar operaciónes' para:</li>
        <ul>
                <li>Registar un pago.</li>
                <li>Cambiar limite de cuenta corriente.</li>
        </ul>
        <h6>Registrar un pago:</h6>
        <li>Seleccione un medio de pago.</li>
        <li>Ingrese el monto del pago a efectuar.</li>
        <li>Presione el boton 'Pagar' para finalizar la operación.</li>
        <br>
        <h6>Cambiar limite de cuenta corriente.</h6>
        <li>Ingrese el nuevo limite de cuenta corriente para el proveedor.</li>
        <li>Presione 'Cambiar' para finalizar la operación.</li>

";
$manualCuentaCorrienteOperacionesProveedor = "
        <h5>Cuenta corriente de proveedor</h5>
        <br>
        <h6>Registrar un pago:</h6>
        <li>Seleccione un medio de pago.</li>
        <li>Ingrese el monto del pago a efectuar.</li>
        <li>Presione el boton 'Pagar' para finalizar la operación.</li>
        <br>
        <h6>Cambiar limite de cuenta corriente.</h6>
        <li>Ingrese el nuevo limite de cuenta corriente del proveedor.</li>
        <li>Presione 'Cambiar' para finalizar la operación.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualCuentaCorrienteOperacionesCliente = "
        <h5>Cuenta corriente de cliente</h5>
        <br>
        <h6>Registrar un pago:</h6>
        <li>Seleccione un medio de pago.</li>
        <li>Ingrese el monto del pago a efectuar.</li>
        <li>Presione el boton 'Pagar' para finalizar la operación.</li>
        <br>
        <h6>Cambiar limite de cuenta corriente.</h6>
        <li>Ingrese el nuevo limite de cuenta corriente del cliente.</li>
        <li>Presione 'Cambiar' para finalizar la operación.</li>
        <br>
        <li>Los datos marcados con * son obligatorios.</li>
";
$manualVerVentaRealizada = "
        <h5>Ver Venta Realizada</h5>
        <br>
        <h6>En esta pagina se lista la información de una venta ya realizada</h6>
        
        <br>
        
";

$manualListadoDeVentas = "
<h5>Listado de Ventas Realizada</h5>
<br>
<h6>En esta pagina se lista las ventas realizadas en todas las sucursales. Se puede filtrar por nombre de Cliente o Sucursal</h6>

<br>

";


// <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
// <i class="fa fa-question-circle fa-2x"></i> 
// </button>
?>