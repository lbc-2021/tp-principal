<?php

include('conexion.php');
include('usuario.php');
include('inicio.php');
include('manual.php');
$texto = $manualReporteComision;
$aviso = '';

$ano = $_GET['ano'];
$mes = $_GET['mes'];
$porcentaje = $_GET['porcentaje'];

$desde='01-'.$mes."-".$ano;
$desde = $desde." 00:00";
$hasta='31-'.$mes."-".$ano;
$hasta = $hasta." 23:59";

$desde = date("Y/m/d H:i", strtotime($desde));
$hasta = date("Y/m/d H:i", strtotime($hasta));



$laquery = "SELECT distinct vendedor_venta from venta where fechahora_venta between '$desde' and '$hasta'";
$ejecutar = mysqli_query($con, $laquery);

$row_count = mysqli_num_rows($ejecutar);
if ($row_count == 0) {
    $aviso = "No hay resultados para mostrar, por favor pruebe con otro periodo";
}


?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Reporte de Comisiones</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal"
                data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <form method="get" action="" novalidate>
                        <span class="section">Selección de sucursal y de rango de fechas</span>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Mes<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <select name="mes" class="form-control" required="required">
                                    <option value="">Seleccionar Mes</option>
                                    <option value="01">Enero</option>
                                    <option value="02">Febrero</option>
                                    <option value="03">Marzo</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Mayo</option>
                                    <option value="06">Junio</option>
                                    <option value="07">Julio</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Septiembre</option>
                                    <option value="10">Octubre</option>
                                    <option value="11">Noviembre</option>
                                    <option value="12">Diciembre</option>
                                </select>
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Año<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <select name="ano" class="form-control" required="required">
                                    <option value="">Seleccionar Año</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2020">2022</option>                                    
                                </select>
                            </div>
                        </div>
                        <div class="field item form-group">
                            <label class="col-form-label col-md-3 col-sm-3  label-align">Porcentaje de Comisión<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6">
                                <input type="number" class="form-control" name="porcentaje" id="porcentaje" required="required" data-validate-minmax="0,100">
                            </div>
                        </div>
                        
                        <div class="field item form-group">
                            <div class="col-md-4 col-sm-4 label-align">
                                <button type='submit' class="btn btn-primary"  id="myBtn">Generar
                                    reporte</button>
                            </div>
                        </div>
<!--                        <div id="mjeError" style="position: auto; left: 20%;" class="col-md-12 col-sm-12"></div> -->
                    </form>
                </div>

                <?php if($ano != ''  && $mes !='') { ?>
                <div class="x_panel">
                    
                    <span class="section">Reporte de Comisiones del Periodo <?php echo $mes."/".$ano?> </span>
                    <div class="field item form-group">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>                                    
                                        <th class='text-center'>Vendedor</th>
                                        <th class='text-center'>Total de Ventas</th>
                                        <th class='text-center'>Monto Total</th>
                                        <th class='text-center'>% de comisión</th>
                                        <th class='text-center'>Comisión de Vendedor</th>                                        
                                    </tr>
                                </thead>
                                <tbody>
                                <?php  while ($row = mysqli_fetch_array($ejecutar)) {
                                    $monto = 0;
                                    $cont = 0;
                                    $id_venta = $row['id_venta'];
                                    $vendedor_venta = $row['vendedor_venta'];
                                    
                                    $laquery2 = "SELECT * from venta left join usuario on id_usuario = vendedor_venta where fechahora_venta between '$desde' and '$hasta' and vendedor_venta = $vendedor_venta";
                                    $ejecutar2 = mysqli_query($con, $laquery2);

                                    while ($row = mysqli_fetch_array($ejecutar2)) {
                                        $nombre_usuario = $row['descripcion_usuario'];
                                        $monto = $monto + $row['monto_venta'];
                                        $cont++;

                                    }


                                    
                                    ?>
                                    <tr>
                                    <td class='text-center'><?php echo $nombre_usuario ?></td>
                                    <td class='text-center'><?php echo $cont ?></td>
                                    <td class='text-center'><?php echo "$".$monto ?></td>
                                    <td class='text-center'><?php echo $porcentaje."%" ?></td>
                                    <td class='text-center'><?php echo "$".(($monto * $porcentaje)/100) ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                            <?php echo $aviso ?>
                        </div>
                    </div>
                </div>
               <?php } ?>
           

<?php if($ano != ""){ ?>
            <form action="reporteComisionPDF.php" method="get">

            <input type="text" hidden name="ano" value="<?php echo $ano; ?>">
            <input type="text" hidden name="mes" value="<?php echo $mes; ?>">
            <input type="text" hidden name="porcentaje" value="<?php echo $porcentaje; ?>">

            <button type="submit" style="float:right" class="btn btn-success">Descargar PDF del
            reporte</button>
            </form>
<?php }?>
</div>

        </div>
    </div>
</div>
<!-- /page content -->

<?php include("fin.php"); ?>

<script type="text/javascript">
window.onload = cambiarTitulo("Gestión de cuenta corriente");
</script>