<?php

include('conexion.php');
include('usuario.php');
include('inicio.php');
include('manual.php');
$texto = $manualReporteComision;


$laquery = "SELECT * from producto left join categoria on id_categoria = categoria_producto left join subcategoria on id_subcategoria = subcategoria_producto left join proveedor on proveedorPredeterminado_producto = id_proveedor where estado_producto = 1";
$ejecutar = mysqli_query($con, $laquery);


?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Reporte de Productos</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal"
                data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">

                    <span class="section">Reporte de Productos Activos </span>
                    <div class="field item form-group">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class='text-center'>#</th>
                                        <th class='text-center'>Nombre de Producto</th>
                                        <th class='text-center'>Categoria</th>
                                        <th class='text-center'>SubCategoria</th>
                                        <th class='text-center'>Proveedor Predeterminado</th>
                                        <th class='text-center'>Precio</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  while ($row = mysqli_fetch_array($ejecutar)) {

                                    $id_producto = $row['id_producto'];
                                    $nombre_producto = $row['nombre_producto'];
                                    $nombre_categoria = $row['nombre_categoria'];
                                    $nombre_subcategoria = $row['nombre_subcategoria'];
                                    $nombre_proveedor = $row['nombre_proveedor'];
                                    $precio_proveedor = $row['precio_producto'];                                
?>
                                    <tr>
                                        <td class='text-center'><?php echo $id_producto ?></td>
                                        <td class='text-center'><?php echo $nombre_producto ?></td>
                                        <td class='text-center'><?php echo $nombre_categoria ?></td>
                                        <td class='text-center'><?php echo $nombre_subcategoria ?></td>
                                        <td class='text-center'><?php echo $nombre_proveedor ?></td>
                                        <td class='text-center'><?php echo "$".$precio_proveedor ?></td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    
                    <a href="reporteDeProductosPDF.php" style="float:right" class="btn btn-success">Descargar PDF del
                        reporte</a>

                    
                </div>
            </div>
        </div>
    </div>
</div>
    <!-- /page content -->

    <?php include("fin.php"); ?>

    <script type="text/javascript">
    window.onload = cambiarTitulo("Reporte de Productos");
    </script>