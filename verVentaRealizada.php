<?php
$id = $_GET['id'];
include('conexion.php');
include('usuario.php');
include('manual.php');
$texto = $manualVerVentaRealizada;



//traigo el contenido del valor a editar consultando por ID y lo guardo en variables
$query = "SELECT * FROM venta  left join cliente on cliente_venta = id_cliente left join sucursal on sucursal_venta = id_sucursal WHERE id_venta = $id";
$resultado = mysqli_query($con, $query);

while ($fila = mysqli_fetch_array($resultado)) {
    $cliente_venta = $fila['cliente_venta'];
    $nombre_cliente = $fila['nombre_cliente'];
    $nombre_sucursal = $fila['nombre_sucursal'];
    $id_venta = $fila['id_venta'];
    $fechahora_venta = $fila['fechahora_venta'];
    $fechahora_venta = date("d/m/Y - H:i", strtotime($fechahora_venta));
    $estado_venta = $fila['estado_venta'];
    $monto_venta = $fila['monto_venta'];
    $estado_cliente = $fila['estado_cliente'];
    $nombre_sucursal = $fila['nombre_sucursal'];
    $tipo = 'Local';
    if ($estado_venta == 'Abonada - Virtual') $tipo = 'Virtual';
}

//echo round($monto_venta/$valor_dolar, 2);

$query2 = "SELECT * FROM detalle_venta left join producto on producto_detalle = id_producto WHERE venta_detalle = $id_venta";
$resultado2 = mysqli_query($con, $query2);

if ($cliente_venta == '0') {
    $nombre_cliente = "Cliente sin registrar";
}

include('inicio.php');
?>

<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Venta</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                   <div class="x_content">
                        <button class="btn btn-danger" style="float:right" onclick="goBack()">Regresar</button> <br>

                        <form method="post" action="funciones/verVenta_funcion.php" method="POST" novalidate>
                            <span class="section">Ver Datos de la Venta</span>

                            <input type="hidden" name="id" id="id" value="<?php echo $id_venta; ?>">

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Cliente<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" data-validate-length-range="3" data-validate-words="2" name="nombre" id="nombre" required="required" readonly value="<?php echo $nombre_cliente; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Fecha y Hora de Compra<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="fecha" required="required" readonly value="<?php echo $fechahora_venta; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Tipo de Venta<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="tipo" required="required" readonly value="<?php echo $tipo; ?>">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Sucursal <span class="required">*</span> $</label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="sucursal"  required="required" readonly value="<?php echo $nombre_sucursal; ?>" id="sucursal">
                                </div>
                            </div>

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Total Abonado <span class="required">*</span> $</label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" name="monto"  required="required" readonly value="<?php echo $monto_venta; ?>" id="total">
                                </div>
                            </div>
                            

                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Detalle<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <textarea class="form-control" readonly cols="30" rows="10">
<?php while ($fila = mysqli_fetch_array($resultado2)) {
    $nombre_producto = $fila['nombre_producto'];
    $cantidad_detalle = $fila['cantidad_detalle'];
    echo "- " . $nombre_producto . " x" . $cantidad_detalle . "\n";
} ?>
                                </textarea>
                                </div>
                            </div>
                            
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("fin.php"); ?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Ver venta");
</script>