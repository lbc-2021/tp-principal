<script>
    var datoschart = new Array(0);
    var datosLabel = new Array(0);
    var datosColor = new Array(0);
    
    <?php 
    foreach ($datosLabel as $e){ ?>
    datosLabel.push('<?php echo $e ?>');
    <?php } ?>

    <?php 
    $auxiliar = 0;
    foreach ($datosChart as $e){ 
        $auxiliar += 1;
    ?>
    datoschart.push(<?php echo $e ?>);

    <?php if($auxiliar % 2 == 0) { ?>
        datosColor.push('rgb(0, 80, 51,0.70)');
        console.log('datoscolor '+datosColor);
    <?php } else { ?>
        datosColor.push('rgb(0,76,153,0.7)');
        console.log('datoscolor '+datosColor);
        <?php } ?>
    <?php } ?>
    
    const ctx= document.getElementById("myChart").getContext("2d");
    ctx.save();
    ctx.fillStyle = 'lightGreen';
    var myChart = new Chart(ctx,{
        type:"pie",//"doughnut",
        data:{
            labels:datosLabel,
            datasets:[
                {
                    label:'Porcentaje por debajo del <?php echo $headerLabel ?>',
                    backgroundColor:datosColor,
                    data: datoschart,
                }
            ]
        },
        options: {
            responsive: false,
            plugins: {
                legend:{
                    display:true
                },
                title: {
                    display: false,
                    text: 'Items con mayor riesgo de Stock (por <?php echo $headerLabel ?>)'
                }
            },

        }
    })
</script>
