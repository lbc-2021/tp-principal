<?php //query para traer el contenido de la tabla Sector para ser usando en el ComboBox
include('conexion.php');
include('usuario.php');
include('manual.php');

$texto = $manualPintura;

$subCategoria = $_GET['subcategoria'];
//query para traer las sucursales para llenar el combobox
$queryClientes = "SELECT * from cliente where estado_cliente = '1'";
$ejecutarClientes = mysqli_query($con, $queryClientes);


//query para traer las sucursales para llenar el combobox
$queryProductos = "SELECT * from producto left join stock_sucursal on stock_sucursal.id_producto = producto.id_producto left join subcategoria on producto.subcategoria_producto = id_subcategoria  where estado_producto = '1' and id_sucursal = $sucursal_usuario_log and nombre_subcategoria = '$subCategoria' order by nombre_producto asc";
$ejecutarProductos = mysqli_query($con, $queryProductos);
$ejecutarProductos2 = mysqli_query($con, $queryProductos);
$ejecutarProductos3 = mysqli_query($con, $queryProductos);


$cliente = $_GET['cliente'];

if ($cliente != ''){

$querySelect = "SELECT * from cliente where id_cliente = $cliente";
$resultadoSelect = mysqli_query($con, $querySelect);

    while ($fila = mysqli_fetch_array($resultadoSelect)) {
        $nombre_cliente = $fila['nombre_cliente'];
        $dni_cliente = $fila['dni_cliente'];
    }
}

if($cliente == 0){
    $nombre_cliente = 'Cliente no registrado';
    $dni_cliente = 'No Aplica';
}

include('inicio.php');

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Nueva Venta de Pinturas (<?php echo $subCategoria ?>)</h3>
            </div>
            <button type="button" class="btn btn-link" style="float:right" data-toggle="modal" data-target="#exampleModal" title="Ayuda">
                <i class="fa fa-question-circle fa-2x"></i> 
            </button>
        </div>
        <div class="clearfix"></div>

        <?php if ($cliente == ''){ ?>

        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div class="x_content">
                        <form method="get" novalidate>
                            <span class="section">Buscar Cliente </span> 
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Buscar Cliente por Nombre / Dni<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                <input type="hidden" name="subcategoria" value="<?php echo $subCategoria?>">
                                <select name="cliente" class="select2_single form-control" data-placeholder="Elige un color" required="required">
                                        <option value="0">Venta a Cliente no registrado</option>
                                        <?php while($row = mysqli_fetch_array($ejecutarClientes)){ ?>
                                        <option value="<?php echo $row['id_cliente']; ?>">
                                            <?php echo $row['nombre_cliente']." (Dni: ".$row['dni_cliente'].")"  ?></option> <!-- PARCHECITO DE -1 -->
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>                            

                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Siguiente</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <?php if ($cliente != ''){ 
        $texto = $manualPinturaCliente;?>
        


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="x_panel">
                    <div id="main" class="x_content">

                        <form method="post" action="crearPedido.php" novalidate>
                        <input type="hidden" name="subcategoria" value="<?php echo $subCategoria?>">

                        <input type="hidden" name="cliente" id="cliente" value="<?php echo $cliente; ?>">

                            <h3>Cliente</h3> <hr>
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Nombre de Cliente<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control"
                                    name="descripcion" id="descripcion" readonly  value="<?php echo $nombre_cliente; ?>" >
                                </div>
                            </div>     
                            
                            <div class="field item form-group">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Dni de Cliente<span class="required">*</span></label>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" class="form-control" d
                                    name="descripcion" id="descripcion" readonly  value="<?php echo $dni_cliente; ?>" >
                                </div>
                            </div>   

                            <h3>Pedido</h3> <hr>
                            <label> Inidicar las pinturas correspondientes, se pueden mezclar hasta 3 colores.</label> <br>
                            <label><small><font style="color:red">Atención:</font> En caso de que se solicite una cantidad mayor al stock disponible en la sucursal, se otorgará la mayor cantidad posible. </small></label>

                            <div class="field item form-group" id="1">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Pintura 1<span class="required">*</span></label>
                                <div class="col-md-4 col-sm-4">
                                    <select id="producto1" name="producto1" class="form-control" required="required" style="width:100%">
                                        <option value="">Seleccione una pintura</option>
                                        <?php while($row = mysqli_fetch_array($ejecutarProductos)){
                                            $stk = $row['stock_sucursal'];
                                            $stk2 = $stk." Litros en stock";
                                            if($stk == 0){ ?>
                                        <option style="color: red !important;" value="<?php echo $row['id_producto']; ?>" disabled>
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (Sin Stock)" ?></option> <!-- PARCHECITO DE -1 -->
                                        <?php } else { ?>
                                            <option value="<?php echo $row['id_producto']; ?>">
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." x Ltr (".$stk2.")" ?></option> 
                                            <?php }
                                            }  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <label class="col-form-label col-md-1 col-sm-1  label-align">Cantidad de Litros<span class="required">*</span></label>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <input type="number" class="form-control" name="cantidad1" id="cantidad1" required="required" data-validate-minmax="0,999999999">
                                </div>
                            </div> 

                            <div class="field item form-group" id="1">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Pintura 2</label>
                                <div class="col-md-4 col-sm-4">
                                    <select id="producto2" name="producto2" class="form-control" style="width:100%">
                                        <option value="">Seleccione una pintura para mezclar (Opcional)</option>
                                        <?php while($row = mysqli_fetch_array($ejecutarProductos2)){
                                            $stk = $row['stock_sucursal'];
                                            $stk2 = $stk." Litros en stock";
                                            if($stk == 0){ ?>
                                        <option style="color: red !important;" value="<?php echo $row['id_producto']; ?>" disabled>
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (Sin Stock)" ?></option> <!-- PARCHECITO DE -1 -->
                                        <?php } else { ?>
                                            <option value="<?php echo $row['id_producto']; ?>">
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." x Ltr (".$stk2.")" ?></option> 
                                            <?php }
                                            }  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <label class="col-form-label col-md-1 col-sm-1  label-align">Cantidad de Litros</label>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <input type="number" class="form-control" name="cantidad2" id="cantidad2"  data-validate-minmax="1,999999999">
                                </div>
                            </div> 

                            <div class="field item form-group" id="1">
                                <label class="col-form-label col-md-3 col-sm-3  label-align">Pintura 3</label>
                                <div class="col-md-4 col-sm-4">
                                    <select id="producto3" name="producto3" class="form-control"  style="width:100%">
                                        <option value="">Seleccione una pintura para mezclar (Opcional)</option>
                                        <?php while($row = mysqli_fetch_array($ejecutarProductos3)){
                                            $stk = $row['stock_sucursal'];
                                            $stk2 = $stk." Litros en stock";
                                            if($stk == 0){ ?>
                                        <option style="color: red !important;" value="<?php echo $row['id_producto']; ?>" disabled>
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." (Sin Stock)" ?></option> <!-- PARCHECITO DE -1 -->
                                        <?php } else { ?>
                                            <option value="<?php echo $row['id_producto']; ?>">
                                            <?php echo $row['nombre_producto']." - $".$row['precio_producto']." x Ltr (".$stk2.")" ?></option> 
                                            <?php }
                                            }  ?>
                                    </select>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <label class="col-form-label col-md-1 col-sm-1  label-align">Cantidad de Litros</label>
                                </div>
                                <div class="col-md-1 col-sm-1">
                                    <input type="number" class="form-control" name="cantidad3" id="cantidad3" data-validate-minmax="1,999999999">
                                </div>
                            </div> 




                            <div class="form-group">
                                <div class="col-md-6 offset-md-3">
                                    <button type='submit' class="btn btn-primary">Procesar Pedido</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

                            
    <?php  } ?>
    </div>

</div>


<?php include("fin.php");?>

<script type="text/javascript">
    window.onload = cambiarTitulo("Pinturas");
</script>