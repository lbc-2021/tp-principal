<?php


include('../conexion.php');
include('../usuario.php');

$error = 0;
$id = $_POST['id'];

$select = "SELECT * from venta where id_venta = $id ";
$ejecSelect = mysqli_query($con, $select);
while ($row = mysqli_fetch_array($ejecSelect)) {
    $monto = $row['monto_venta'];
    $sucursal = $row['sucursal_venta'];
    $id_cliente = $row['cliente_venta'];
}

$medio = $_POST['medio_pago'];
$monto = $_POST['monto'];
$montoPagado = $_POST['monto_pagado'];
$banco = $_POST['banco'];
$compania = $_POST['compania'];
$numero = $_POST['numero'];
$transaccion = $_POST['transaccion'];
$fecha = $_POST['fecha'];
$vto = $_POST['vto'];

//agregado de moneas
$moneda= $_POST['moneda'];
$dolar= $_POST['dolar'];
$euro= $_POST['euro'];

//paso la moneda a peso para ver si se esta pagando bien
$montoAux = $montoPagado;
if ($moneda == 'dolar' || $moneda == "euro") {
    $montoAux = $_POST['montomoneda'];
}

//consulto si hay 2do medio de pago
$montoPagado2 = $_POST['monto_pagado2'];
$moneda2= $_POST['moneda2'];
$montoAux2 = $montoPagado2;
//paso la moneda a peso para ver si se esta pagando bien
if($montoPagado2 != '') {
    if ($moneda2 == 'dolar' || $moneda2 == "euro") {
        $montoAux2 = $_POST['montomoneda2'];
    }
}


$medio2 = $_POST['medio_pago2'];

if($montoPagado2 == '') $montoPagado2 = 0 ;
$totalpago = $montoPagado + $montoPagado2;


// verifico que abone el total y justo---------------------
if((floatval($montoAux) + floatval($montoAux2)) > $monto) {
    $message = "Monto abonado invalido: Se esta abonando de mas";
    $class = "alert alert-danger";
    $error = 1;
}

if((floatval($montoAux) + floatval($montoAux2)) < $monto) {
    $message = "Monto abonado invalido: No se esta abonando el total";
    $class = "alert alert-danger";
    $error = 1;
}




// si es cliente, me guardo la deuda por si las dudas
if ($id_cliente != 0){
    $queryDeuda = "SELECT * from cliente where id_cliente = $id_cliente";
    $ejecutarQueryDeuda = mysqli_query($con, $queryDeuda);
    while ($row = mysqli_fetch_array($ejecutarQueryDeuda)) {
        $deudaActual = $row['deuda_cliente'];
        $limite = $row['limite_cliente'];
    }
}

// si paga con cta corriente, me fijo si le da el monto
if($medio == 'ctacorriente'){
    if((floatval($montoAux) + floatval($deudaActual)) > $limite){
        $message = "Credito Insuficiente";
        $class = "alert alert-danger";
        $error = 1;
    }
}

if($medio2 == 'ctacorriente'){
    if((floatval($montoAux2) + floatval($deudaActual)) > $limite){
        $message = "Credito Insuficiente";
        $class = "alert alert-danger";
        $error = 1;
    }
}


//si no hay ningun error, sigo    
if($error == 0){

switch ($medio) {
    case "efectivo":
        if ($moneda == 'peso') $columna = "montoEfectivo_sucursal";
        if ($moneda == 'dolar') $columna = "montoEfectivoDolar_sucursal";
        if ($moneda == 'euro') $columna = "montoEfectivoEuro_sucursal";
        break;

  /*  case "ctacorriente":
        if ($moneda == 'peso') $columna = ;
        if ($moneda == 'dolar') $columna = ;
        if ($moneda == 'euro') $columna = ;
        break;*/

    case "credito":
        if ($moneda == 'peso') $columna = "montoCredito_sucursal";
        if ($moneda == 'dolar') $columna = "montoCreditoDolar_sucursal";
        if ($moneda == 'euro') $columna = "montoCreditoEuro_sucursal";
        break;

    case "cheque":
        if ($moneda == 'peso') $columna = "montoCheques_sucursal";
        if ($moneda == 'dolar') $columna = "montoChequesDolar_sucursal" ;
        if ($moneda == 'euro') $columna = "montoChequesEuro_sucursal" ;
        break;
}

//me fijo el medio de pago, inserto el pago y actualizo el monto de la sucursal
if ($medio == 'efectivo' ){

    $insertarPago = "INSERT INTO pago_efectivo(monto_efectivo, venta_efectivo, moneda_efectivo) VALUES ('$montoPagado', '$id', '$moneda')";
    $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

    $updateSucursal =  "UPDATE sucursal SET $columna = $columna + $montoPagado WHERE id_sucursal = '$sucursal'";
    $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);


    $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
        while ($fila = mysqli_fetch_array($querysucursal)) {
            $montoActual = $fila["$columna"];
        }
        $monto_final = $montoActual + $montoPagado;     //impacta en la caja el monto pagado, no el monto total.
        $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja,moneda_acaja, medio_acaja)
        VALUES ('Se abonó la venta número $id','$montoPagado','$montoActual','$sucursal', '$id_usuario_log', '$monto_final', '$moneda', '$medio')";
        $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);
       


}

if ($medio == 'ctacorriente' ){

    $insertarPago = "INSERT INTO pago_credito(monto_credito, venta_credito) VALUES ('$montoPagado', '$id')";
    $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

    $updateDeuda = "UPDATE cliente SET deuda_cliente= deuda_cliente + $montoPagado WHERE id_cliente = $id_cliente";
    $ejecutarUpdateDeuda = mysqli_query($con, $updateDeuda);

    $nuevaDeuda = $deudaActual + $montoPagado;

    $insertAuditoria = "INSERT INTO auditoria_cc_clientes (cliente_accc, descripcion_accc, monto_accc, montoPrevio_accc, montoActual_accc) VALUES ('$id_cliente', 'Se abonó la venta número $id', '$montoPagado', '$deudaActual', '$nuevaDeuda')";
    $ejecutarInsertAuditoria = mysqli_query($con, $insertAuditoria);
}

if ($medio == 'credito' ){
    $insertarPago = "INSERT INTO pago_tarjeta(monto_tarjeta, banco_tarjeta, compania_tarjeta, nro_tarjeta, transaccion_tarjeta, vto_tarjeta, venta_tarjeta, moneda_tarjeta) VALUES ('$montoPagado', '$banco', '$compania', '$numero', '$transaccion', '$vto',  '$id', '$moneda')";
    $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

    $updateSucursal =  "UPDATE sucursal SET $columna = $columna + $montoPagado WHERE id_sucursal = '$sucursal'";
    $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);

    $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
    while ($fila = mysqli_fetch_array($querysucursal)) {
        $montoActual = $fila["$columna"];
    }
    $monto_final = $montoActual + $montoPagado;     //impacta en la caja el monto pagado, no el monto total.
    $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja,moneda_acaja, medio_acaja)
    VALUES ('Se abonó la venta número $id','$montoPagado','$montoActual','$sucursal', '$id_usuario_log', '$monto_final', '$moneda', '$medio')";
    $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);

}

if ($medio == 'cheque' ){
    $insertarPago = "INSERT INTO pago_cheque(banco_cheque, nro_cheque, fecha_cheque, monto_cheque, venta_cheque, moneda_cheque) VALUES ('$banco', '$numero', '$fecha', '$montoPagado', '$id','$moneda')";
    $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

    $updateSucursal =  "UPDATE sucursal SET $columna = $columna + $montoPagado WHERE id_sucursal = '$sucursal'";
    $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);

    $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
    while ($fila = mysqli_fetch_array($querysucursal)) {
        $montoActual = $fila["$columna"];
    }
    $monto_final = $montoActual + $montoPagado;     //impacta en la caja el monto pagado, no el monto total.
    $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja,moneda_acaja, medio_acaja)
    VALUES ('Se abonó la venta número $id','$montoPagado','$montoActual','$sucursal', '$id_usuario_log', '$monto_final', '$moneda', '$medio')";
    $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);

}
//me fijo como esta el medio de pago 2
if ($medio2 != '') {
    $montoPagado2 = $_POST['monto_pagado2'];
    $banco2 = $_POST['banco2'];
    $compania2 = $_POST['compania2'];
    $numero2 = $_POST['numero2'];
    $transaccion2 = $_POST['transaccion2'];
    $fecha2 = $_POST['fecha2'];
    $vto2 = $_POST['vto2'];

    switch ($medio2) {
        case "efectivo":
            if ($moneda2 == 'peso') $columna = "montoEfectivo_sucursal";
            if ($moneda2 == 'dolar') $columna = "montoEfectivoDolar_sucursal";
            if ($moneda2 == 'euro') $columna = "montoEfectivoEuro_sucursa";
            break;
    
      /*  case "ctacorriente":
            if ($moneda == 'peso') $columna = ;
            if ($moneda == 'dolar') $columna = ;
            if ($moneda == 'euro') $columna = ;
            break;*/
    
        case "credito":
            if ($moneda2 == 'peso') $columna = "montoCredito_sucursal";
            if ($moneda2 == 'dolar') $columna = "montoCreditoDolar_sucursal";
            if ($moneda2 == 'euro') $columna = "montoCreditoEuro_sucursal";
            break;
    
        case "cheque":
            if ($moneda2 == 'peso') $columna = "montoCheques_sucursal";
            if ($moneda2 == 'dolar') $columna = "montoChequesDolar_sucursal" ;
            if ($moneda2 == 'euro') $columna = "montoChequesEuro_sucursal" ;
            break;
    }

    //me fijo el medio de pago 2, inserto el pago y actualizo el monto de la sucursal

        if ($medio2 == 'efectivo' ){

            $insertarPago = "INSERT INTO pago_efectivo(monto_efectivo, venta_efectivo, moneda_efectivo) VALUES ('$montoPagado2', '$id', '$moneda2')";
            $ejecutarInsertarPago = mysqli_query($con, $insertarPago);
        
            $updateSucursal =  "UPDATE sucursal SET $columna = $columna + $montoPagado2 WHERE id_sucursal = '$sucursal'";
            $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);


                    
            $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
            while ($fila = mysqli_fetch_array($querysucursal)) {
                $montoActual = $fila['montoEfectivo_sucursal'];
            }
            $monto_final = $montoActual + $montoPagado2;     //impacta en la caja el monto pagado, no el monto total.
            $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja,moneda_acaja, medio_acaja)
            VALUES ('Se abonó la venta número $id','$montoPagado2','$montoActual','$sucursal', '$id_usuario_log', '$monto_final', '$moneda2', '$medio2')";
            $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);


        
        }

        
        if ($medio2 == 'ctacorriente' ){

            $insertarPago = "INSERT INTO pago_credito(monto_credito, venta_credito) VALUES ('$montoPagado2', '$id')";
            $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

            $updateDeuda = "UPDATE cliente SET deuda_cliente= deuda_cliente + $montoPagado2 WHERE id_cliente = $id_cliente";
            $ejecutarUpdateDeuda = mysqli_query($con, $updateDeuda);

            $nuevaDeuda = $deudaActual + $montoPagado2;

            $insertAuditoria = "INSERT INTO auditoria_cc_clientes (cliente_accc, descripcion_accc, monto_accc, montoPrevio_accc, montoActual_accc) VALUES ('$id_cliente', 'Se abonó la venta número $id', '$montoPagado2', '$deudaActual', '$nuevaDeuda')";
            $ejecutarInsertAuditoria = mysqli_query($con, $insertAuditoria);


        }
        
        if ($medio2 == 'credito' ){
            $insertarPago = "INSERT INTO pago_tarjeta(monto_tarjeta, banco_tarjeta, compania_tarjeta, nro_tarjeta, transaccion_tarjeta, vto_tarjeta, venta_tarjeta, moneda_tarjeta) VALUES ('$montoPagado2', '$banco2', '$compania2', '$numero2', '$transaccion2', '$vto2',  '$id', '$moneda2')";
            $ejecutarInsertarPago = mysqli_query($con, $insertarPago);
        
            $updateSucursal =  "UPDATE sucursal SET $columna = $columna + $montoPagado2 WHERE id_sucursal = '$sucursal'";
            $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
        
            $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
            while ($fila = mysqli_fetch_array($querysucursal)) {
                $montoActual = $fila["$columna"];
            }
            $monto_final = $montoActual + $montoPagado;     //impacta en la caja el monto pagado, no el monto total.
            $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja,moneda_acaja, medio_acaja)
            VALUES ('Se abonó la venta número $id','$montoPagado','$montoActual','$sucursal', '$id_usuario_log', '$monto_final', '$moneda2', '$medio2')";
            $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);

        }
        
        if ($medio2 == 'cheque' ){
            $insertarPago = "INSERT INTO pago_cheque(banco_cheque, nro_cheque, fecha_cheque, monto_cheque, venta_cheque, moneda_cheque) VALUES ('$banco2', '$numero2', '$fecha2', '$montoPagado2', '$id', '$moneda2')";
            $ejecutarInsertarPago = mysqli_query($con, $insertarPago);
        
            $updateSucursal =  "UPDATE sucursal SET $columna = $columna + $montoPagado2 WHERE id_sucursal = '$sucursal'";
            $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
        
            $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
            while ($fila = mysqli_fetch_array($querysucursal)) {
                $montoActual = $fila["$columna"];
            }
            $monto_final = $montoActual + $montoPagado;     //impacta en la caja el monto pagado, no el monto total.
            $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja,moneda_acaja, medio_acaja)
            VALUES ('Se abonó la venta número $id','$montoPagado','$montoActual','$sucursal', '$id_usuario_log', '$monto_final', '$moneda2', '$medio2')";
            $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);
        }
    }


    //actualizo la venta a abonada 
    $query = "UPDATE venta SET estado_venta='Abonado', cobrador_venta ='$id_usuario_log' WHERE id_venta = $id";
    $ejecutarQuery = mysqli_query($con, $query);


    if($ejecutarQuery){
        $message = "Venta Abonada con exito, redirigiendo a la pagina anterior.";
        $class = "alert alert-success";
    } else {
        $message = "Algo salió mal.";
        $class = "alert alert-danger";
    }
}




/*

if ($medio == 'efectivo' || $medio == 'cheque') {
    $deudaASumar = $monto - $montoPagado;
    $nuevaDeuda = $deudaActual + $deudaASumar;
} else {
    $nuevaDeuda = $deudaActual + $monto;
}

if ($nuevaDeuda <= $limite) {
    $updateDeuda = "UPDATE cliente SET deuda_cliente=$nuevaDeuda WHERE id_cliente = $id_cliente";
    $ejecutarUpdateDeuda = mysqli_query($con, $updateDeuda);

    $query = "UPDATE venta SET estado_venta='Abonado' WHERE id_venta = $id";
    $ejecutarQuery = mysqli_query($con, $query);

    if ($medio == 'efectivo') $columnaMedio = 'montoEfectivo_sucursal';
    if ($medio == 'credito') $columnaMedio = 'montoCredito_sucursal';
    if ($medio == 'cheque') $columnaMedio = 'montoCheques_sucursal';

    if ($medio == 'efectivo')   //Si se ejecutaron todas las querys, dejo registro en la auditoria_caja
    {
        $querysucursal = mysqli_query($con, "SELECT * FROM sucursal WHERE id_sucursal = $sucursal");
        while ($fila = mysqli_fetch_array($querysucursal)) {
            $montoActual = $fila['montoEfectivo_sucursal'];
        }
        $monto_final = $montoActual + $montoPagado;     //impacta en la caja el monto pagado, no el monto total.
        $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja)
        VALUES ('Se abonó la venta número $id','$montoPagado','$montoActual','$sucursal', '$id_usuario_log', '$monto_final')";
        $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);
    }

    $queryUpdate = "UPDATE sucursal SET $columnaMedio = $columnaMedio + $montoPagado WHERE id_sucursal = $sucursal";
    $ejecutarQueryUpd = mysqli_query($con, $queryUpdate);

    if ($deudaASumar > 0) {
        $insertAuditoria = "INSERT INTO auditoria_cc_clientes (cliente_accc, descripcion_accc, monto_accc, montoPrevio_accc, montoActual_accc) VALUES ('$id_cliente', 'Pago parcial de una compra', '$deudaASumar', '$deudaActual', '$nuevaDeuda')";
        $ejecutarInsertAuditoria = mysqli_query($con, $insertAuditoria);
    }

    if ($ejecutarUpdateDeuda && $ejecutarQuery && $ejecutarQueryUpd) {
        $message = "Venta Abonada con exito, redirigiendo a la pagina anterior.";
        $class = "alert alert-success";
    } else {
        $message = "Algo salió mal.";
        $class = "alert alert-danger";
    }
} else {
    $message = "La deuda supera el límite, redirigiendo a la pagina anterior.";
    $class = "alert alert-danger";
}*/
$destino = "ventasACobrar.php";
//echo $message;
header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
