<?php

include('../conexion.php');
include('../usuario.php');

if ($perfil_usuario_log != 'Administrador') {
    $message = "No posee permisos para realizar la acción";
    $class = "alert alert-danger";
    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=index.php");
} else {

    if (isset($_POST) && !empty($_POST)) {
        $checkusuario = "SELECT * FROM usuario WHERE nombre_usuario = '$_POST[usuario]' AND id_usuario <> '$_POST[id]'";
        $resultadocheck = mysqli_query($con, $checkusuario);
        $row_count = mysqli_num_rows($resultadocheck);
        if ($row_count != 0) {
            $message = "El usuario ya existe";
            $class = "alert alert-danger";
        } else {
            $id_usuario = $_POST['id'];
            $nombre_usuario = $_POST['usuario'];
            $descripcion_usuario = $_POST['descripcion'];
            $id_sucursal = $_POST['sucursal'];
            $nombre_sucursal = $_POST['nombre_sucursal'];
            $id_perfil = $_POST['perfil'];
            $estado_usuario = $_POST['estado'];

            

            $mail = $_POST['mail'];

            $update = "UPDATE usuario SET nombre_usuario = '$nombre_usuario', descripcion_usuario = '$descripcion_usuario', sucursal_usuario = '$id_sucursal', estado_usuario = '$estado_usuario' , perfil_usuario = '$id_perfil' , mail_usuario = '$mail' WHERE id_usuario = '$id_usuario' ";

            $ejecturarUpdate = mysqli_query($con, $update);

            if ($ejecturarUpdate) {
                $message = "Usuario Actualizado con Exito. Redirigiendo a la pagina anterior";
                $class = "alert alert-success";
                //actualizo los datos de sesión
                $sucu_query   =  mysqli_query($con, "SELECT nombre_sucursal FROM sucursal where id_sucursal = '$id_sucursal'");
                if($id_usuario_log == $id_usuario){
                    $_SESSION['descripcion_usuario'] = $descripcion_usuario;
                    $_SESSION['nombre_usuario'] = $nombre_usuario;
                    $_SESSION['estado_usuario'] = $estado_usuario;
                    $_SESSION["id_usuario"]= $id_usuario;
                    $_SESSION["sucursal_usuario"]= $id_sucursal;
                    $_SESSION["perfil_usuario"]= $id_perfil;
                    while($row = mysqli_fetch_array($sucu_query)){                  
                        $_SESSION["nombre_sucursal"]= $row["nombre_sucursal"];
                    }
                }
            } else {
                $message = "No se pudieron insertar los datos";
                $class = "alert alert-danger";
            }
        }
        $destino = "abmUsuarios.php";
        header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=abmUsuarios.php");
    }
}
