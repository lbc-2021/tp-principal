<?php

include('../conexion.php');
include('../usuario.php');

$id_suc = $_POST['id'];
$accion = $_POST['accion'];
$monto = $_POST['monto'];
$descripcion = $_POST['descripcion'];

if ($monto != 0) {
    $querySucursal = mysqli_query($con, "SELECT montoEfectivo_sucursal FROM sucursal WHERE id_sucursal = $id_suc");
    while ($row = mysqli_fetch_array($querySucursal)) {
        $monto_sucursal = $row['montoEfectivo_sucursal'];
    }
    if ($accion == "agregar") {
        $montoFinal = $monto + $monto_sucursal;
    } else {
        $montoFinal = $monto_sucursal - $monto;
        $monto *= -1; //lo dejo en negativo para que aparezca en la auditoria como una resta.
    }
    if ($montoFinal >= 0) {
        $update = "UPDATE sucursal SET montoEfectivo_sucursal = '$montoFinal' WHERE id_sucursal = '$id_suc' ";     //se lo agrego a la sucursal 
        $ejecturarUpdate = mysqli_query($con, $update);

        $update2 = "INSERT INTO auditoria_caja(`descripcion_acaja`, `monto_acaja`, `montoAnterior_acaja`, `montoPosterior_acaja`, `sucursal_acaja`, `usuario_acaja`) VALUES ('$descripcion','$monto','$monto_sucursal', '$montoFinal', '$id_suc','$id_usuario_log')";

        $ejecturarUpdate2 = mysqli_query($con, $update2);
        if ($ejecturarUpdate) {
            $message = "Operación realizada con Exito. Redirigiendo a la pagina anterior.";
            $class = "alert alert-success";
        } else {
            $message = "Error: No se pudieron insertar los datos";
            $class = "alert alert-danger";
        }
    } else {
        $message = "No hay suficiente dinero en la caja para realizar la operación. Redirigiendo a la pagina anterior.";
        $class = "alert alert-danger";
    }
    $destino = "verSucursal.php?id=$id_suc&success=true";
    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
} else header("refresh:0; ../verSucursal.php?id=$id_suc&success=false");
