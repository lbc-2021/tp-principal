<?php
include("../conexion.php");
include("../usuario.php");
$parametro = $_GET['parametro'];

if (isset($_POST) && !empty($_POST)) {
    $nombre = $_POST['nombre'];
    $estado = 1;
    $apoderado = $_POST['apoderado'];
    $dni = $_POST['dni'];
    $calle = $_POST['calle'];
    $altura = $_POST['altura'];
    $piso = $_POST['piso'];
    $depto = $_POST['depto'];
    $cp = $_POST['cp'];
    $localidad = $_POST['localidad'];
    $provincia = $_POST['provincia'];
    $cuil = $_POST['cuil'];
    $mail = $_POST['mail'];
    $telefono = $_POST['telefono'];
    $limite = $_POST['limite'];
    $precio = $_POST['precio'];
    $proveedor = $_POST['proveedor'];
    $stockminimo = $_POST['stockminimo'];
    $cantidadasolicitar = $_POST['cantidadasolicitar'];
    $cantidadLote = $_POST['cantidadLote'];
    $categoria = $_POST['categoria'];
    $stockseguridad = $_POST['stockseguridad'];
    $precio = round($precio, 2);
    $condicionFiscal = $_POST['condicionFiscal'];
    $tipo = $_POST['tipo'];
    $categoria2 = $_POST['categoria2'];
    $subcategoria = $_POST['subcategoria'];
    $iva = $_POST['iva'];
    $unidad = $_POST['unidaddecompra'];
    
    if($stockseguridad == '' ) $stockseguridad = 0;
    if($stockminimo == '' ) $stockminimo = 0;

    if($categoria == 2 ) $cantidadLote = $unidad;

    $checkparam = "SELECT * FROM $parametro WHERE nombre_$parametro = '$_POST[nombre]' ";
    $resultadocheck = mysqli_query($con, $checkparam);
    $row_count = mysqli_num_rows($resultadocheck);
    if ($row_count != 0) {
        $message = "El parametro ingresado ya existe";
        $class = "alert alert-danger";
    } else {

        if ($parametro == 'categoria') {
            $insertar = "INSERT INTO $parametro (nombre_$parametro, estado_$parametro ) VALUES ('$nombre', '$estado')";
        }
        if ($parametro == 'subcategoria') {
            $insertar = "INSERT INTO $parametro (nombre_$parametro, estado_$parametro, categoria_$parametro ) VALUES ('$nombre', '$estado', $categoria2)";
        }
        if ($parametro == 'producto') {
            $insertar = "INSERT INTO $parametro (nombre_$parametro, estado_$parametro, precio_$parametro, proveedorPredeterminado_$parametro, stockMinimo_$parametro, cantidadASolicitar_$parametro, categoria_$parametro, cantidadLote_$parametro, stockSeguridad_$parametro, subcategoria_$parametro) VALUES ('$nombre', '$estado', '$precio', '$proveedor', '$stockminimo', '$cantidadasolicitar', '$categoria', '$cantidadLote', '$stockseguridad', '$subcategoria')";
        }
        if ($parametro == 'sucursal' || $parametro == 'proveedor' || $parametro == 'cliente') {

            $checkparam = "SELECT * FROM domicilio WHERE calle_domicilio = '$calle' AND altura_domicilio = '$altura' AND piso_domicilio = '$piso' AND departamento_domicilio = '$depto' AND cp_domicilio = '$cp' AND localidad_domicilio = '$localidad' AND provincia_domicilio = '$provincia'";
            
            $resultadocheck = mysqli_query($con, $checkparam);
            $row_count = mysqli_num_rows($resultadocheck);
            if ($row_count != 0) {
                while ($row = mysqli_fetch_array($resultadocheck)) {
                    $id_domicilio = $row['id_domicilio'];
                }
            } else {

                $insertar_domicilio = "INSERT INTO domicilio (calle_domicilio, altura_domicilio, piso_domicilio, departamento_domicilio, cp_domicilio, localidad_domicilio, provincia_domicilio) VALUES ('$calle', '$altura', '$piso', '$depto', '$cp', '$localidad', '$provincia')";
                
                $ejecutar = mysqli_query($con, $insertar_domicilio);
                $idQuery = "SELECT MAX(id_domicilio) AS id FROM domicilio";
                $resultado = mysqli_query($con, $idQuery);
                while ($row = mysqli_fetch_array($resultado)) {
                    $id_domicilio = $row['id'];
                }
            }
            if ($parametro == 'sucursal') {
                $insertar = "INSERT INTO $parametro (nombre_$parametro, domicilio_$parametro, estado_$parametro ) VALUES ('$nombre', '$id_domicilio', '$estado')";
            }
            if ($parametro == 'proveedor') {
                $insertar = "INSERT INTO $parametro (nombre_$parametro, estado_$parametro, apoderado_$parametro, dni_$parametro, domicilio_$parametro, cuil_$parametro, mail_$parametro, telefono_$parametro, deuda_$parametro, limite_$parametro) VALUES ('$nombre', '$estado', '$apoderado', '$dni', '$id_domicilio', '$cuil', '$mail', '$telefono', 0, $limite)"; //lo creo siempre como activo
            }
            if ($parametro == 'cliente') {
                $insertar = "INSERT INTO $parametro (nombre_$parametro, dni_$parametro, cuil_$parametro, telefono_$parametro, mail_$parametro, domicilio_$parametro, condicionFiscal_$parametro, tipo_$parametro, estado_$parametro, deuda_$parametro, limite_$parametro, responsableIva_cliente) VALUES ('$nombre', '$dni', '$cuil', '$telefono', '$mail', '$id_domicilio', '$condicionFiscal', '$tipo', 1, 0, $limite, '$iva')";  //lo creo siempre como activo
            }
        }

        $ejecutar = mysqli_query($con, $insertar);

        if ($ejecutar) {
            $message = "Parametro cargado con Exito. Redirigiendo a la pagina anterior";
            $class = "alert alert-success";

            if($parametro == 'cliente')
            {
                $queryID = "SELECT * FROM cliente";
                $resultado = mysqli_query($con, $queryID);
                while ($row = mysqli_fetch_array($resultado)) 
                {
                    $id_cliente = $row['id_cliente'];                   //getteo el id del ultimo cliente agregado
                }


                $queryAuditoria = "INSERT INTO auditoria_cliente (cliente_ac, comentario_ac, responsable_ac) VALUES ('$id_cliente', 'Se creó el cliente', $id_usuario_log)"; //dejo registro en la auditoria
                $resultado = mysqli_query($con, $queryAuditoria);


            }

            if ($parametro == 'producto') {
                $query = "SELECT * FROM sucursal ORDER BY nombre_sucursal ASC";
                $resultado = mysqli_query($con, $query);

                while ($row = mysqli_fetch_array($resultado)) {
                    $id_sucursal = $row['id_sucursal'];
                    $querySuc = "INSERT INTO stock_sucursal(id_producto, id_sucursal, stock_sucursal) VALUES ((SELECT MAX(id_producto) FROM producto), $id_sucursal, 0)";
                    $resultadoSuc = mysqli_query($con, $querySuc);
                }
            }

            if ($parametro == 'sucursal') {
                $query = "SELECT * FROM producto ORDER BY nombre_producto ASC";
                $resultado = mysqli_query($con, $query);

                while ($row = mysqli_fetch_array($resultado)) {
                    $id_producto = $row['id_producto'];
                    $queryProd = "INSERT INTO stock_sucursal(id_sucursal, id_producto, stock_sucursal) VALUES ((SELECT MAX(id_sucursal) FROM sucursal), $id_producto, 0)";
                    $resultadoProd = mysqli_query($con, $queryProd);
                }
            }
        } else {
            $message = "No se pudieron insertar los datos";
            $class = "alert alert-danger";
        }
    }
   // echo $insertar;
    $destino = "abmParametros.php?parametro=$parametro";
    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
}
