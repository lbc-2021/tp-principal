<?php
include("../conexion.php");
include("../usuario.php");

if (isset($_POST) && !empty($_POST)) {
    $id = $_POST['id'];
    $parametro = $_POST['parametro'];
    $medioPago = $_POST['medio_pago'];
    $monto = $_POST['monto'];
    $nuevoLimite = $_POST['nuevoLimite'];
    $accion = $_POST['accion'];

    $querySelect = "SELECT * from $parametro where id_$parametro = $id";
    $resultadoSelect = mysqli_query($con, $querySelect);
    while ($fila = mysqli_fetch_array($resultadoSelect)) {
        $deuda = $fila['deuda_' . $parametro];
        $limite = $fila['limite_' . $parametro];
    }

    if ($accion == "pagar") {
        $querySuc = "SELECT * from sucursal where id_sucursal = $sucursal_usuario_log";
        $resultadoSuc = mysqli_query($con, $querySuc);
        while ($fila = mysqli_fetch_array($resultadoSuc)) {
            $montoEfectivo = $fila['montoEfectivo_sucursal'];
            $montoCheques = $fila['montoCheques_sucursal'];
            $montoCredito = $fila['montoCredito_sucursal'];
        }
        if ($parametro == 'cliente') {
            if ($medioPago == 'efectivo') {
                $nuevoMontoCaja = $montoEfectivo + $monto;
                $updateCaja = "UPDATE sucursal SET montoEfectivo_sucursal= '$nuevoMontoCaja' WHERE id_sucursal = $sucursal_usuario_log";
            } else if ($medioPago == 'cheque') {
                $nuevoMontoCaja = $montoCheques + $monto;
                $updateCaja = "UPDATE sucursal SET montoCheques_sucursal= '$nuevoMontoCaja' WHERE id_sucursal = $sucursal_usuario_log";
            } else {
                $nuevoMontoCaja = $montoCredito + $monto;
                $updateCaja = "UPDATE sucursal SET montoCredito_sucursal= '$nuevoMontoCaja' WHERE id_sucursal = $sucursal_usuario_log";
            }
        } else {
            if ($medioPago == 'efectivo') {
                $nuevoMontoCaja = $montoEfectivo - $monto;
                $updateCaja = "UPDATE sucursal SET montoEfectivo_sucursal= '$nuevoMontoCaja' WHERE id_sucursal = $sucursal_usuario_log";
            } else if ($medioPago == 'cheque') {
                $nuevoMontoCaja = $montoCheques - $monto;
                $updateCaja = "UPDATE sucursal SET montoCheques_sucursal= '$nuevoMontoCaja' WHERE id_sucursal = $sucursal_usuario_log";
            } else {
                $nuevoMontoCaja = $montoCredito - $monto;
                $updateCaja = "UPDATE sucursal SET montoCredito_sucursal= '$nuevoMontoCaja' WHERE id_sucursal = $sucursal_usuario_log";
            }
        }
        if ($nuevoMontoCaja >= 0) {
            $ejecutarUpdateCaja = mysqli_query($con, $updateCaja);
            $result = $deuda - $monto;
            $update = "UPDATE $parametro SET deuda_$parametro= '$result' WHERE id_$parametro = $id";
            $ejecutarUpdate = mysqli_query($con, $update);

            if ($parametro == 'cliente' && $result <= 0) { 
                $update = "UPDATE $parametro SET estado_cliente = 1 WHERE id_$parametro = $id";
                $ejecutarUpdate = mysqli_query($con, $update);
            }


            if ($parametro == 'cliente')
                $insertAuditoria = "INSERT INTO auditoria_cc_clientes (cliente_accc, descripcion_accc, monto_accc, montoPrevio_accc, montoActual_accc) VALUES ('$id', 'Pago de deuda', '$monto', '$deuda', '$result')";
            else
                $insertAuditoria = "INSERT INTO auditoria_cc_proveedores (proveedor_accp, descripcion_accp, monto_accp, montoPrevio_accp, montoActual_accp) VALUES ('$id', 'Pago de deuda', '$monto', '$deuda', '$result')";
            $ejecutarInsertAuditoria = mysqli_query($con, $insertAuditoria);

            if ($ejecutarUpdateCaja && $ejecutarUpdate && $ejecutarInsertAuditoria) {
                $message = "Pago registrado con éxito. Redirigiendo a la página anterior";
                $class = "alert alert-success";
            } else {
                $message = "No se pudo registrar el pago.";
                $class = "alert alert-danger";
            }
        } else {
            $message = "No hay dinero suficiente en esta sucursal para realizar el pago.";
            $class = "alert alert-danger";
        }
    } else {
        $update = "UPDATE $parametro SET limite_$parametro= '$nuevoLimite' WHERE id_$parametro = $id";
        $ejecutarUpdate = mysqli_query($con, $update);

        if ($parametro == 'cliente')
            $insertAuditoria = "INSERT INTO auditoria_cc_clientes (cliente_accc, descripcion_accc, monto_accc, montoPrevio_accc, montoActual_accc) VALUES ('$id', 'Cambio de límite', '0', '$limite', '$nuevoLimite')";
        else
            $insertAuditoria = "INSERT INTO auditoria_cc_proveedores (proveedor_accp, descripcion_accp, monto_accp, montoPrevio_accp, montoActual_accp) VALUES ('$id', 'Cambio de límite', '0', '$limite', '$nuevoLimite')";
        $ejecutarInsertAuditoria = mysqli_query($con, $insertAuditoria);

        if ($ejecutarUpdate && $ejecutarInsertAuditoria) {
            $message = "Límite actualizado con éxito. Redirigiendo a la página anterior";
            $class = "alert alert-success";
        } else {
            $message = "No se pudo actualizar el límite.";
            $class = "alert alert-danger";
        }
    }

    if($parametro == 'cliente') $parametro = 'clientes';
    $destino = "listaCuentasCorrientes.php?parametro=$parametro";
    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
}
