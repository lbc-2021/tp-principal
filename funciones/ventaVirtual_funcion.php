<?php
include("../conexion.php");
include("../usuario.php");
$flag = false;
$error = 0;

if($sucursal_usuario_log != 6 ) {
  $error = 1;
  $alerta = "Sucursal Invalida";                             
}

$sucursal = $sucursal_usuario_log;

function verStock($producto, $sucursal, $cantidad, $con ){
    $query="SELECT * from stock_sucursal where id_sucursal='$sucursal' and id_producto='$producto'";
    $ejecutar= mysqli_query($con, $query);
  
    while ($row = mysqli_fetch_array($ejecutar)){
      $stock = $row['stock_sucursal'];;
    }
  
    if ($stock >= $cantidad){
      return true;
    } else{
    return false;
    }
  
  }
  
  
  function stockMaximo($producto, $sucursal, $cantidad, $con ){
    $query="SELECT stock_sucursal from stock_sucursal where id_sucursal='$sucursal' and id_producto='$producto'";
    $ejecutar= mysqli_query($con, $query);
  
    while ($row = mysqli_fetch_array($ejecutar)){
      $stock = $row['stock_sucursal'];
    }
  
    return $stock;  
  }
  


if (!empty($_FILES['uploadedFile']['name'])){ 
      
    if (isset($_POST['uploadBtn']) && $_POST['uploadBtn'] == 'Procesar Archivo')  {
          if (isset($_FILES['uploadedFile']) && $_FILES['uploadedFile']['error'] === UPLOAD_ERR_OK)  {
                
  
          $fileTmpPath = $_FILES['uploadedFile']['tmp_name'];
          $fileName = $_FILES['uploadedFile']['name'];
          $fileSize = $_FILES['uploadedFile']['size'];
          $fileType = $_FILES['uploadedFile']['type'];
          $fileNameCmps = explode(".", $fileName);
          $fileExtension = strtolower(end($fileNameCmps));
          $newFileName = 'ventaVirtual.' . $fileExtension;
          $allowedfileExtensions = array('csv');

                if (in_array($fileExtension, $allowedfileExtensions))  {
                      $uploadFileDir = '';
                      $dest_path = $uploadFileDir . $newFileName;

                      if(move_uploaded_file($fileTmpPath, $dest_path))  {

                        $destino = "listadoVentaVirtual.php?estado=ok";
                        $class = "alert alert-success";
                        

                       $fila = 1;
                       if (($gestor = fopen("ventaVirtual.csv", "r")) !== FALSE) {
                         $aux = 0;
     
                           while (($datos = fgetcsv($gestor, 1000, ";")) !== FALSE) {  

                            //datos de la operacion-----------------------------------------------------------------------------------------------------------
                            //guardo la fecha de venta
                            if($aux == 0 ){
                              if($datos[1] != ''){
                                $nrodeVentaVirtual = $datos[1];   
                                $checkVtaVirtual = "SELECT * FROM venta_virtual WHERE nro_ventaVirtual = $nrodeVentaVirtual ";
                                $resultadocheck = mysqli_query($con, $checkVtaVirtual);
                                $row_count = mysqli_num_rows($resultadocheck);
                                if ($row_count > 0) {
                                  $alerta = "Nro de Venta Virtual ya fue procesado";
                                  $error = 1;
                                } 
                              }
                              else { 
                                $error = 1;
                                $alerta = "Nro de Venta Virtual Invalido";
                              }                              
                            }

                            //guardo la fecha de venta
                            if($aux == 1 ){
                              if($datos[1] != ''){
                                $fechaVenta = $datos[1];   
                              }
                              else { 
                                $error = 1;
                                $alerta = "Fecha Invalida";
                              }                              
                            }

                            //guardo el ciente
                            if($aux == 2 ){
                              if($datos[1] != null){
                                $cliente = $datos[1];
                              }
                              else { 
                                $error = 1;
                                $alerta = "Cliente Invalido";}                              
                            }

                            //guardo la sucursal
                          /*  if($aux == 3 ){
                              if($datos[1] != null){
                                $sucursal = $datos[1];
                              }
                              else { 
                                $error = 1;
                                $alerta = "Sucursal Invalida";}                              
                            } */

                            //medios de pago-----------------------------------------------------------------------------------------------------------

                            //efectivo
                            if($aux == 3 ){
                                $monto_efectivo = $datos[1];  
                                if($monto_efectivo == '') $monto_efectivo = 0;
                                $suma = $suma + $monto_efectivo;                          
                            }

                            //tarjeta                           
                            if($aux == 4 ){
                              $monto_tarjeta1 = $datos[1];  
                              if($monto_tarjeta1 == '') $monto_tarjeta1 = 0;                          
                              $banco_tarjeta1 = $datos[2];                            
                              $compania_tarjeta1 = $datos[3];                            
                              $nro_tarjeta1 = $datos[4];                            
                              $transaccion_tarjeta1 = $datos[5];                            
                              $vto_tarjeta1 = $datos[6];   
                              $suma = $suma + $monto_tarjeta1;                         
                            }

                            //tarjeta 2 
                            if($aux == 5 ){
                              $monto_tarjeta2 = $datos[1];   
                              if($monto_tarjeta2 == '') $monto_tarjeta2 = 0;                        
                              $banco_tarjeta2 = $datos[2];                            
                              $compania_tarjeta2 = $datos[3];                            
                              $nro_tarjeta2 = $datos[4];                            
                              $transaccion_tarjeta2 = $datos[5];                            
                              $vto_tarjeta2 = $datos[6];  
                              $suma = $suma + $monto_tarjeta2;                         
                            
                            }

                            //cheque 
                            if($aux == 6 ){
                              $monto_cheque = $datos[1];  
                              if($monto_cheque == '') $monto_cheque = 0;                                              
                              $banco_cheque = $datos[2];                            
                              $nro_cheque = $datos[3];                            
                              $fecha_cheque = $datos[4]; 
                              $suma = $suma + $monto_cheque;                         
                           
                            }

                            //credito 
                            if($aux == 7 ){
                              $monto_credito = $datos[1];   
                              if($monto_credito == '') $monto_credito = 0;                                              

                              
                              //chequeo si el limite le alcanza
                              if($monto_credito != null and $monto_credito > 0){

                                $checkLimite = "SELECT deuda_cliente, limite_cliente from cliente where id_cliente = $cliente";
                                $ejecutarCheckLimite = mysqli_query($con, $checkLimite);

                                while($row = mysqli_fetch_array($ejecutarCheckLimite)){	
                                  $deuda = $row['deuda_cliente'];
                                  $limite = $row['limite_cliente'];
                                }
                                
                                if (($monto_credito + $deuda) >= $limite){
                                  $error = 1;
                                  $alerta = "Credito Insuficiente";
                                }
                                $suma = $suma + $monto_credito;                         


                              }
                            }

                           // $suma = $monto_cheque + $monto_credito + $monto_efectivo + $monto_tarjeta1 + $monto_tarjeta2; 

                                                         
                               if($error == 0){
     
                                 if($aux == 8){
                                   $insertVenta = "INSERT INTO venta(cliente_venta, vendedor_venta, monto_venta, estado_venta, sucursal_venta) VALUES ('$cliente', '$id_usuario_log' , '$suma', 'Abonada - Virtual', '$sucursal')";
                                   $ejecutarVenta = mysqli_query($con, $insertVenta);



                                    if($ejecutarVenta){

                                      $insertarVtaVirtual = "INSERT INTO venta_virtual(nro_ventaVirtual, usuario_ventaVirtual) VALUES ('$nrodeVentaVirtual', '$id_usuario_log')";
                                      $ejecutarVtaVirtual = mysqli_query($con, $insertarVtaVirtual);


                                      $queryaux2 = "SELECT MAX(id_venta) FROM venta";
                                      $queryIDventa = mysqli_query($con, $queryaux2);
                                      while ($fila2 = mysqli_fetch_array($queryIDventa)) 
                                        {
                                          $idUltimaVenta = $fila2['MAX(id_venta)'];
                                        }
                                     
                                     
                                      if($monto_cheque != null and $monto_cheque > 0){
                                        $insertarPago = "INSERT INTO pago_cheque(banco_cheque, nro_cheque, fecha_cheque, monto_cheque, venta_cheque) VALUES ('$banco_cheque', '$nro_cheque', '$fecha_cheque', '$monto_cheque', '$idUltimaVenta')";
                                        $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

                                        $updateSucursal =  "UPDATE sucursal SET montoCheques_sucursal=montoCheques_sucursal+$monto_cheque WHERE id_sucursal = '$sucursal'";
                                        $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
                                      }

                                      
                                      if($monto_credito != null and $monto_credito > 0){
                                        $insertarPago = "INSERT INTO pago_credito(monto_credito, cliente_credito, venta_credito) VALUES ('$monto_credito', '$cliente', $idUltimaVenta)";
                                        $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

                                        $updateSucursal =  "UPDATE cliente SET deuda_cliente= deuda_cliente + $monto_credito WHERE id_cliente = '$cliente'";
                                        $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
                                      }


                                      if($monto_efectivo != null and $monto_efectivo > 0){
                                        $insertarPago = "INSERT INTO pago_efectivo(monto_efectivo, venta_efectivo) VALUES ('$monto_efectivo', '$idUltimaVenta')";
                                        $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

                                        $updateSucursal =  "UPDATE sucursal SET montoEfectivo_sucursal=montoEfectivo_sucursal+ $monto_efectivo WHERE id_sucursal = '$sucursal'";
                                        $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
                                      }


                                      if($monto_tarjeta1 != null and $monto_tarjeta1 > 0){
                                        $insertarPago = "INSERT INTO pago_tarjeta(monto_tarjeta, banco_tarjeta, compania_tarjeta, nro_tarjeta, transaccion_tarjeta, vto_tarjeta, venta_tarjeta) VALUES ('$monto_tarjeta1', '$banco_tarjeta1', '$compania_tarjeta1', '$nro_tarjeta1', '$transaccion_tarjeta1', '$vto_tarjeta1', '$vto_tarjeta1', '$idUltimaVenta')";
                                        $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

                                        $updateSucursal =  "UPDATE sucursal SET montoCredito_sucursal=montoCredito_sucursal+ $monto_tarjeta1 WHERE id_sucursal = '$sucursal'";
                                        $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
                                      }


                                      if($monto_tarjeta2 != null and $monto_tarjeta2 > 0){
                                        $insertarPago = "INSERT INTO pago_tarjeta(monto_tarjeta, banco_tarjeta, compania_tarjeta, nro_tarjeta, transaccion_tarjeta, vto_tarjeta, venta_tarjeta) VALUES ('$monto_tarjeta2', '$banco_tarjeta2', '$compania_tarjeta2', '$nro_tarjeta2', '$transaccion_tarjeta2', '$vto_tarjeta2', '$vto_tarjeta2', '$idUltimaVenta')";
                                        $ejecutarInsertarPago = mysqli_query($con, $insertarPago);

                                        $updateSucursal =  "UPDATE sucursal SET montoCredito_sucursal=montoCredito_sucursal+ $monto_tarjeta2 WHERE id_sucursal = '$sucursal'";
                                        $ejecutarUpdateSucursal  = mysqli_query($con, $updateSucursal);
                                      }


                                   }

                                 }

                                 if($aux >= 9){

 
                                 if(verStock($datos[0], $sucursal_usuario_log, $datos[1], $con )){
                                   $cant = $datos[1];
                                 } else{
                                   $cant = stockMaximo($datos[0], $sucursal_usuario_log, $datos[1], $con);
                                   $filaaux = $aux+1;
                                   $alerta = $alerta."- Stock insuficiente del producto $datos[0] (fila $filaaux), se cargaron $cant (Se habian solicitado $datos[1])<br>";
                                 }

                                 $insertDetalle = "INSERT INTO detalle_venta(venta_detalle, producto_detalle, cantidad_detalle) VALUES ((SELECT MAX(id_venta) FROM venta), '$datos[0]', '$cant') ";
                                 $ejecutarDetalle = mysqli_query($con, $insertDetalle);

                                 $qaux= "SELECT MAX(id_venta) as id FROM venta";
                                 $eaux= mysqli_query($con, $qaux);
                                 while ($row = mysqli_fetch_array($eaux)){
                                   $idventa = $row['id'];
                                 }

                                 $updateStock = "UPDATE stock_sucursal SET stock_sucursal=stock_sucursal-$cant WHERE id_producto = $datos[0] and id_sucursal = $sucursal_usuario_log ";
                                 $ejecutarUpdate = mysqli_query($con, $updateStock);

     
                           }
                          }
                          $destino = "listadoVentaVirtual.php";
                          $class = "alert alert-danger";
                        
                           $aux = $aux + 1;
                         }
                        }
                           fclose($gestor);
                       }
                      /* $message = 'Listado cargado con exito';
                       $destino = "listadoVentaVirtual.php";
                       $class = "alert alert-success";*/



                      } else   {
                           $class = "alert alert-danger";
                           $message = 'No se adjuntó el archivo, formato invalido (Se admite solo formato .CSV)';
                           $destino = "ventaVirtual.php";

                      }
                }  else {
                    $class = "alert alert-danger";
                    $message = 'Algo salió mal';
                    $destino = "ventaVirtual.php";

                }
          } else  {
            $class = "alert alert-danger";
            $message = 'Algo salió mal.<br>';
            $message .= 'Error:' . $_FILES['uploadedFile']['error'];
            $destino = "ventaVirtual.php";

          } 
    

if($error == 0 ) {
  $message = 'Archivo Procesado con exito';
  $destino = "listadoVentaVirtual.php?estado=ok&id=$idventa";
  $class = "alert alert-success";
}
$message = $message."<br>".$alerta;

/*if($alerta != ''){ ?>
   <script>alert("<?php echo $alerta ?>")</script>
<?php } */

    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
}





?>