<?php
include("../conexion.php");
include("../usuario.php");

if (isset($_POST) && !empty($_POST)) {
    $id = $_POST['id'];
    $parametro = $_POST['parametro'];
    $nombre = $_POST['nombre'];
    $estado = $_POST['estado'];
    $apoderado = $_POST['apoderado'];
    $dni = $_POST['dni'];
    $calle = $_POST['calle'];
    $altura = $_POST['altura'];
    $piso = $_POST['piso'];
    $depto = $_POST['depto'];
    $cp = $_POST['cp'];
    $localidad = $_POST['localidad'];
    $provincia = $_POST['provincia'];
    $cuil = $_POST['cuil'];
    $mail = $_POST['mail'];
    $telefono = $_POST['telefono'];
    $precio = $_POST['precio'];
    $proveedor = $_POST['proveedor'];
    $stockminimo = $_POST['stockminimo'];
    $cantidadasolicitar = $_POST['cantidadasolicitar'];
    $categoria = $_POST['categoria'];
    $categoria2 = $_POST['categoria2'];
    $stockseguridad = $_POST['stockseguridad'];
    $checkstock = $_POST['checkstock'];
    $condicionFiscal = $_POST['condicionFiscal'];
    $tipo = $_POST['tipo'];
    $subcategoria = $_POST['subcategoria'];
    $iva = $_POST['iva'];
    $cantidadLote = $_POST['cantidadLote'];

    
    $unidad = $_POST['unidaddecompra'];
    if($categoria == 2 ) $cantidadLote = $unidad;

    $precio = round($precio, 2);


    if ($checkstock == 0) {
        $stockseguridad = 0;
        $cantidadasolicitar = 0;
        $stockminimo = 0;
        $proveedor = 0;
    }


    $checkparam = "SELECT * FROM $parametro WHERE nombre_$parametro = '$_POST[nombre]' and id_$parametro <> $id ";
    $resultadocheck = mysqli_query($con, $checkparam);
    $row_count = mysqli_num_rows($resultadocheck);
    if ($row_count != 0) {
        $message = "No se pudo actualizar. Ya existe un parametro con el nombre ingresado.";
        $class = "alert alert-danger";
    } else {

        if ($parametro == 'sucursal' || $parametro == 'proveedor' || $parametro == 'cliente') {
            $checkparam = "SELECT * FROM domicilio WHERE calle_domicilio = '$calle' AND altura_domicilio = '$altura' AND piso_domicilio = '$piso' AND departamento_domicilio = '$depto' AND cp_domicilio = '$cp' AND localidad_domicilio = '$localidad' AND provincia_domicilio = '$provincia'";

            $resultadocheck = mysqli_query($con, $checkparam);
            $row_count = mysqli_num_rows($resultadocheck);

            if ($row_count != 0) {
                while ($row = mysqli_fetch_array($resultadocheck)) {
                    $id_domicilio = $row['id_domicilio'];
                }
            } else {
                $insertar_domicilio = "INSERT INTO domicilio (calle_domicilio, altura_domicilio, piso_domicilio, departamento_domicilio, cp_domicilio, localidad_domicilio, provincia_domicilio) VALUES ('$calle', '$altura', '$piso', '$depto', '$cp', '$localidad', '$provincia')";
                $ejecutar = mysqli_query($con, $insertar_domicilio);

                $idQuery = "SELECT MAX(id_domicilio) AS id FROM domicilio";
                $resultado = mysqli_query($con, $idQuery);
                while ($row = mysqli_fetch_array($resultado)) {
                    $id_domicilio = $row['id'];
                }
            }
        }
        if ($parametro == 'sucursal') {
            $update = "UPDATE $parametro SET nombre_$parametro= '$nombre', domicilio_$parametro= '$id_domicilio', estado_$parametro= '$estado' WHERE id_$parametro = $id";
        }
        if ($parametro == 'proveedor') {
            $update = "UPDATE $parametro SET nombre_$parametro= '$nombre', apoderado_$parametro= '$apoderado', dni_$parametro='$dni', domicilio_$parametro='$id_domicilio', cuil_$parametro='$cuil', mail_$parametro='$mail', telefono_$parametro='$telefono', estado_$parametro= '$estado' WHERE id_$parametro = $id";
        }
        if ($parametro == 'cliente') {
            $update = "UPDATE $parametro SET nombre_$parametro= '$nombre', dni_$parametro= '$dni', cuil_$parametro='$cuil', telefono_$parametro='$telefono', mail_$parametro='$mail', domicilio_$parametro='$id_domicilio',  condicionFiscal_$parametro='$condicionFiscal', tipo_$parametro= '$tipo', estado_$parametro= '$estado', responsableIva_cliente = '$iva' WHERE id_$parametro = $id";
        }
    }
    if ($parametro == 'producto') {
        $update = "UPDATE $parametro SET nombre_$parametro= '$nombre', precio_$parametro= '$precio', proveedorPredeterminado_$parametro= '$proveedor', stockMinimo_$parametro='$stockminimo', estado_$parametro= '$estado', cantidadASolicitar_$parametro= '$cantidadasolicitar', cantidadLote_$parametro = '$cantidadLote', categoria_$parametro = '$categoria', stockSeguridad_$parametro = '$stockseguridad', subcategoria_$parametro = '$subcategoria' WHERE id_$parametro = $id";

    }

    if ($parametro == 'categoria') {
        $update = "UPDATE $parametro SET nombre_$parametro= '$nombre', estado_$parametro= '$estado' WHERE id_$parametro = $id";
    }

    if ($parametro == 'subcategoria') {
        $update = "UPDATE $parametro SET nombre_$parametro= '$nombre', estado_$parametro= '$estado', categoria_$parametro = '$categoria2' WHERE id_$parametro = $id";
    }

    $ejecutarUpdate = mysqli_query($con, $update);



    if ($ejecutarUpdate) {

        if ($parametro == 'cliente') {

            $queryAuditoria = "INSERT INTO auditoria_cliente (cliente_ac, comentario_ac, responsable_ac) VALUES ('$id', 'Se actualizó el cliente', $id_usuario_log)"; //dejo registro en la auditoria
            $resultado = mysqli_query($con, $queryAuditoria);
        }

        $message = "Operación finalizada exitosamente. Redirigiendo a la pagina anterior";
        $class = "alert alert-success";
    } else {
        $message = "Error: No se pudieron insertar los datos";
        $class = "alert alert-danger";
    }
    //echo $update;
    $destino = "abmParametros.php?parametro=$parametro";
    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
}
