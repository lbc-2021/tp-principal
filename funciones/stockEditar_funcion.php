<?php

include('../conexion.php');
include('../usuario.php');


//actualizo la base de datos, esto se ejecuta luego de presionar "Actualizar" en el formulario.
if (isset($_POST) && !empty($_POST)) {
    $id = $_POST['id'];
    $accion = $_POST['accion'];
    $cantidad = $_POST['cantidad'];
    $observacion = $_POST['observacion'];
    $id_sucursal = $_POST['id_sucursal'];
    $sucursal = $_POST['sucursal'];
    $producto = $_POST['producto'];
    $stock = $_POST['stock'];
    $lote = $_POST['lote'];
    $id_proveedor = $_POST['id_proveedor'];
    $monto_pedido = $_POST['monto_pedido'];
    $monto_pagado = $_POST['monto_pagado'];
    $sucursalDestino = $_POST['sucursaldestino'];
    $cantidadASolicitar_producto = $_POST['cantidadASolicitar_producto'];
    $nombre_proveedor = $_POST['nombre_proveedor'];
    $mail_proveedor = $_POST['mail_proveedor'];
    $stockMinimo_producto = $_POST['stockMinimo_producto'];
    $stockSeguridad_producto = $_POST['stockSeguridad_producto'];
    $nombre_producto = $_POST['nombre_producto'];
    $nombre_sucursal = $_POST['nombre_sucursal'];
    $medio_pago = $_POST['medio_pago'];


    $query = "SELECT * FROM stock_sucursal where id = $id";
    $resultado = mysqli_query($con, $query);

    while ($fila = mysqli_fetch_array($resultado)) {
        $id_sucursal = $fila['id_sucursal'];
        $stock_sucursal = $fila['stock_sucursal'];
        $id_producto = $fila['id_producto'];
    }

    $query = "SELECT * FROM proveedor where id_proveedor = $id_proveedor";
    $resultado = mysqli_query($con, $query);

    //Si ingreso Stock --------------------------------------------------------------
    if ($accion == 'ingresar') {
        while ($fila = mysqli_fetch_array($resultado)) {
            $proveedor = $fila['nombre_proveedor'];
            $deuda = $fila['deuda_proveedor'];
            $limite = $fila['limite_proveedor'];
        }

        $query = "SELECT * FROM sucursal where id_sucursal = $id_sucursal";
        $resultado = mysqli_query($con, $query);

        while ($fila = mysqli_fetch_array($resultado)) {
            $montoEfectivo = $fila['montoEfectivo_sucursal'];
            $montoCheques = $fila['montoCheques_sucursal'];
            $montoCredito = $fila['montoCredito_sucursal'];
        }
        if($lote == 'Galon') {$cantidad = $cantidad*3.78; 
            $cantidad = floatval($cantidad);
            $total = $stock + $cantidad;
        }
        else if ($lote == 'Litro') {
             $lote =0;
             $total = $stock + $cantidad;

        }
         else if ($lote == 0) {$total = $stock + $cantidad; }
        else if ($lote > 0) { $total = $stock + ($cantidad * $lote); }


        if ($medio_pago == 'efectivo') {
            $nuevoMontoCaja = $montoEfectivo - $monto_pagado;
        } else if ($medio_pago == 'cheque') {
            $nuevoMontoCaja = $montoCheques - $monto_pagado;
        } else {
            $nuevoMontoCaja = $montoCredito - $monto_pagado;
        }
        if ($nuevoMontoCaja >= 0) {
            $montoAdeudado = $monto_pedido - $monto_pagado;
            $nuevaDeuda = $deuda + $montoAdeudado;
            if ($nuevaDeuda <= $limite) {
                $update = "UPDATE stock_sucursal SET stock_sucursal= $total WHERE id = $id ";
                $ejecturarUpdate = mysqli_query($con, $update);
                $aux = "Agregar Stock";
                if ($observacion == "") {
                    $observacion .= "El stock fue proveído por $proveedor.\nEl valor del pedido fue $$monto_pedido y el valor pagado fue $$monto_pagado.";
                } else {
                    $observacion .= "\nEl stock fue proveído por $proveedor.\nEl valor del pedido fue $$monto_pedido y el valor pagado fue $$monto_pagado.";
                }
                $updateDeuda = "UPDATE proveedor SET deuda_proveedor= $nuevaDeuda WHERE id_proveedor= $id_proveedor";
                $ejecutarUpdateDeuda = mysqli_query($con, $updateDeuda);
                if ($medio_pago == 'efectivo') {

                    //Dejo registro en la auditoria_caja
                    $monto_final = $montoEfectivo - $monto_pagado;
                    $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja)
                        VALUES ('Se ingresó stock del producto $producto','$monto_pagado','$montoEfectivo','$id_sucursal', '$id_usuario_log', '$monto_final')";
                    $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);

                    $updateCaja = "UPDATE sucursal SET montoEfectivo_sucursal= $nuevoMontoCaja WHERE id_sucursal= $id_sucursal";
                } else if ($medio_pago == 'cheque') {
                    $monto_final = $montoCheques - $monto_pagado;
                    $updateCaja = "UPDATE sucursal SET montoCheques_sucursal= $nuevoMontoCaja WHERE id_sucursal= $id_sucursal";
                    $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja, medio_acaja)
                    VALUES ('Se ingresó stock del producto $producto','$monto_pagado','$montoCheques','$id_sucursal', '$id_usuario_log', '$monto_final', 'cheque')";
                    $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);
                } else {
                    $monto_final = $montoCredito - $monto_pagado;

                    $updateCaja = "UPDATE sucursal SET montoCredito_sucursal= $nuevoMontoCaja WHERE id_sucursal= $id_sucursal";
                    $insertarAuditoriaCaja = "INSERT INTO auditoria_caja (descripcion_acaja, monto_acaja, montoAnterior_acaja, sucursal_acaja, usuario_acaja, montoPosterior_acaja, medio_acaja)
                    VALUES ('Se ingresó stock del producto $producto','$monto_pagado','$montoCredito','$id_sucursal', '$id_usuario_log', '$monto_final', 'tarjeta')";
                    $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoriaCaja);

                }
                $ejecutarUpdateCaja = mysqli_query($con, $updateCaja);
                if ($montoAdeudado > 0) {
                    $insertAuditoria = "INSERT INTO auditoria_cc_proveedores (proveedor_accp, descripcion_accp, monto_accp, montoPrevio_accp, montoActual_accp) VALUES ('$id_proveedor', 'Pago parcial de pedido', '$montoAdeudado', '$deuda', '$nuevaDeuda')";
                    $ejecutarInsertAuditoria = mysqli_query($con, $insertAuditoria);
                }
            } else {
                $error = "Error: La deuda supera el límite, no se puede completar el ingreso. Redirigiendo a la pagina anterior";
                $class = "alert alert-danger";
                $destino = "stock.php";
                header("refresh:2; ../mensaje.php?class=$class&message=$message&destino=$destino");
            }
        } else {
            $error = "Error: No hay dinero suficiente en esta sucursal para pagar el pedido. Redirigiendo a la pagina anterior";
            $class = "alert alert-danger";
            $destino = "stock.php";
            header("refresh:2; ../mensaje.php?class=$class&message=$message&destino=$destino");
        }
    }

    //Si retiro Stock --------------------------------------------------------------
    if ($accion == 'retirar') {
        $total = $stock - $cantidad;
        if ($total >= 0) {
            $update = "UPDATE stock_sucursal SET stock_sucursal= $total WHERE id = $id ";
            $ejecturarUpdate = mysqli_query($con, $update);
            $aux = "Retirar Stock";
        } else {
            $error = "Error: Stock insuficiente para la operación. Redirigiendo a la pagina anterior";
            $class = "alert alert-danger";
            $destino = "stock.php";
            header("refresh:2; ../mensaje.php?class=$class&message=$message&destino=$destino");
        }
    }

    //Si Muevo a otra sucursal Stock --------------------------------------------------------------
    if ($accion == 'cambiosucursal') {
        $total = $stock - $cantidad;
        if ($total >= 0) {
            $update = "UPDATE stock_sucursal SET stock_sucursal= $total WHERE id = $id ";
            $ejecturarUpdate1 = mysqli_query($con, $update);
            if ($ejecturarUpdate1) {

                $query2 = "SELECT * FROM stock_sucursal  where  id_producto = $id_producto and id_sucursal = $sucursalDestino ";
                $resultado2 = mysqli_query($con, $query2);

                while ($fila = mysqli_fetch_array($resultado2)) {
                    $id2 = $fila['id'];
                    $stock2 = $fila['stock_sucursal'];
                    $total2 = $stock2 + $cantidad;

                    $update2 = "UPDATE stock_sucursal SET stock_sucursal= $total2 WHERE id = $id2 ";
                    $ejecturarUpdate = mysqli_query($con, $update2);
                }
            }
            $aux = "Envio a otra Sucursal";
        } else {
            $error = "Error: Stock insuficiente para la operación. Redirigiendo a la pagina anterior";
            $class = "alert alert-danger";
            $destino = "stock.php";
            header("refresh:2; ../mensaje.php?class=$class&message=$message&destino=$destino");
        }
    }

    //Aca se ejecuto bien la query (de cualquier variante), hago el insert en auditoria y redirijo la pagina
    if ($ejecturarUpdate) {
        $message = "Stock Actualizado con Exito. Redirigiendo a la pagina anterior";
        $class = "alert alert-success";

        $insertarAuditoria = "INSERT INTO auditoria_stock (usuario_as, accion_as, comentario_as, sucursal_as, producto_as, sucursalDestino_as, stockPrevio_as, stockFinal_as, cantidad_as) 
        VALUES ('$id_usuario_log','$aux','$observacion','$id_sucursal','$id_producto','$sucursalDestino','$stock_sucursal','$total',$cantidad)";
        $ejecutarInsertarAuditoria = mysqli_query($con, $insertarAuditoria);



        //envio de mail-----------------------------------------------------------------
        if ($accion == 'retirar' || $accion == 'cambiosucursal') {
            if ($stockMinimo_producto != 0 && $total < $stockMinimo_producto) { //envio de mail de StockMinimo
                $EmailTo = $mail_proveedor;
                $EmailFrom = "pedidos@appcomerciales.com.ar";
                $Body = "";
                $Body .= "Estimado $nombre_proveedor,";
                $Body .= "\n";
                $Body .= "Se solicita $cantidadASolicitar_producto unidades de $nombre_producto para la sucursal $nombre_sucursal.";
                $Body .= "\n";
                $Body .= "\n";
                $Body .= "\n";
                $Body .= "Saludos,";
                $Body .= "\n";
                $Body .= "Nombre del Negocio";
                $Body .= "\n";
                $Body .= "\n";
                $Body .= "Este mail fue generado automaticamente, por favor no responder el mismo (no será leido)";
                $Subject = "Pedido de $nombre_producto";
                $success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");


            }


            if ($stockSeguridad_producto != 0 && $total < $stockSeguridad_producto) { //envio de mail de StockSeguridad

                $queryUsuarios = "SELECT mail_usuario from usuario where perfil_usuario = 'Gerente'";
                $ejecutarQueryUsuarios = mysqli_query($con, $queryUsuarios);
                while ($row = mysqli_fetch_array($ejecutarQueryUsuarios)) {
                    $mail_gerente = $row['mail_usuario'];

                    $EmailTo = $mail_gerente;
                    $EmailFrom = "pedidos@appcomerciales.com.ar";
                    $Body = "";
                    $Body .= "Estimado Gerente,";
                    $Body .= "\n";
                    $Body .= "Se informa que el producto $nombre_producto se encuentra por debajo del stock de seguridad en la sucursal $nombre_sucursal. Se solicita agilizar su reposición";
                    $Body .= "\n";
                    $Body .= "\n";
                    $Body .= "\n";
                    $Body .= "Saludos,";
                    $Body .= "\n";
                    $Body .= "Nombre del Negocio";
                    $Body .= "\n";
                    $Body .= "\n";
                    $Body .= "Este mail fue generado automaticamente, por favor no responder el mismo (no será leido)";
                    $Subject = "Pedido de $nombre_producto";
                    $success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");


                }
            }
        }

        //fin de envio de mail----------------------------------------------------------

        $message = "Operación realizada con Exito. Redirigiendo a la pagina anterior";
        $class = "alert alert-success";
        // header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
        // header("refresh:2; ../stock.php");
    } else {
        $message = $error;
        $class = "alert alert-danger";
    }
    $destino = "stock.php";
    header("refresh:0; ../mensaje.php?class=$class&message=$message&destino=$destino");
}
