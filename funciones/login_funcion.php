<?php
session_start();
?>

<!doctype html>
<html lang="en">
    <head>
        <title>Inicio de sesión</title>
        <meta charset="utf-8">
        <meta nombre="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    </head>
    <body>  
        <div class="container">
            <?php
            include ("../conexion.php");         
            $usuario = $_POST['usuario']; 
            $password = $_POST['password'];
        
            $checkusuario = "SELECT * FROM usuario left join sucursal on sucursal_usuario = id_sucursal WHERE nombre_usuario = '$usuario' and estado_usuario = '1' ";
            $ejecutarA = mysqli_query($con, $checkusuario);		

            while($fila = mysqli_fetch_array($ejecutarA)){
                $pass = $fila['password_usuario'];
                $descripcion_usuario= $fila['descripcion_usuario'];
                $nombre_usuario= $fila['nombre_usuario'];
                $estado_usuario= $fila ['estado_usuario'];
                $id_usuario= $fila['id_usuario'];
                $sucursal= $fila['sucursal_usuario'];
                $sucursalNombre = $fila['nombre_sucursal'];
                $perfil= $fila['perfil_usuario'];

            }

            if (password_verify($_POST['password'], $pass)) {			
                $_SESSION['descripcion_usuario'] = $descripcion_usuario;
                $_SESSION['nombre_usuario'] = $nombre_usuario;
                $_SESSION['estado_usuario'] = $estado_usuario;
                $_SESSION["id_usuario"]= $id_usuario;
                $_SESSION["sucursal_usuario"]= $sucursal;
                $_SESSION["perfil_usuario"]= $perfil;
                $_SESSION["nombre_sucursal"]= $sucursalNombre;


                echo "<div class='alert alert-success' role='alert'><strong>Bienvenido $descripcion_usuario	</strong>		
                </div>";	
                header( "Location: ../index.php" );
            } else{
                echo "<div class='alert alert-danger' role='alert'>Usuario o Password incorrecto!
                <p><a href='../login.php'><strong>Por favor volvé a intentar</strong></a></p></div>";			
            }

            ?>
        </div>       
	</body>
</html>